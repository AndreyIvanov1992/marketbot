package com.pirogsoft.marketbot.database.service;

import com.pirogsoft.marketbot.database.entities.Variable;
import com.pirogsoft.marketbot.database.repository.VariableRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.naming.Reference;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Сервис для работы с таблицей переменных.
 */
@Service
public class VariableService {

    /**
     * Имя переменной ключа маркета
     */
    public static final String API_MARKET_KEY_VAR_NAME = "api_market_key";

    /**
     * круд сервис для работы с таблицей
     */
    private VariableRepository variableRepository;

    /**
     * Карта закешированных значений
     */
    private Map<String, String> variablesMap;

    /**
     * получение ключа к API для маркета
     *
     * @return ключ к API для маркета
     */
    public String getApiMarketKey() {
        return getValueByName(API_MARKET_KEY_VAR_NAME);
    }

    /**
     * Инициализация карты переменных
     */
    @PostConstruct
    private void initVariablesMap() {
        variablesMap = new HashMap<>();
        List<Variable> variables = variableRepository.findAll();
        for (Variable variable : variables) {
            variablesMap.put(variable.getName(), variable.getValue());
        }
    }

    /**
     * Получет значение переменной по названию
     *
     * @param name название переменной
     * @return значение переменной
     */
    public String getValueByName(String name) {
        return getVariablesMap().get(name);
    }

    /**
     * получение карты переменных
     *
     * @return карта переменных
     */
    private Map<String, String> getVariablesMap() {
        return variablesMap;
    }

    /**
     * Установка круд сервиса для работы с таблицей переменных
     *
     * @param variableRepository инстанс круд сервиса для работы с таблицей переменных
     */
    @Autowired
    public void setVariableRepository(VariableRepository variableRepository) {
        this.variableRepository = variableRepository;
    }
}
