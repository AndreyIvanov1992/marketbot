package com.pirogsoft.marketbot.database.service.fill;

import com.pirogsoft.marketbot.common.ClassInstanceId;
import com.pirogsoft.marketbot.common.ClassInstanceIdSupport;
import com.pirogsoft.marketbot.database.entities.Game;
import com.pirogsoft.marketbot.database.entities.MarketItem;
import com.pirogsoft.marketbot.database.entities.SteamItem;
import com.pirogsoft.marketbot.database.repository.MarketItemRepository;
import com.pirogsoft.marketbot.database.repository.SteamItemRepository;
import com.pirogsoft.marketbot.query.beans.market.response.massinfo.MassInfoResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Сервис для заполнения БД на основании ответов на запросы на маркет
 * Created by AnIvanov on 09.05.2018.
 */
@Service
public class CreateDBItemsByQueryResponseService {

    /**
     * Приоритет этих вещей не перезатирается.
     */
    public static final int IMMUTABLE_PRIORITY = 5;

    /**
     * CRUD-сервис для предметов маркет
     */
    private MarketItemRepository marketItemRepository;

    /**
     * CRUD-сервис для предметов steam
     */
    private SteamItemRepository steamItemRepository;

    /**
     * Установка CRUD-сервиса для предметов маркет
     *
     * @param marketItemRepository CRUD-сервис для предметов маркет
     */
    @Autowired
    public void setMarketItemRepository(MarketItemRepository marketItemRepository) {
        this.marketItemRepository = marketItemRepository;
    }

    /**
     * Установка CRUD-сервиса для предметов steam
     *
     * @param steamItemRepository CRUD-сервис для предметов steam
     */
    @Autowired
    public void setSteamItemRepository(SteamItemRepository steamItemRepository) {
        this.steamItemRepository = steamItemRepository;
    }

    /**
     * Добавление предмета в БД на основании объекта предмета из ответа на запрос получения массовой информации.
     *
     * @param itemInfo     Объект с информацией о предмете из ответа
     * @param game         Игра
     * @param sellPriority Приоритет продажи
     * @param buyPriority  Приоритет покупки
     * @return Добавленый в базу данных предмет маркета или null, в случае, если предмет уже существует в БД
     */
    public MarketItem addMarketItemToDb(MassInfoResult itemInfo, Game game, int sellPriority, int buyPriority, boolean updatePriority) {
        return addMarketItemToDb(itemInfo, itemInfo.getInfo().getMarketHashName(), itemInfo.getInfo().getMarketName(), game, sellPriority, buyPriority, updatePriority);
    }

    /**
     * Добавление предмета в БД или обновление приоритета, в случае сущестовавания предмета
     *
     * @param item           Предмет
     * @param marketHashName Идентификатор вещи в стиме
     * @param marketName     название вещи на маркете
     * @param game           Объект игры из БД
     * @param sellPriority   приоритет покупки
     * @param buyPriority    приоритет продажи
     * @param updatePriority обновлять ли приоритет вещи, если она уже существует
     * @return добавленый в базу данных предмет маркета или null, в случае, если предмет уже существует в БД
     */
    public MarketItem addMarketItemToDb(ClassInstanceIdSupport item, String marketHashName, String marketName, Game game, int sellPriority, int buyPriority, boolean updatePriority) {
        ClassInstanceId classInstanceId = item.obtainClassInstanceId();
        String classInstanceIdString = classInstanceId.obtainClassInstanceIdString();
        MarketItem marketItem = marketItemRepository.findMarketItemByClassInstanceId(classInstanceIdString);
        if (marketItem != null) {
            if (updatePriority) {
                boolean needSave = false;
                if (marketItem.getBuyPriority() != IMMUTABLE_PRIORITY) {
                    marketItem.setBuyPriority(buyPriority);
                    needSave = true;
                }
                if (marketItem.getSellPriority() != IMMUTABLE_PRIORITY) {
                    marketItem.setSellPriority(sellPriority);
                    needSave = true;
                }
                if (needSave) {
                    marketItemRepository.save(marketItem);
                }
            }
            //Уже существует
            return null;
        }

        SteamItem steamItem = steamItemRepository.findByMarketHashName(marketHashName);
        if (steamItem == null) {
            steamItem = new SteamItem();
            steamItem.setGame(game);
            steamItem.setMarketHashName(marketHashName);
            boolean isInscribed = calculateIsInscribed(marketHashName);
            steamItem.setInscribed(isInscribed);

            if (isInscribed) {
                SteamItem referenceSteamItem = new SteamItem();
                referenceSteamItem.setInscribed(false);
                referenceSteamItem.setGame(game);
                referenceSteamItem.setMarketHashName(calculateReferenceItemHashName(marketHashName));
                steamItemRepository.save(referenceSteamItem);
                steamItem.setReferenceSteamItem(referenceSteamItem);
            }
            steamItem = steamItemRepository.save(steamItem);
        }

        marketItem = new MarketItem();
        marketItem.setClassInstanceId(classInstanceIdString);
        marketItem.setSteamItemByMarketHashName(steamItem);
        marketItem.setName(marketName);
        marketItem.setBuyPriority(buyPriority);
        marketItem.setSellPriority(sellPriority);
        return marketItemRepository.save(marketItem);
    }

    /**
     * Проверка рунированный ли предмет
     *
     * @param marketHashName уникальный идентификатор предмета в стиме
     * @return результат проверки
     */
    private boolean calculateIsInscribed(String marketHashName) {
        return marketHashName.toLowerCase().contains("inscribed");
    }

    /**
     * Получение идентификатора steam "Референсной" вещи для "Рунированной" (для доты)
     *
     * @param marketHashName идентификатор steam рунированной вещи
     * @return индикатор steam референсной вещи
     */
    private String calculateReferenceItemHashName(String marketHashName) {
        return marketHashName.replace("Inscribed ", "");
    }
}
