package com.pirogsoft.marketbot.database.repository;

import com.pirogsoft.marketbot.database.entities.Variable;
import org.springframework.data.repository.Repository;

import java.util.List;

/**
 * КРУД сервис для таблицы переменных (параметров)
 */
public interface VariableRepository extends Repository<Variable, String> {
    /**
     * Получить все переменные
     *
     * @return список всех переменных
     */
    List<Variable> findAll();
}
