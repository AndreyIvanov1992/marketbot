package com.pirogsoft.marketbot.database.repository;

import com.pirogsoft.marketbot.database.entities.Game;
import com.pirogsoft.marketbot.database.entities.SteamItem;
import org.springframework.data.repository.Repository;

import java.util.List;

/**
 * CRUD для SteamItem
 * Created by AnIvanov on 07.05.2018.
 */
public interface SteamItemRepository extends Repository<SteamItem, String> {

    /**
     * Поиск по прдемета в стиме по MarketHashName
     *
     * @param marketHashName MarketHashName в стиме
     * @return Предмет из стима
     */
    SteamItem findByMarketHashName(String marketHashName);

    /**
     * Получение всех предметов в Steam
     *
     * @return список предметов Steam
     */
    List<SteamItem> findAll();

    /**
     * Поиск предметов Steam по игре
     *
     * @param game игра
     * @return список предметов Steam
     */
    List<SteamItem> findByGame(Game game);

    /**
     * Поиск предметов Steam по признаку "рунирования" предмета
     *
     * @param inscribed признак "рунирования" предмета
     * @return список предметов Steam
     */
    List<SteamItem> findByInscribed(boolean inscribed);

    /**
     * Поиск предметов Steam по признаку "рунирования" предмета и по игре
     *
     * @param game      игра
     * @param inscribed признак "рунирования" предмета
     * @return список предметов Steam
     */
    List<SteamItem> findByGameAndInscribed(Game game, boolean inscribed);

    /**
     * Сохраняет объект в БД
     *
     * @param steamItem предмет из стим
     * @return сохраненный предмет из стим
     */
    SteamItem save(SteamItem steamItem);
}
