package com.pirogsoft.marketbot.database.repository;

import com.pirogsoft.marketbot.database.entities.Game;
import org.springframework.data.repository.Repository;

/**
 * CRUD-сервис для игр
 */
public interface GameRepository extends Repository<Game, Integer> {

    /**
     * Получеие игры по имени
     * @param Name имя игры
     * @return игра
     */
    Game findByName(String Name);
}
