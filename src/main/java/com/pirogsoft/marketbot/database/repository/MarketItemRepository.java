package com.pirogsoft.marketbot.database.repository;

import com.pirogsoft.marketbot.database.entities.Game;
import com.pirogsoft.marketbot.database.entities.MarketItem;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;

import java.util.List;

/**
 * CRUD сервис для предметов с маркета
 * Created by AnIvanov on 29.04.2018.
 */
public interface MarketItemRepository extends Repository<MarketItem, String> {

    /**
     * Получение списка предметов с маркета в рамках игры и приоритета на продажу
     *
     * @param game         игра
     * @param sellPriority приоритет на продажу
     * @return список предметов с макета
     */
    @Query("select item from MarketItem item where item.steamItemByMarketHashName.game= ?1 and item.sellPriority = ?2")
    List<MarketItem> findItemByGameAndSellPriority(Game game, int sellPriority);


    /**
     * Получение списка предметов с маркета в рамках игры и приоритета на покупку
     *
     * @param game        игра
     * @param buyPriority приоритет на покупку
     * @return список предметов с макета
     */
    @Query("select item from MarketItem item where item.steamItemByMarketHashName.game= ?1 and item.buyPriority = ?2")
    List<MarketItem> findItemByGameAndBuyPriority(Game game, int buyPriority);


    /**
     * Получение списка предметов с маркета в рамках игры
     *
     * @param game        игра
     * @return список предметов с макета
     */
    @Query("select item from MarketItem item where item.steamItemByMarketHashName.game= ?1")
    List<MarketItem> findItemByGame(Game game);

    /**
     * Сохраняет объект в БД
     *
     * @param marketItem предмет из маркета
     * @return сохраненный предмет из маркета
     */
    MarketItem save(MarketItem marketItem);

    /**
     * Проверяет наличие записи в таблице предметов маркета с указанными classInstanceId
     *
     * @param classInstanceId classInstanceId
     * @return наличие записи
     */
    MarketItem findMarketItemByClassInstanceId(String classInstanceId);

    /**
     * Получение списка предметов с маркета в рамках игры
     *
     * @param game игра
     * @return список предметов с макета
     */
    @Query("select item from MarketItem item where item.steamItemByMarketHashName.game= ?1")
    List<MarketItem> findByGame(Game game);
}
