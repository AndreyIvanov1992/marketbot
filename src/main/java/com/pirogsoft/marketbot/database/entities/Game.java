package com.pirogsoft.marketbot.database.entities;

import javax.persistence.*;
import java.util.Collection;

@Entity
@Table(name = "games", schema = "market_bot")
public class Game {
    private int id;
    private String name;
    private Collection<SteamItem> steamItemsById;
    private double marketCommission;
    private double marketDiscount;

    @Id
    @Column(name = "id", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    @Basic
    @Column(name = "name", nullable = false, length = 50)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "market_commission")
    public double getMarketCommission() {
        return marketCommission;
    }

    public void setMarketCommission(double marketCommission) {
        this.marketCommission = marketCommission;
    }

    @Basic
    @Column(name = "market_discount")
    public double getMarketDiscount() {
        return marketDiscount;
    }

    public void setMarketDiscount(double marketDiscount) {
        this.marketDiscount = marketDiscount;
    }

    @OneToMany(mappedBy = "game")
    public Collection<SteamItem> getSteamItemsById() {
        return steamItemsById;
    }

    public void setSteamItemsById(Collection<SteamItem> steamItemsById) {
        this.steamItemsById = steamItemsById;
    }
}
