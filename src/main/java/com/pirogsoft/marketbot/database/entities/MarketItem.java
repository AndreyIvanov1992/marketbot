package com.pirogsoft.marketbot.database.entities;

import com.pirogsoft.marketbot.common.ClassInstanceId;
import com.pirogsoft.marketbot.common.ClassInstanceIdSupport;

import javax.persistence.*;

@Entity
@Table(name = "market_items", schema = "market_bot")
public class MarketItem implements ClassInstanceIdSupport {
    private String classInstanceId;
    private String name;
    private String marketHashName;
    private int buyPriority;
    private int sellPriority;
    private SteamItem steamItemByMarketHashName;

    @Id
    @Column(name = "class_instance_id", nullable = false, length = 50)
    public String getClassInstanceId() {
        return classInstanceId;
    }

    public void setClassInstanceId(String classInstanceId) {
        this.classInstanceId = classInstanceId;
    }

    @Basic
    @Column(name = "name", nullable = false, length = 50)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "market_hash_name", nullable = false, length = 100, insertable=false, updatable=false)
    public String getMarketHashName() {
        return marketHashName;
    }

    public void setMarketHashName(String marketHashName) {
        this.marketHashName = marketHashName;
    }

    @Basic
    @Column(name = "buy_priority", nullable = false)
    public int getBuyPriority() {
        return buyPriority;
    }

    public void setBuyPriority(int buyPriority) {
        this.buyPriority = buyPriority;
    }

    @Basic
    @Column(name = "sell_priority", nullable = false)
    public int getSellPriority() {
        return sellPriority;
    }

    public void setSellPriority(int sellPriority) {
        this.sellPriority = sellPriority;
    }

    @ManyToOne
    @JoinColumn(name = "market_hash_name", referencedColumnName = "market_hash_name", nullable = false)
    public SteamItem getSteamItemByMarketHashName() {
        return steamItemByMarketHashName;
    }

    public void setSteamItemByMarketHashName(SteamItem steamItemByMarketHashName) {
        this.steamItemByMarketHashName = steamItemByMarketHashName;
    }

    @Override
    public ClassInstanceId obtainClassInstanceId() {
        return ClassInstanceId.obtainClassInstanceId(getClassInstanceId());
    }
}
