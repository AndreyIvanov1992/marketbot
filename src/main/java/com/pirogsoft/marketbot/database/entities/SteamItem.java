package com.pirogsoft.marketbot.database.entities;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Collection;

@Entity
@Table(name = "steam_item", schema = "market_bot")
public class SteamItem {
    private String marketHashName;
    private String steamId;
    private Integer price;
    private Timestamp lastPriceUpdate;
    private int gameId;
    private Collection<MarketItem> marketItemsByMarketHashName;
    private Game game;
    private boolean inscribed;
    private String referenceItemHashName;
    private SteamItem referenceSteamItem;
    private Collection<SteamItem> referencedSteamItems;

    @Id
    @Column(name = "market_hash_name", nullable = false, length = 100)
    public String getMarketHashName() {
        return marketHashName;
    }

    public void setMarketHashName(String marketHashName) {
        this.marketHashName = marketHashName;
    }

    @Basic
    @Column(name = "steam_id", nullable = true, length = 50)
    public String getSteamId() {
        return steamId;
    }

    public void setSteamId(String steamId) {
        this.steamId = steamId;
    }

    @Basic
    @Column(name = "price")
    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    @Basic
    @Column(name = "last_price_update")
    public Timestamp getLastPriceUpdate() {
        return lastPriceUpdate;
    }

    public void setLastPriceUpdate(Timestamp lastPriceUpdate) {
        this.lastPriceUpdate = lastPriceUpdate;
    }

    @Basic
    @Column(name = "game_id", nullable = false, insertable=false, updatable=false)
    public int getGameId() {
        return gameId;
    }

    public void setGameId(int gameId) {
        this.gameId = gameId;
    }

    @Basic
    @Column(name = "reference_item_hash_name", insertable=false, updatable=false)
    public String getReferenceItemHashName() {
        return referenceItemHashName;
    }

    public void setReferenceItemHashName(String referenceItemHashName) {
        this.referenceItemHashName = referenceItemHashName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SteamItem that = (SteamItem) o;

        if (gameId != that.gameId) return false;
        if (marketHashName != null ? !marketHashName.equals(that.marketHashName) : that.marketHashName != null)
            return false;
        if (steamId != null ? !steamId.equals(that.steamId) : that.steamId != null) return false;
        if (price != null ? !price.equals(that.price) : that.price != null) return false;
        if (lastPriceUpdate != null ? !lastPriceUpdate.equals(that.lastPriceUpdate) : that.lastPriceUpdate != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = marketHashName != null ? marketHashName.hashCode() : 0;
        result = 31 * result + (steamId != null ? steamId.hashCode() : 0);
        result = 31 * result + (price != null ? price.hashCode() : 0);
        result = 31 * result + (lastPriceUpdate != null ? lastPriceUpdate.hashCode() : 0);
        result = 31 * result + gameId;
        return result;
    }

    @OneToMany(mappedBy = "steamItemByMarketHashName")
    public Collection<MarketItem> getMarketItemsByMarketHashName() {
        return marketItemsByMarketHashName;
    }

    public void setMarketItemsByMarketHashName(Collection<MarketItem> marketItemsByMarketHashName) {
        this.marketItemsByMarketHashName = marketItemsByMarketHashName;
    }

    @ManyToOne
    @JoinColumn(name = "game_id", referencedColumnName = "id", nullable = false)
    public Game getGame() {
        return game;
    }

    public void setGame(Game gamesByGameId) {
        this.game = gamesByGameId;
    }

    @Basic
    @Column(name = "inscribed", nullable = false)
    public boolean isInscribed() {
        return inscribed;
    }

    public void setInscribed(boolean inscribed) {
        this.inscribed = inscribed;
    }

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "reference_item_hash_name", referencedColumnName = "market_hash_name")
    public SteamItem getReferenceSteamItem() {
        return referenceSteamItem;
    }

    public void setReferenceSteamItem(SteamItem steamItem) {
        this.referenceSteamItem = steamItem;
    }

    @OneToMany(mappedBy = "referenceSteamItem")
    public Collection<SteamItem> getReferencedSteamItems() {
        return referencedSteamItems;
    }

    public void setReferencedSteamItems(Collection<SteamItem> referencedSteamItems) {
        this.referencedSteamItems = referencedSteamItems;
    }
}
