package com.pirogsoft.marketbot.query.parser.json;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Модель, значения в которой храняться в карте
 * Created by AnIvanov on 01.05.2018.
 */
public class JsonMapWrapper {

    /**
     * карта названий свйств - значений свойств
     */
    private Map properties;

    /**
     * Установка карты названий свойств - значений свойств
     *
     * @param properties карта названий свойств - значений свойств
     */
    protected void setProperties(Map properties) {
        this.properties = properties;
    }

    /**
     * Получение объекта по названию свойства
     *
     * @param propertyName название свойства
     * @return объект
     */
    public Object getObject(String propertyName) {
        return properties.get(propertyName);
    }

    /**
     * Получение строки по названию свойства
     *
     * @param propertyName название свойства
     * @return строка
     */
    public String getString(String propertyName) {
        return getValue(propertyName, String.class);
    }

    /**
     * Получение флага по названию свойства
     *
     * @param propertyName название свойства
     * @return флаг
     */
    public Boolean getBoolean(String propertyName) {
        return getValue(propertyName, Boolean.class);
    }

    /**
     * Получение целого числа по названию свойства, конвертируя его из double
     *
     * @param propertyName название свойства
     * @return целое число
     */
    public Integer getDoubleAsInteger(String propertyName) {
        Double doubleValue = getDouble(propertyName);
        if(doubleValue == null) {
            return null;
        }
        return  doubleValue.intValue();
    }

    /**
     * Получение целого числа по названию свойства, конвертируя его из String
     *
     * @param propertyName название свойства
     * @return целое число
     */
    public Integer getStringAsInteger(String propertyName) {
        String stringValue = getString(propertyName);
        if(stringValue == null) {
            return null;
        }
        return  Integer.valueOf(stringValue);
    }


    /**
     * Получение вещественного числа по названию свойства
     *
     * @param propertyName название свойства
     * @return вещественное число
     */
    public Double getDouble(String propertyName) {
        return getValue(propertyName, Double.class);
    }

    /**
     * Получение карты по названию свойства
     *
     * @param propertyName название свойства
     * @return карта
     */
    public Map getMap(String propertyName) {
        return getValue(propertyName, Map.class);
    }

    /**
     * Получение обертки для карты по наименованию свойства
     *
     * @param propertyName наименование свойства
     * @return обертка для карты
     */
    public <T extends JsonMapWrapper> T getJsonMapWrapper(String propertyName, Class<T> wrapperClass) {
        Map includedMap = getMap(propertyName);
        if (includedMap == null) {
            return null;
        }
        return obtainJsonMapWrapperByMap(includedMap, wrapperClass);
    }

    /**
     * Получение списка оберток карт (бинов) по наименованию свойства.
     *
     * @param propertyName Наименование свойства
     * @param wrapperClass Тип обертки (бина)
     * @param <T>          Тип обертки (бина)
     * @return Список оберток карт (бинов)
     */
    public <T extends JsonMapWrapper> List<T> getJsonMapWrapperList(String propertyName, Class<T> wrapperClass) {
        List includedList = getList(propertyName);
        if (includedList == null) {
            return null;
        }
        List<T> result = new ArrayList<>();
        for (Object object : includedList) {
            if (object != null && object instanceof Map) {
                Map propertyMap = (Map) object;
                result.add(obtainJsonMapWrapperByMap(propertyMap, wrapperClass));
            }
        }
        return result;
    }

    /**
     * Получение списка оберток списков по нименованию свойства.
     *
     * @param propertyName Наименование свойства
     * @param wrapperClass Тип обертки
     * @param <T>          Тип обертки
     * @return Список списков оберток
     */
    public <T extends JsonListWrapper> List<T> getJsonListWrapperList(String propertyName, Class<T> wrapperClass) {
        List includedList = getList(propertyName);
        if (includedList == null) {
            return null;
        }
        List<T> result = new ArrayList<>();
        for (Object object : includedList) {
            if (object != null && object instanceof List) {
                List propertyList = (List) object;
                result.add(obtainJsonListWrapperByList(propertyList, wrapperClass));
            }
        }
        return result;
    }

    /**
     * Поучение обертки списка на основании списка
     *
     * @param propertyList список
     * @param wrapperClass класс обертки
     * @param <T>          класс обертки
     * @return объект обертки
     */
    private static <T extends JsonListWrapper> T obtainJsonListWrapperByList(List propertyList, Class<T> wrapperClass) {
        try {
            T includedObject = wrapperClass.newInstance();
            includedObject.setData(propertyList);
            return includedObject;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Получение обертки карты на основании карты
     *
     * @param map          карта
     * @param wrapperClass класс обертки
     * @param <T>          класс обертки
     * @return объект обертки
     */
    public static <T extends JsonMapWrapper> T obtainJsonMapWrapperByMap(Map map, Class<T> wrapperClass) {
        try {
            T includedObject = wrapperClass.newInstance();
            if(map == null) {
                includedObject.setProperties(new HashMap<>());
            } else {
                includedObject.setProperties(map);
            }
            return includedObject;
            //TODO: можно перезаписывать в карту
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Получение списка по названию свойства
     *
     * @param propertyName название свойства
     * @return список
     */
    public List getList(String propertyName) {
        return getValue(propertyName, List.class);
    }

    /**
     * Получение значения свойства по наименованию свойства приведенного к конкртеному типу.
     *
     * @param propertyName наименование свойствк
     * @param neededType   тип, к которому нужно привести значение свойства
     * @param <T>          тип, к которому нужно привести значение свойства
     * @return значение свойства
     */
    public <T> T getValue(String propertyName, Class<T> neededType) {
        Object value = properties.get(propertyName);
        if (neededType.isInstance(value)) {
            return (T) value;
        }
        return null;
    }
}
