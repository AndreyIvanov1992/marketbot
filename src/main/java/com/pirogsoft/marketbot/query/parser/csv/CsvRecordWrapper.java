package com.pirogsoft.marketbot.query.parser.csv;

import org.apache.commons.csv.CSVRecord;

public interface CsvRecordWrapper {

    /**
     * Установка csv строки
     *
     * @param csvRecord Csv строка
     */
    void setCsvRecord(CSVRecord csvRecord);

    /**
     * Получение значения ячейки в String
     *
     * @param propertyIndex Индекс ячейки
     * @return Значения ячейки в String
     */
    String getString(int propertyIndex);

    /**
     * Получение значения ячейки в int
     * @param propertyIndex
     * @return Значения ячейки в int
     */
    Integer getInteger(int propertyIndex);
}
