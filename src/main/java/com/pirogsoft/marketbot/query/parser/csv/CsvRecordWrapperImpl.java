package com.pirogsoft.marketbot.query.parser.csv;

import org.apache.commons.csv.CSVRecord;

/**
 * Обертка для строки csv
 * Created by Andrey on 31.05.2018.
 */
public class CsvRecordWrapperImpl implements CsvRecordWrapper {

    /**
     * Объект csv строки
     */
    private CSVRecord csvRecord;

    /**
     * Установка csv строки
     *
     * @param csvRecord Csv строка
     */
    public void setCsvRecord(CSVRecord csvRecord) {
        this.csvRecord = csvRecord;
    }

    /**
     * Получение значения ячейки в String
     *
     * @param propertyIndex Индекс ячейки
     * @return Значения ячейки в String
     */
    public String getString(int propertyIndex) {
        try {
            return csvRecord.get(propertyIndex);
        } catch (IndexOutOfBoundsException e) {
            return "";
        }
    }

    /**
     * Получение значения ячейки с обрезанными пробелами и без ковычек
     *
     * @param propertyIndex Индекс ячейки
     * @return Значение ячейки без ковычек
     */
    public String getStringWithoutQuotes(int propertyIndex) {
        String value = getString(propertyIndex);
        if(value == null) {
            return null;
        }
        String trimmedValue = value.trim();
        if(trimmedValue.isEmpty() || trimmedValue.equals("\"")) {
            return value;
        }
        if (trimmedValue.charAt(0) == '"' && trimmedValue.charAt(trimmedValue.length() - 1) == '"') {
            return trimmedValue.substring(1, trimmedValue.length() - 1);
        }
        return value;
    }


    /**
     * Получение значения ячейки в int
     *
     * @param propertyIndex
     * @return Значения ячейки в int
     */
    public Integer getInteger(int propertyIndex) {
        String stringValue = getString(propertyIndex);
        if (stringValue == null) {
            return null;
        }
        return Integer.valueOf(stringValue);
    }
}

