package com.pirogsoft.marketbot.query.parser.csv;

import com.pirogsoft.marketbot.query.ResponseParser;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.http.HttpResponse;
import org.springframework.beans.factory.annotation.Required;

import java.io.*;

/**
 * Парсер ответа в коллекцию из оберток csv строк
 * Created by Andrey on 31.05.2018.
 *
 * @param <T> класс csv строки
 */
public class CsvResponseParser<T extends CsvRecordWrapper> implements ResponseParser<CsvRecordWrappersCollection> {

    /**
     * Класс csv строки
     */
    private Class<T> csvRecordWrapperClass;

    /**
     * Разделитель
     */
    private char delimiter;

    /**
     * Установка класса csv строки
     *
     * @param csvRecordWrapperClass класс csv строки
     */
    @Required
    public void setCsvRecordWrapperClass(Class<T> csvRecordWrapperClass) {
        this.csvRecordWrapperClass = csvRecordWrapperClass;
    }

    /**
     * Устанговка разделителя
     * @param delimiter разделитель
     */
    @Required
    public void setDelimiter(char delimiter) {
        this.delimiter = delimiter;
    }


    @Override
    public CsvRecordWrappersCollection parseResponse(HttpResponse response, Class<CsvRecordWrappersCollection> responseBeanClass) throws IOException {
        CSVFormat csvFormat = CSVFormat.newFormat(delimiter);
        CSVParser csvParser = csvFormat.parse(new InputStreamReader(response.getEntity().getContent()));
        CsvRecordWrappersCollection recordWrappersCollection = new CsvRecordWrappersCollection();
        recordWrappersCollection.setCsvParser(csvParser);
        recordWrappersCollection.setCsvRecordClass(csvRecordWrapperClass);
        return recordWrappersCollection;
    }
}
