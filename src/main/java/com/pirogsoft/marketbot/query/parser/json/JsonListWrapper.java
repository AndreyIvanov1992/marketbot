package com.pirogsoft.marketbot.query.parser.json;

import java.util.List;

/**
 * Оббертка для списка, в котором по номеру в списке можно определить смысл значения.
 * Например 0 - цена, 1 - номер в очереди
 * Created by AnIvanov on 02.05.2018.
 */
public class JsonListWrapper {

    /**
     * Список данных
     */
    private List data;

    public void setData(List data) {
        this.data = data;
    }

    /**
     * Получение строки по номеру в списке
     *
     * @param index номер в списке
     * @return строка
     */
    public String getString(int index) {
        return getValue(index, String.class);
    }

    /**
     * Получение целого числа по номеру в списке, конвертируя его из double
     *
     * @param index номер в списке
     * @return целое число
     */
    public Integer getDoubleAsInteger(int index) {
        Double doubleValue = getDouble(index);
        if(doubleValue == null) {
            return null;
        }
        return getDouble(index).intValue();
    }

    /**
     * Получение вещественного числа по номеру в списке
     *
     * @param index номер в списке
     * @return целое число
     */
    public Double getDouble(int index) {
        return getValue(index, Double.class);
    }

    /**
     * Получение значения свойства по номеру в списке, приведенного к конкртеному типу.
     *
     * @param index      номер в списке
     * @param neededType тип, к которому нужно привести значение свойства
     * @param <T>        тип, к которому нужно привести значение свойства
     * @return значение свойства
     */
    public <T> T getValue(int index, Class<T> neededType) {
        Object value = data.get(index);
        if (neededType.isInstance(value)) {
            return (T) value;
        }
        return null;
    }
}
