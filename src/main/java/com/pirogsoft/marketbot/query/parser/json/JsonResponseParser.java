package com.pirogsoft.marketbot.query.parser.json;

import com.google.gson.Gson;
import com.pirogsoft.marketbot.LogUtils;
import com.pirogsoft.marketbot.query.ResponseParser;
import com.pirogsoft.marketbot.query.service.api.ResponseStringParsesService;
import org.apache.http.HttpResponse;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.Map;

/**
 * Парсит ответ как JSON.
 * @param <T> Оббертка для карты ответа (бин ответа)
 */
@Component
public class JsonResponseParser<T extends JsonMapWrapper> implements ResponseParser<T> {

    public static Logger logger = Logger.getLogger(JsonResponseParser.class);

    /**
     * Сервис для преобразования объекта HTTP ответа в строку
     */
    private ResponseStringParsesService service;

    /**
     * Парсит HttpResponse в карту, если в ответе JSON объект
     *
     * @param response объект HTTP ответа
     * @return карта основанная на JSON объекте, обернутая в объект
     * @throws IOException ошибка ввода/вывода
     */
    @Override
    public T parseResponse(HttpResponse response, Class<T> jsonMapWrapperType) throws IOException {
        Gson gson = new Gson();
        String responseString = service.obtainStringResponse(response);

        logger.debug(LogUtils.obtainLogMessage("response: " + responseString));

        Map responseMap = gson.fromJson(responseString, Map.class);
        return JsonMapWrapper.obtainJsonMapWrapperByMap(responseMap, jsonMapWrapperType);
    }

    /**
     * Устанавливает сервис для преобразования объекта HTTP ответа в строку
     *
     * @param service Сервис для преобразования объекта HTTP ответа в строку
     */
    @Autowired
    public void setService(ResponseStringParsesService service) {
        this.service = service;
    }
}
