package com.pirogsoft.marketbot.query.parser.csv;

import com.pirogsoft.marketbot.LogUtils;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.log4j.Logger;

import java.util.Iterator;

/**
 * Коллекция оберток csv строк
 * Created by Andrey on 31.05.2018.
 *
 * @param <T> класс csv строки
 */
public class CsvRecordWrappersCollection<T extends CsvRecordWrapper> implements Iterable<T> {

    public static Logger logger = Logger.getLogger(CsvRecordWrappersCollection.class);

    /**
     * Csv парсер из библиотеки apache
     */
    private CSVParser csvParser;

    /**
     * Класс csv строки
     */
    private Class<T> CsvRecordClass;

    /**
     * Устанавливает csvParser
     *
     * @param csvParser csvParser
     */
    public void setCsvParser(CSVParser csvParser) {
        this.csvParser = csvParser;
    }

    /**
     * Установка класс csv строки
     *
     * @param csvRecordClass класс csv строки
     */
    public void setCsvRecordClass(Class<T> csvRecordClass) {
        CsvRecordClass = csvRecordClass;
    }

    @Override
    public Iterator<T> iterator() {
        return new Iterator<T>() {

            private Iterator<CSVRecord> iterator = csvParser.iterator();

            private boolean first = true;

            @Override
            public boolean hasNext() {
                return iterator.hasNext();
            }

            @Override
            public T next() {
                if(first) {
                    first = false;
                    iterator.next();
                }
                T csvRecordWrapper = null;
                try {
                    csvRecordWrapper = CsvRecordClass.newInstance();
                } catch (Exception e) {
                    logger.error(LogUtils.obtainLogMessage(""), e);
                }
                csvRecordWrapper.setCsvRecord(iterator.next());
                return csvRecordWrapper;
            }
        };
    }
}
