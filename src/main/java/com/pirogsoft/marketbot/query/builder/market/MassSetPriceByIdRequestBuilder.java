package com.pirogsoft.marketbot.query.builder.market;

import com.pirogsoft.marketbot.query.beans.market.request.masssetpricebyidrequest.MassSetPriceByIdRequestParametersBean;

/**
 * Строитель запросов для массового изменения цен.
 * @author AnIvanov
 */
public class MassSetPriceByIdRequestBuilder extends AbstractSecurableMarketPostRequestBuilder<MassSetPriceByIdRequestParametersBean> {

    /**
     * Основная часть URL сервиса
     */
    public static final String MAIN_PART_URL= "/api/MassSetPriceById/";

    @Override
    protected String buildUrlMainPart(MassSetPriceByIdRequestParametersBean params) {
        return MAIN_PART_URL;
    }
}
