package com.pirogsoft.marketbot.query.builder.market;

import com.pirogsoft.marketbot.query.beans.market.request.processorder.ProcessOrderParametersBean;
import org.apache.http.client.methods.HttpGet;

import java.text.MessageFormat;

/**
 * Created by AnIvanov on 03.05.2018.
 */
public class ProcessOrderRequestBuilder extends AbstractSecurableMarketRequestBuilder<HttpGet, ProcessOrderParametersBean> {

    /**
     * Шаблон для основной части URL сервиса
     */
    public static final String MAIN_PART_URL_PATTERN = "/api/ProcessOrder/{0}/{1}/{2}";

    @Override
    protected String buildUrlMainPart(ProcessOrderParametersBean params) {
        return MessageFormat.format(MAIN_PART_URL_PATTERN, params.getClassid(), params.getInstanceid(), Integer.toString(params.getPrice()));
    }

}
