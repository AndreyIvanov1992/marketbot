package com.pirogsoft.marketbot.query.builder.market;

import com.pirogsoft.marketbot.query.builder.AbstractRequestBuilder;
import org.apache.http.client.methods.HttpGet;

public class TestIpRequrstBuilder extends AbstractRequestBuilder<HttpGet,Object> {

    public static final String URL = "https://api.ipify.org/?format=json";

    @Override
    protected String buildUrl(Object params) {
        return URL;
    }
}
