package com.pirogsoft.marketbot.query.builder.market;

import com.pirogsoft.marketbot.query.beans.market.request.common.MarketSecurableRequestParametersBean;
import org.apache.http.client.methods.HttpRequestBase;

/**
 * Защищенный запрос на маркет (запрос с передачей ключа)
 * Created by AnIvanov on 16.04.2018.
 */
abstract public class AbstractSecurableMarketRequestBuilder<T extends HttpRequestBase, S extends MarketSecurableRequestParametersBean> extends AbstractMarketRequestBuilder<T, S> {
    @Override
    protected String buildUrl(S params) {
        return params.getGameApp().getMarketApiUrl() + buildUrlMainPart(params) + obtainKeyQueryPart(params);
    }

    /**
     * Строит часть запроса с ключем.
     *
     * @param params объект параметров запроса
     * @return часть запроса с ключем
     */
    private String obtainKeyQueryPart(S params) {
        return "?key=" + params.getMarketKey();
    }
}
