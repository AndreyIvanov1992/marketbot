package com.pirogsoft.marketbot.query.builder.steam;

import com.pirogsoft.marketbot.query.beans.steam.request.PriceOverviewRequestBean;
import com.pirogsoft.marketbot.query.builder.AbstractRequestBuilder;
import org.apache.http.client.methods.HttpGet;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.MessageFormat;

/**
 * Строитель запроса в стим по цене предмета
 * Created by AnIvanov on 10.05.2018.
 */
public class PriceOverviewRequestBuilder extends AbstractRequestBuilder<HttpGet, PriceOverviewRequestBean> {

    /**
     * шаблон URL
     */
    public static final String URL_PATTERN = "https://steamcommunity.com/market/priceoverview/?appid={0}&market_hash_name={1}&currency={2}";

    @Override
    protected String buildUrl(PriceOverviewRequestBean params) {
        try {
            return MessageFormat.format(URL_PATTERN, params.getGameApp().getSteamAppId(), URLEncoder.encode(params.getMarketHashName(), "UTF-8"), params.getCurrency().getCode());
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }
}
