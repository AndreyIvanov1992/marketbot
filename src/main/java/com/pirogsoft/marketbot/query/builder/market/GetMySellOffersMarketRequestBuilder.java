package com.pirogsoft.marketbot.query.builder.market;

import com.pirogsoft.marketbot.query.beans.market.request.common.MarketSecurableRequestParametersBean;
import org.apache.http.client.methods.HttpGet;

/**
 * Строитель запроса на Получение только своих предметов, которые находятся на продаже на любом месте в очереди.
 * Created by AnIvanov on 17.04.2018.
 */
public class GetMySellOffersMarketRequestBuilder extends AbstractSecurableMarketRequestBuilder<HttpGet,MarketSecurableRequestParametersBean>{

    public static final String URL_MAIN_PART = "/api/GetMySellOffers/";

    @Override
    protected String buildUrlMainPart(MarketSecurableRequestParametersBean params) {
        return URL_MAIN_PART;
    }

}
