package com.pirogsoft.marketbot.query.builder.market;

import com.pirogsoft.marketbot.query.builder.AbstractRequestBuilder;
import com.pirogsoft.marketbot.query.beans.market.request.common.MarketParametersBean;
import org.apache.http.client.methods.HttpRequestBase;

/**
 * Запрос на маркет.
 *
 * Created by AnIvanov on 16.04.2018.
 */
abstract public class AbstractMarketRequestBuilder<T extends HttpRequestBase, S extends MarketParametersBean> extends AbstractRequestBuilder<T, S> {

    @Override
    protected String buildUrl(S params) {
        return params.getGameApp().getMarketApiUrl() + buildUrlMainPart(params);
    }

    /**
     * Строительство и получение основной части URL.
     *
     * @return основная часть URL
     */
    protected abstract String buildUrlMainPart(S params);
}
