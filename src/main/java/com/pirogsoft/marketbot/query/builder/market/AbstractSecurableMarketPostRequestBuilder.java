package com.pirogsoft.marketbot.query.builder.market;

import com.pirogsoft.marketbot.query.beans.market.request.common.MarketSecurableRequestParametersBean;
import org.apache.http.client.methods.HttpPost;

/**
 * Абстрактный строитель защищенного пост запроса на маркет.
 * @param <S> Тип бина параметров
 */
public abstract class AbstractSecurableMarketPostRequestBuilder<S extends MarketSecurableRequestParametersBean> extends AbstractSecurableMarketRequestBuilder<HttpPost, S> {

}
