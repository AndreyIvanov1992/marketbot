package com.pirogsoft.marketbot.query.builder;

import com.pirogsoft.marketbot.query.ProxyConfig;
import com.pirogsoft.marketbot.query.QueryBuilder;
import org.apache.http.HttpHost;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpRequestBase;

import java.net.URI;

/**
 * Абстрактный строитель запросов
 * Created by AnIvanov on 16.04.2018.
 */
abstract public class AbstractRequestBuilder<T extends HttpRequestBase, S> implements QueryBuilder<T, S> {

    /**
     * Создатель запроса. Делает все кроме формирования URL
     */
    private QueryBuilder<T, S> requestCreator;

    /**
     * Флаг использования прокси
     */
    boolean useProxy;

    /**
     * Конфигурация прокси
     */
    ProxyConfig proxyConfig;

    public boolean isUseProxy() {
        return useProxy;
    }

    public void setUseProxy(boolean useProxy) {
        this.useProxy = useProxy;
    }

    public ProxyConfig getProxyConfig() {
        return proxyConfig;
    }

    public void setProxyConfig(ProxyConfig proxyConfig) {
        this.proxyConfig = proxyConfig;
    }

    @Override
    public T obtainRequest(S params) {
        T httpRequest = requestCreator.obtainRequest(params);
        if (useProxy) {
            acceptProxy(httpRequest);
        }
        String url = buildUrl(params);
        httpRequest.setURI(URI.create(url));
        return httpRequest;
    }

    /**
     * Полчение создателя запроса
     *
     * @return Создатель запроса
     */
    public QueryBuilder<T, S> getRequestCreator() {
        return requestCreator;
    }

    /**
     * Установка создателя запроса
     *
     * @param requestCreator Создатель запроса
     */
    public void setRequestCreator(QueryBuilder<T, S> requestCreator) {
        this.requestCreator = requestCreator;
    }

    /**
     * Создание URL запроса
     *
     * @param params объект параметров запроса
     * @return URL запроса
     */
    protected abstract String buildUrl(S params);

    /**
     * Установка прокси в запрос
     *
     * @param httpRequest запрос
     */
    private void acceptProxy(T httpRequest) {
        HttpHost proxy = new HttpHost(proxyConfig.getHostname(), proxyConfig.getPort(), proxyConfig.getScheme());
        RequestConfig config = RequestConfig.custom()
                .setProxy(proxy)
                .setConnectionRequestTimeout(10000)
                .build();
        httpRequest.setConfig(config);
    }
}
