package com.pirogsoft.marketbot.query.builder.market;

import com.pirogsoft.marketbot.query.beans.market.request.itemdb.ItemdbRequestParametersBean;
import org.apache.http.client.methods.HttpGet;

import java.text.MessageFormat;

/**
 * Строитель запроса на получение БД маркета в формате CSV
 */
public class ItemdbMarketRequestBuilder extends AbstractMarketRequestBuilder<HttpGet, ItemdbRequestParametersBean>{

    public static final String MAIN_PART_URL_PATTERN = "/itemdb/{0}";

    @Override
    protected String buildUrlMainPart(ItemdbRequestParametersBean params) {
        return MessageFormat.format(MAIN_PART_URL_PATTERN, params.getDbName());
    }
}
