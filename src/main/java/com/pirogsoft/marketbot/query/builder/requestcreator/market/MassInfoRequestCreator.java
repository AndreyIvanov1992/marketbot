package com.pirogsoft.marketbot.query.builder.requestcreator.market;

import com.pirogsoft.marketbot.query.builder.requestcreator.AbstractParamsPostRequestCreator;
import com.pirogsoft.marketbot.query.beans.market.request.massinfo.MassInfoRequestParametersBean;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * Создатель запроса на получение массовой информации по предметам
 * Created by AnIvanov on 20.04.2018.
 */
@Component
public class MassInfoRequestCreator extends AbstractParamsPostRequestCreator<MassInfoRequestParametersBean> {

    @Override
    protected List<NameValuePair> buildParams(MassInfoRequestParametersBean params) {
        List<NameValuePair> urlParameters = new ArrayList<>();
        urlParameters.add(new BasicNameValuePair("list", obtainListParameter(params)));
        return urlParameters;
    }

    /**
     * Получение значение post-параметра list для этого запроса
     *
     * @param params параметры запроса
     * @return значения post-параметра list
     */
    private String obtainListParameter(MassInfoRequestParametersBean params) {
        StringBuilder result = new StringBuilder("");
        boolean first = true;
        for (String classInstanceId : params.getList()) {
            if (!first) {
                result.append(",");
            }
            result.append(classInstanceId);
            first = false;
        }
        return result.toString();
    }
}
