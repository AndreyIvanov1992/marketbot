package com.pirogsoft.marketbot.query.builder.requestcreator.market;

import com.pirogsoft.marketbot.query.builder.requestcreator.AbstractParamsPostRequestCreator;
import com.pirogsoft.marketbot.query.beans.market.request.masssetpricebyidrequest.IdPricePair;
import com.pirogsoft.marketbot.query.beans.market.request.masssetpricebyidrequest.MassSetPriceByIdRequestParametersBean;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.springframework.stereotype.Component;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Создатель запроса на массовое изменение цен
 * Created by AnIvanov on 20.04.2018.
 */
@Component("massSetPriceByIdRequestCreator")
public class MassSetPriceByIdRequestCreator extends AbstractParamsPostRequestCreator<MassSetPriceByIdRequestParametersBean> {

    /**
     * Патеерн для названия параметров
     */
    public static final String PARAM_NAME_PATTERN = "list[{0}]";

    @Override
    protected List<NameValuePair> buildParams(MassSetPriceByIdRequestParametersBean params) {
        List<NameValuePair> urlParameters = new ArrayList<>();
        for(IdPricePair pair : params.getList()) {
            String paramName = MessageFormat.format(PARAM_NAME_PATTERN, Integer.toString(pair.getId()));
            urlParameters.add(new BasicNameValuePair(paramName, Integer.toString(pair.getPrice())));
        }
        return urlParameters;
    }
}
