package com.pirogsoft.marketbot.query.builder.requestcreator;

import com.pirogsoft.marketbot.query.QueryBuilder;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;

import org.apache.http.client.methods.HttpPost;

import java.io.UnsupportedEncodingException;
import java.util.List;

/**
 * Абстрактный создатель POST запроса
 * @param <S> Тип бина параметров запроса
 */
public abstract class AbstractParamsPostRequestCreator<S> implements QueryBuilder<HttpPost, S> {

    @Override
    public HttpPost obtainRequest(S params) {
        HttpPost httpPost = new HttpPost();
        List<NameValuePair> urlParameters = buildParams(params);
        try {
            httpPost.setEntity(new UrlEncodedFormEntity(urlParameters));
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
        return httpPost;
    }

    /**
     * Добавление POST параметров запросу
     * @param params бин параметров
     */
    protected abstract List<NameValuePair> buildParams(S params);
}
