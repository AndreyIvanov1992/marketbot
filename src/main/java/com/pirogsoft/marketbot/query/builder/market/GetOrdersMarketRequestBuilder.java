package com.pirogsoft.marketbot.query.builder.market;

import com.pirogsoft.marketbot.query.beans.market.request.getorders.GetOrdersRequestParametersBean;
import org.apache.http.client.methods.HttpGet;

import java.text.MessageFormat;

/**
 * Строитель запроса на получение "Моих ордеров"
 * Created by AnIvanov on 06.05.2018.
 */
public class GetOrdersMarketRequestBuilder extends AbstractSecurableMarketRequestBuilder<HttpGet,GetOrdersRequestParametersBean>{

    /**
     * Шаблон для основной части URL сервиса, в случае если передали параметр page
     */
    public static final String MAIN_PART_URL_PATTERN = "/api/GetOrders/{0}/";

    /**
     * URL сервиса, в случае, если параметр page не передали
     */
    public static final String URL_MAIN_PART = "/api/GetOrders/";

    @Override
    protected String buildUrlMainPart(GetOrdersRequestParametersBean params) {
        if(params.getPage() == null) {
            return URL_MAIN_PART;
        }
        return MessageFormat.format(MAIN_PART_URL_PATTERN, params.getPage());

    }
}
