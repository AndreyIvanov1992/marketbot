package com.pirogsoft.marketbot.query.builder.market;

import com.pirogsoft.marketbot.query.beans.market.request.common.MarketParametersBean;
import org.apache.http.client.methods.HttpGet;

import java.text.MessageFormat;

/**
 * Строитель запроса на получение названия файла с актуальной базой предметов с маркета в формате csv
 * Created by Andrey on 25.05.2018.
 */
public class CurrentDbRequestBuilder extends AbstractMarketRequestBuilder<HttpGet, MarketParametersBean>{

    public static final String MAIN_PART_URL_PATTERN = "/itemdb/current_{0}.json";

    @Override
    protected String buildUrlMainPart(MarketParametersBean params) {
        return MessageFormat.format(MAIN_PART_URL_PATTERN, params.getGameApp().getSteamAppId());
    }
}
