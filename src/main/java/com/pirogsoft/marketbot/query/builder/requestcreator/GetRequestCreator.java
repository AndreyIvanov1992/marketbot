package com.pirogsoft.marketbot.query.builder.requestcreator;

import com.pirogsoft.marketbot.query.QueryBuilder;
import org.apache.http.client.methods.HttpGet;
import org.springframework.stereotype.Component;

/**
 * Создатель get-запроса
 * Created by AnIvanov on 20.04.2018.
 */
@Component("getRequestCreator")
public class GetRequestCreator<S> implements QueryBuilder<HttpGet, S> {

    @Override
    public HttpGet obtainRequest(S params) {
        return new HttpGet();
    }
}
