package com.pirogsoft.marketbot.query.builder.market;

import com.pirogsoft.marketbot.query.beans.market.request.massinfo.MassInfoRequestParametersBean;

import java.text.MessageFormat;

/**
 * Строитель запросов для массовой
 * Created by AnIvanov on 19.04.2018.
 */
public class MassInfoMarketRequestBuilder extends AbstractSecurableMarketPostRequestBuilder<MassInfoRequestParametersBean> {

    /**
     * Шаблон для основной части URL сервиса
     */
    public static final String MAIN_PART_URL_PATTERN = "/api/MassInfo/{0}/{1}/{2}/{3}";

    @Override
    protected String buildUrlMainPart(MassInfoRequestParametersBean params) {
        return MessageFormat.format(MAIN_PART_URL_PATTERN, params.getSell().ordinal(), params.getBuy().ordinal(), params.getHistory().ordinal(), params.getInfo().ordinal());
    }
}
