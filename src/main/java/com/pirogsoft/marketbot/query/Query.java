package com.pirogsoft.marketbot.query;

import com.pirogsoft.marketbot.ContextContainer;
import com.pirogsoft.marketbot.LogUtils;
import com.pirogsoft.marketbot.query.beans.TestIpResponse;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.client.utils.HttpClientUtils;
import org.apache.http.conn.ConnectionPoolTimeoutException;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.log4j.Logger;

import java.io.IOException;

/**
 * Класс для постройки, отправки запроса и получение результата.
 *
 * @param <S> Тип бина параметров запроса
 * @param <U> Тип бина ответа
 */
public class Query<S, U> {

    static Logger logger = Logger.getLogger(Query.class);

    /**
     * Строитель запросов. Преобразователь объекта параметров запроса в объект HTTP запрсоа
     */
    private QueryBuilder<? extends HttpRequestBase, S> queryBuilder;

    /**
     * Парсер ответа. Преобразователь объекта HTTP запроса в объект бина ответа
     */
    private ResponseParser<U> responseParser;

    /**
     * Тип объекта ответа
     */
    private Class<U> responseType;

    private int lastReponseStatus;

    private String url;

    public Query() {
    }

    /**
     * Получение строителя запросов
     *
     * @return Строитель запросов
     */
    public QueryBuilder<? extends HttpRequestBase, S> getQueryBuilder() {
        return queryBuilder;
    }

    public int getLastReponseStatus() {
        return lastReponseStatus;
    }

    /**
     * Установка строителя запросов
     *
     * @param queryBuilder Строитель запросов
     */
    public void setQueryBuilder(QueryBuilder<? extends HttpRequestBase, S> queryBuilder) {
        this.queryBuilder = queryBuilder;
    }

    /**
     * Установка парсера ответов
     *
     * @param responseParser Парсер ответов
     */
    public void setResponseParser(ResponseParser<U> responseParser) {
        this.responseParser = responseParser;
    }

    /**
     * Установка класса ответа
     *
     * @param responseType класс ответа
     */
    public void setResponseType(Class<U> responseType) {
        this.responseType = responseType;
    }

    /**
     * Выполняет HTTP запрос на основе параметров. Результат парсит в необходимый объект.
     *
     * @param params объект параметров запорса
     * @return объект ответа
     * @throws IOException ошибка ввода/вывода, возникшая при выполнении запроса
     */
    public U execute(S params) throws IOException {
        CloseableHttpResponse response = null;
        try {
            CloseableHttpClient client = HttpClientBuilder.create().build();
            HttpRequestBase request = queryBuilder.obtainRequest(params);
            url = request.getURI().toString();
            logger.debug(LogUtils.obtainLogMessage(request.toString()));

            response = client.execute(request);
            lastReponseStatus = response.getStatusLine().getStatusCode();
            if (lastReponseStatus != HttpStatus.SC_OK) {
                logger.error(LogUtils.obtainLogMessage("request: " + request));
                logger.error(LogUtils.obtainLogMessage("responce: " + response));
                if(!parseIfNotOk()) {
                    return null;
                }
            }
            logger.debug(LogUtils.obtainLogMessage(response.toString()));
            return responseParser.parseResponse(response, responseType);
        } finally {
            response.close();
        }
    }

    protected boolean parseIfNotOk() {
        return true;
    }

    @Override
    public String toString() {
        return "Query{" +
                "lastReponseStatus=" + lastReponseStatus +
                ", url='" + url + '\'' +
                '}';
    }
}
