package com.pirogsoft.marketbot.query;

import org.apache.http.HttpResponse;

import java.io.IOException;

/**
 * Интерфейс для парсинга ответа в объект определенного типа
 *
 * @param <T> тип объекьа, в который нужно преобразовать объект
 */
public interface ResponseParser<T> {

    /**
     * парсит объект ответа в нужный тип
     *
     * @param response объект HTTP ответа
     * @param responseBeanClass тип объекьа, в который нужно преобразовать объект
     * @return результирующий бин
     * @throws IOException ошибка ввода вывода.
     */
    T parseResponse(HttpResponse response, Class<T> responseBeanClass) throws IOException;
}
