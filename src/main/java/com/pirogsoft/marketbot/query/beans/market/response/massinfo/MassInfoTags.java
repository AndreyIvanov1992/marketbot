package com.pirogsoft.marketbot.query.beans.market.response.massinfo;

import com.pirogsoft.marketbot.query.parser.json.JsonMapWrapper;

/**
 * Тег предмета из steam
 * Created by AnIvanov on 02.05.2018.
 */
public class MassInfoTags extends JsonMapWrapper {

    /**
     * Название пропертей
     */
    public static class PropertyNames {

        public static final String INTERNAL_NAME = "internal_name";

        public static final String NAME = "name";

        public static final String CATEGORY = "category";

        public static final String COLOR = "color";

        public static final String CATEGORY_NAME = "category_name";
    }

    /**
     * Получение внутреннего названия тега
     *
     * @return внутреннее название тега
     */
    public String getInternalName() {
        return getString(PropertyNames.INTERNAL_NAME);
    }

    /**
     * Получение названия тега
     *
     * @return наззвание тега
     */
    public String getName() {
        return getString(PropertyNames.NAME);
    }

    /**
     * Получение категории тега
     *
     * @return категория тега
     */
    public String getCategory() {
        return getString(PropertyNames.CATEGORY);
    }

    /**
     * Получение цвета тега
     *
     * @return цвет тега
     */
    public String getColor() {
        return getString(PropertyNames.COLOR);
    }

    /**
     * Получение названия каегории тега
     *
     * @return
     */
    public String getCategoryName() {
        return getString(PropertyNames.CATEGORY_NAME);
    }
}
