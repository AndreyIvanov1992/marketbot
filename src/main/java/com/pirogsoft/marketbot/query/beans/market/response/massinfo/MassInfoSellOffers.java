package com.pirogsoft.marketbot.query.beans.market.response.massinfo;

import com.pirogsoft.marketbot.query.parser.json.JsonMapWrapper;

import java.util.List;

/**
 * Предложения о продаже предмета
 * Created by AnIvanov on 02.05.2018.
 */
public class MassInfoSellOffers extends JsonMapWrapper {

    /**
     * Название пропертей
     */
    public static class PropertyNames {

        public static final String BEST_OFFER = "best_offer";

        public static final String OFFERS = "offers";

        public static final String MY_OFFERS = "my_offers";
    }

    /**
     * Получение цены лучшего предложения в копейках
     *
     * @return Цена лучшего предложения в копейках
     */
    public Integer getBestOffer() {
        return getDoubleAsInteger(PropertyNames.BEST_OFFER);
    }

    /**
     * Получение предложений о продаже. Рассортированы в порядке возрастания цены.
     *
     * @return Предлжения о продаже
     */
    public List<MassInfoOffer> getOffers() {
        return getJsonListWrapperList(PropertyNames.OFFERS, MassInfoOffer.class);
    }

    /**
     * Получение списка моих предложений в виде списка интеджеров, цен в копейках.
     *
     * @return список цен в копейках
     */
    public List getMyOffers() {
        return getList(PropertyNames.MY_OFFERS);
    }

}
