package com.pirogsoft.marketbot.query.beans.market.response.masssetpricebyid;

import com.pirogsoft.marketbot.query.beans.market.response.common.MarketResponseBean;

import java.util.List;

/**
 * Бин ответа на запрос для массового изменения цен по ui_id
 * Created by AnIvanov on 02.05.2018.
 */
public class MassSetPriceByIdResponseBean extends MarketResponseBean {

    /**
     * Название пропертей
     */
    public static class PropertyNames extends MarketResponseBean.PropertyNames {

        /**
         * Предметы
         */
        public static final String ITEMS = "items";
    }

    /**
     * Получение измененных предметов
     *
     * @return измененные предметы
     */
    public List<MassSetPriceByIdItem> getItems() {
        return getJsonMapWrapperList(PropertyNames.ITEMS, MassSetPriceByIdItem.class);
    }
}
