package com.pirogsoft.marketbot.query.beans.steam.request;

/**
 * Валюта в стиме
 * Created by AnIvanov on 10.05.2018.
 */
public enum SteamCurrency {

    RUB(5);

    /**
     * Код валюты в стиме
     */
    private int code;

    SteamCurrency(int code) {
        this.code = code;
    }

    /**
     * Получение кода валюты в стиме
     * @return код валюты в стиме
     */
    public int getCode() {
        return code;
    }
}
