package com.pirogsoft.marketbot.query.beans.steam.request;

import com.pirogsoft.marketbot.service.GameApp;

/**
 * Бин запроса на маркет на получение цены предмета
 * Created by AnIvanov on 10.05.2018.
 */
public class PriceOverviewRequestBean {

    /**
     * Валюта
     */
    private SteamCurrency currency;

    /**
     * id приложения
     */
    private GameApp gameApp;

    /**
     * уникальная строка для предмета
     */
    private String marketHashName;

    /**
     * Получение валюты
     *
     * @return валюта
     */
    public SteamCurrency getCurrency() {
        return currency;
    }

    /**
     * Установка валюты
     *
     * @param currency валюта
     */
    public void setCurrency(SteamCurrency currency) {
        this.currency = currency;
    }

    /**
     * Получение ID приложения
     *
     * @return ID приложения
     */
    public GameApp getGameApp() {
        return gameApp;
    }

    /**
     * Установка ID приложения
     *
     * @param gameApp ID приложения
     */
    public void setGameApp(GameApp gameApp) {
        this.gameApp = gameApp;
    }

    /**
     * Получение уникальной строка для предмета
     *
     * @return уникальная строка для предмета
     */
    public String getMarketHashName() {
        return marketHashName;
    }

    /**
     * Установка уникальной строка для предмета
     *
     * @param marketHashName уникальная строка для предмета
     */
    public void setMarketHashName(String marketHashName) {
        this.marketHashName = marketHashName;
    }
}
