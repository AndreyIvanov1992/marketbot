package com.pirogsoft.marketbot.query.beans.market.response.common;

import com.pirogsoft.marketbot.query.parser.json.JsonMapWrapper;

/**
 * Бин ответа с маркета
 * Created by AnIvanov on 01.05.2018.
 */
public class MarketResponseBean extends JsonMapWrapper {

    /**
     * Название пропертей
     */
    public static class PropertyNames {

        public static final String SUCCESS = "success";

        public static final String ERROR = "error";

    }

    /**
     * Сообщения об ошибках
     */
    public static class ErrorMessages {

        public static final String BED_KEY = "Bad KEY";
    }

    /**
     * Получение успешности выполнения запроса
     *
     * @return успешность выполнения запроса
     */
    public Boolean isSuccess() {
        return getBoolean(PropertyNames.SUCCESS);
    }

    /**
     * Получение сообщения об ошибке
     *
     * @return сообщение об ошибке
     */
    public String getError() {
        return getString(PropertyNames.ERROR);
    }

    /**
     * Проверяет битый ли ключ
     *
     * @return битый ли ключ
     */
    public boolean isBadKey() {
        String error = getError();
        return error != null && error.equals(ErrorMessages.BED_KEY);
    }
}
