package com.pirogsoft.marketbot.query.beans.market.response.massinfo;

import com.pirogsoft.marketbot.query.parser.json.JsonMapWrapper;

import java.util.List;

/**
 * Предложения о покупке предмета
 * Created by AnIvanov on 02.05.2018.
 */
public class MassInfoBuyOffers extends JsonMapWrapper {
    /**
     * Название пропертей
     */
    public static class PropertyNames {

        public static final String BEST_OFFER = "best_offer";

        public static final String OFFERS = "offers";

        public static final String MY_OFFER = "my_offer";
    }

    /**
     * Получение цены лучшего предложения в копейках
     *
     * @return Цена лучшего предложения в копейках
     */
    public Integer getBestOffer() {
        return getDoubleAsInteger(PropertyNames.BEST_OFFER);
    }


    /**
     * Получение предложений о покупке. Рассортированы в порядке убывания цены.
     *
     * @return Предлжения о покупке
     */
    public List<MassInfoOffer> getOffers() {
        return getJsonListWrapperList(PropertyNames.OFFERS, MassInfoOffer.class);
    }

    /**
     * Получение моего предложения о покупке
     *
     * @return Цена лучшего предложения в копейках
     */
    public Integer getMyOffer() {
        return getDoubleAsInteger(PropertyNames.MY_OFFER);
    }
}
