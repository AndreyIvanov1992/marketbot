package com.pirogsoft.marketbot.query.beans.market.response.masssetpricebyid;

import com.pirogsoft.marketbot.query.parser.json.JsonMapWrapper;

/**
 * Статус предмета
 * Created by AnIvanov on 02.05.2018.
 */
public class MassSetPriceByIdStatus extends JsonMapWrapper{

    /**
     * Название пропертей
     */
    public static class PropertyNames {

        public static final String RESULT = "result";

        public static final String ITEM_ID = "item_id";

        public static final String PRICE = "price";

        public static final String STATUS = "status";

        public static final String POSITION = "position";
    }

    public Boolean getResult() {
        return getBoolean(PropertyNames.RESULT);
    }

    public Integer getItemId() {
        return getDoubleAsInteger(PropertyNames.ITEM_ID);
    }

    public Double getPrice() {
        return getDouble(PropertyNames.PRICE);
    }

    public String getStatus() {
        return getString(PropertyNames.STATUS);
    }

    public Integer getPosition() {
        return getDoubleAsInteger(PropertyNames.POSITION);
    }
}
