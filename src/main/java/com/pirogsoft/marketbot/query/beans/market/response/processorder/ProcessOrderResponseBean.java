package com.pirogsoft.marketbot.query.beans.market.response.processorder;

import com.pirogsoft.marketbot.query.beans.market.response.common.MarketResponseBean;

/**
 * Created by AnIvanov on 03.05.2018.
 */
public class ProcessOrderResponseBean extends MarketResponseBean {

    /**
     * Название пропертей
     */
    public static class PropertyNames extends MarketResponseBean.PropertyNames {

        /**
         * Результат действия
         */
        public static final String WAY = "way";

        /**
         * цена за которую был куплен предмет или, если предмет не был куплен,
         * цена которая зафиксирована за этим ордеров в копейках.
         */
        public static final String PRICE = "price";

        /**
         *  ID предмета, при успешной покупке для поиска в методе Trades.
         */
        public static final String ITEM = "item";

        /**
         *  причина ошибки при покупке.
         */
        public static final String BUY_ERROR  = "buy_error";

        /**
         * При ошибке покупки дублирует данные из метода StatusOrders(
         * success — индикатор работы ордеров, суммирует два поля ниже
         * online — индикатор нахождение в онлайне
         * has_items - индикатор блокировки из-за не забранных предметов
         * (если предмет лежит более 15 минут и не забран еще). false - есть предмета, true - нет предметов, все отлично
         */
        //TODO: вынести status в отдельный объект
        public static final String STATUS  = "status";
    }

    /**
     * Сообщения об ошибках
     */
    public static class ErrorMessages extends MarketResponseBean.ErrorMessages {

        /**
         * Неправильно задана цена
         */
        public static final String WRONG_PRICE  = "wrong_price";

        /**
         * Недостаточно средств для создания ордера
         */
        public static final String MONEY  = "money";

        /**
         *  Заявка в процессе обработки, изменение невозможно
         */
        public static final String INPROCESS  = "inprocess";

        /**
         * Цена не изменилась
         */
        public static final String SAME_PRICE   = "same_price";

        /**
         * Не удалось изменить заявку (ошибка на сервере), попробуйте ещё раз
         */
        public static final String INTERNAL   = "internal";

        /**
         * Попытка создать ордер с нулевой ценой
         */
        public static final String NULL_PRICE   = "null_price";

        /**
         * Попытка купить завершилась неудачей по причине наличия предметов на вывод или нахождения в оффлайне,
         * ордер не будет размещен т.к. его цена будет выше текущего предложения.
         */
        public static final String UNABLE_TO_BUY  = "unable_to_buy";
    }

    /**
     * Результаты действия
     */
    public static final class Ways {

        /**
         * ордер создан
         */
        public static final String CREATED = "created";

        /**
         * ордер исполнен
         */
        public static final String BUYED = "buyed";

        /**
         * ордер обновлен
         */
        public static final String UPDATED  = "updated";

        /**
         * ордер удален
         */
        public static final String DELETED  = "deleted";
    }

    /**
     * Получение результата действия
     * @return результат действия
     */
    public String getWay() {
        return getString(PropertyNames.WAY);
    }

    /**
     * Получение цены в копейках (ордера или фактической покупки)
     * @return цена в копейках (ордера или фактической покупки)
     */
    public Integer getPrice() {
        return getDoubleAsInteger(PropertyNames.PRICE);
    }

    /**
     * Получение ID предмета
     * @return ID предмета
     */
    public String getItem() {
        return getString(PropertyNames.ITEM);
    }

    /**
     * Получение причина ошибки при покупке.
     * @return Причина ошибки при покупке.
     */
    public String getBuyError() {
        return getString(PropertyNames.BUY_ERROR);
    }
}
