package com.pirogsoft.marketbot.query.beans.market.request.processorder;

import com.pirogsoft.marketbot.query.beans.market.request.common.MarketSecurableRequestParametersBean;

/**
 * Бин параметров запроса на установку или обновления ордера
 * Created by AnIvanov on 03.05.2018.
 */
public class ProcessOrderParametersBean extends MarketSecurableRequestParametersBean {

    /**
     * ClassID предмета в Steam
     */
    private String classid;

    /**
     * InstanceID предмета в Steam
     */
    private String instanceid;

    /**
     * цена в копейках
     */
    private int price;

    public ProcessOrderParametersBean() {
    }

    /**
     * Получение ClassID предмета в Steam
     *
     * @return ClassID предмета в Steam
     */
    public String getClassid() {
        return classid;
    }

    /**
     * Установка ClassID предмета в Steam
     *
     * @param classid ClassID предмета в Steam
     */
    public void setClassid(String classid) {
        this.classid = classid;
    }

    /**
     * Получение InstanceID предмета в Steam
     *
     * @return InstanceID предмета в Steam
     */
    public String getInstanceid() {
        return instanceid;
    }

    /**
     * Установка InstanceID предмета в Steam
     *
     * @param instanceid InstanceID предмета в Steam
     */
    public void setInstanceid(String instanceid) {
        this.instanceid = instanceid;
    }

    /**
     * Получение цены в копейках
     *
     * @return цена в копейках
     */
    public int getPrice() {
        return price;
    }

    /**
     * Установка цены в копейках
     *
     * @param price цена в копейках
     */
    public void setPrice(int price) {
        this.price = price;
    }
}
