package com.pirogsoft.marketbot.query.beans.market.request.itemdb;

import com.pirogsoft.marketbot.query.beans.market.request.common.MarketParametersBean;

/**
 * Бин запроса на получение БД вещей с маркета в формате CSV
 */
public class ItemdbRequestParametersBean extends MarketParametersBean {

    /**
     * Название текущего файла
     */
    private String dbName;

    /**
     * Получение названия текущего файла
     *
     * @return Название текущего файла
     */
    public String getDbName() {
        return dbName;
    }

    /**
     * Установка названия текущего файла
     *
     * @param dbName Название текущего файла
     */
    public void setDbName(String dbName) {
        this.dbName = dbName;
    }
}
