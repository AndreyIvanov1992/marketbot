package com.pirogsoft.marketbot.query.beans.market.response.massinfo;

import com.pirogsoft.marketbot.query.beans.market.response.common.MarketResponseBean;

import java.util.List;

/**
 * Created by AnIvanov on 02.05.2018.
 */
public class MassInfoResponseBean extends MarketResponseBean {

    /**
     * Название пропертей
     */
    public static class PropertyNames extends MarketResponseBean.PropertyNames {

        /**
         * Результат
         */
        public static final String RESULTS = "results";
    }

    public List<MassInfoResult> getResult() {
        return getJsonMapWrapperList(PropertyNames.RESULTS, MassInfoResult.class);
    }
}
