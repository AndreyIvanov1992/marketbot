package com.pirogsoft.marketbot.query.beans.market.response.massinfo;

import com.pirogsoft.marketbot.query.parser.json.JsonMapWrapper;

/**
 * Описание предмета из steam
 * Created by AnIvanov on 02.05.2018.
 */
public class MassInfoDescription extends JsonMapWrapper {

    /**
     * Название пропертей
     */
    public static class PropertyNames {

        public static final String TYPE = "TYPE";

        public static final String VALUE = "value";

        public static final String COLOR = "color";
    }

    /**
     * Получение типа описания
     *
     * @return тип описания
     */
    public String getType() {
        return getString(PropertyNames.TYPE);
    }

    /**
     * Получение занчения описания
     *
     * @return значение описания
     */
    public String getValue() {
        return getString(PropertyNames.VALUE);
    }

    /**
     * Получение цвета описания
     *
     * @return цвет описания
     */
    public String getColor() {
        return getString(PropertyNames.COLOR);
    }
}
