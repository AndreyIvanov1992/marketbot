package com.pirogsoft.marketbot.query.beans.market.response.currentdb;

import com.pirogsoft.marketbot.query.parser.json.JsonMapWrapper;

import java.time.Instant;
import java.time.LocalDateTime;
import java.util.TimeZone;

/**
 * Бин ответа на запрос получения имени csv файла с предметами
 * Created by Andrey on 25.05.2018.
 */
public class CurrentDbResponseBean extends JsonMapWrapper {

    /**
     * Название пропертей
     */
    public static class PropertyNames {

        public static final String TIME = "time";

        public static final String DB = "db";
    }

    /**
     * Получение таймштампа получения названия файла
     *
     * @return таймштамп получения названия файла
     */
    public Integer getTimeStamp() {
        return getDoubleAsInteger(PropertyNames.TIME);
    }

    /**
     * Получение даты и времени получения названия файла
     *
     * @return дата и время получения названия файла
     */
    public LocalDateTime getDateTime() {
        return LocalDateTime.ofInstant(Instant.ofEpochSecond(getTimeStamp()),
                TimeZone.getDefault().toZoneId());
    }

    /**
     * Получение название csv файла с предметами
     * @return
     */
    public String getDb() {
        return getString(PropertyNames.DB);
    }
}
