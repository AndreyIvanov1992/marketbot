package com.pirogsoft.marketbot.query.beans.market.request.common;

/**
 * Параметры запроса на маркет, для которого нужен ключ
 * Created by AnIvanov on 05.04.2018.
 */
public class MarketSecurableRequestParametersBean extends MarketParametersBean {
    private String marketKey;

    public MarketSecurableRequestParametersBean() {
    }

    /**
     * Получение ключа api маркета
     * @return ключ api маркета
     */
    public String getMarketKey() {
        return marketKey;
    }

    /**
     * Установка ключа api маркета
     * @param marketKey ключ api маркета
     */
    public void setMarketKey(String marketKey) {
        this.marketKey = marketKey;
    }
}
