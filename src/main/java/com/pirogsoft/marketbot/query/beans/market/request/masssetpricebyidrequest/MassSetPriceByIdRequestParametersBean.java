package com.pirogsoft.marketbot.query.beans.market.request.masssetpricebyidrequest;

import com.pirogsoft.marketbot.query.beans.market.request.common.MarketSecurableRequestParametersBean;

import java.util.List;

/**
 * Бин запроса на массовую установку цен по id
 */
public class MassSetPriceByIdRequestParametersBean extends MarketSecurableRequestParametersBean {

    /**
     * Лимит количества элементов в списке, для POST заросов на маркет
     */
    public static final int MAX_LIST_ITEM_COUNT = 100;

    /**
     * Список пар ui_id - цена
     */
    private List<IdPricePair> list;

    public MassSetPriceByIdRequestParametersBean() {
    }

    /**
     * Получение списка пар id - цена
     *
     * @return список пар id - цена
     */
    public List<IdPricePair> getList() {
        return list;
    }

    /**
     * Установка списка пар id - цена
     *
     * @param list список пар id - цена
     */
    public void setList(List<IdPricePair> list) {
        this.list = list;
    }


}
