package com.pirogsoft.marketbot.query.beans.market.response.getorders;

import com.pirogsoft.marketbot.query.beans.market.response.common.MarketResponseBean;

import java.util.List;

/**
 * Бин ответа на запрос получения всех "Моих ордеров"
 * Created by AnIvanov on 06.05.2018.
 */
public class GetOrdersResponseBean extends MarketResponseBean {

    /**
     * Количество предметов на странице
     */
    public static final int COUNT_PER_PAGE = 500;

    /**
     * названия пропертей
     */
    public static class PropertyNames extends MarketResponseBean.PropertyNames {

        public static final String ORDERS = "Orders";
    }

    /**
     * Получение списка запросов на покупку
     *
     * @return список запросов на покупку
     */
    public List<MyOrder> getOrders() {
        return getJsonMapWrapperList(PropertyNames.ORDERS, MyOrder.class);
    }
}
