package com.pirogsoft.marketbot.query.beans.market.response.getmyselloffers;

import com.pirogsoft.marketbot.query.beans.market.response.common.MarketResponseBean;

import java.util.List;

/**
 * бин ответа на запрос получения всех выставленных предложений
 * Created by AnIvanov on 01.05.2018..
 */
public class GetMySellOffersResponseBean extends MarketResponseBean {

    /**
     * названия пропертей
     */
    public static class PropertyNames extends MarketResponseBean.PropertyNames {

        public static final String OFFERS = "offers";
    }

    /**
     * Получение списка оферов
     *
     * @return список оферов
     */
    public List<MySellOffer> getOffers() {
        return getJsonMapWrapperList(PropertyNames.OFFERS, MySellOffer.class);
    }
}
