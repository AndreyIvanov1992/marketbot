package com.pirogsoft.marketbot.query.beans.market.response.massinfo;

import com.pirogsoft.marketbot.common.ClassInstanceId;
import com.pirogsoft.marketbot.common.ClassInstanceIdSupport;
import com.pirogsoft.marketbot.query.parser.json.JsonMapWrapper;

/**
 * Результат запроса на получение информации сразу о нескольких предметах
 * Created by AnIvanov on 02.05.2018.
 */
public class MassInfoResult extends JsonMapWrapper implements ClassInstanceIdSupport {

    @Override
    public ClassInstanceId obtainClassInstanceId() {
        return new ClassInstanceId(getClassid(), getInstanceid());
    }

    /**
     * Название пропертей
     */
    public static class PropertyNames {

        public static final String CLASSID = "classid";

        public static final String INSTANCEID = "instanceid";

        public static final String SELL_OFFERS = "sell_offers";

        public static final String BUY_OFFERS = "buy_offers";

        public static final String HISTORY = "history";

        public static final String INFO = "info";
    }

    /**
     * Получение claasid предмета
     *
     * @return claasid предмета
     */
    public String getClassid() {
        return getString(PropertyNames.CLASSID);
    }

    /**
     * Получение instanceid предмета
     *
     * @return instanceid предмета
     */
    public String getInstanceid() {
        return getString(PropertyNames.INSTANCEID);
    }

    /**
     * Получение предложений о продаже
     *
     * @return предложения о продаже
     */
    public MassInfoSellOffers getSellOffers() {
        return getJsonMapWrapper(PropertyNames.SELL_OFFERS, MassInfoSellOffers.class);
    }

    /**
     * Получение предложений о покупке
     *
     * @return предложения о покупке
     */
    public MassInfoBuyOffers getBuyOffers() {
        return getJsonMapWrapper(PropertyNames.BUY_OFFERS, MassInfoBuyOffers.class);
    }

    /**
     * Получение истории продаж предмета
     *
     * @return история продаж предмета
     */
    public MassInfoHistory getHistory() {
        return getJsonMapWrapper(PropertyNames.HISTORY, MassInfoHistory.class);
    }

    /**
     * Получение общей информации о предмете
     *
     * @return общая информация о предмете
     */
    public MassInfoInfo getInfo() {
        return getJsonMapWrapper(PropertyNames.INFO, MassInfoInfo.class);
    }
}
