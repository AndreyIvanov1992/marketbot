package com.pirogsoft.marketbot.query.beans.market.response.massinfo;

import com.pirogsoft.marketbot.query.parser.json.JsonMapWrapper;

import java.util.List;

/**
 * Информация о предмете
 * Created by AnIvanov on 02.05.2018.
 */
public class MassInfoInfo extends JsonMapWrapper {
    /**
     * Название пропертей
     */
    public static class PropertyNames {

        public static final String OUR_MARKET_INSTANCEID = "our_market_instanceid";

        public static final String MARKET_NAME = "market_name";

        public static final String NAME = "name";

        public static final String MARKET_HASH_NAME = "market_hash_name";

        public static final String RARITY = "rarity";

        public static final String QUALITY = "quality";

        public static final String TYPE = "type";

        public static final String MTYPE = "market_hash_name";

        public static final String SLOT = "slot";

        public static final String IMAGE = "image";

        public static final String DESCRIPTION = "description";

        public static final String TAGS = "tags";

        public static final String HASH = "hash";
    }

    public String getOurMarketInstanceid() {
        return getString(PropertyNames.OUR_MARKET_INSTANCEID);
    }

    public String getMarketName() {
        return getString(PropertyNames.MARKET_NAME);
    }

    public String getName() {
        return getString(PropertyNames.NAME);
    }

    public String getMarketHashName() {
        return getString(PropertyNames.MARKET_HASH_NAME);
    }

    public String getRarity() {
        return getString(PropertyNames.RARITY);
    }

    public String getQuality() {
        return getString(PropertyNames.QUALITY);
    }

    public String getType() {
        return getString(PropertyNames.TYPE);
    }

    public String getMtype() {
        return getString(PropertyNames.MTYPE);
    }

    public String getSlot() {
        return getString(PropertyNames.SLOT);
    }

    public String getImage() {
        return getString(PropertyNames.IMAGE);
    }

    public List<MassInfoDescription> getDescription() {
        return getJsonMapWrapperList(PropertyNames.DESCRIPTION, MassInfoDescription.class);
    }

    public List<MassInfoTags> getTags() {
        return getJsonMapWrapperList(PropertyNames.TAGS, MassInfoTags.class);
    }

    public String getHash() {
        return getString(PropertyNames.HASH);
    }
}
