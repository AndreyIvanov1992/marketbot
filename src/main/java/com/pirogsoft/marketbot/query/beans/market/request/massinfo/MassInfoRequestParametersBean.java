package com.pirogsoft.marketbot.query.beans.market.request.massinfo;

import com.pirogsoft.marketbot.query.beans.market.request.common.MarketSecurableRequestParametersBean;

import java.util.List;

/**
 * бин для запроса к маркету на получение информации о предметах
 * Created by AnIvanov on 18.04.2018.
 */
public class MassInfoRequestParametersBean extends MarketSecurableRequestParametersBean {

    /**
     * Лимит количества элементов в списке, для POST заросов на маркет
     */
    public static final int MAX_LIST_ITEM_COUNT = 100;

    /**
     * 0 - Не получать предложения о продаже
     * 1 - Получать топ 50 дешевых предложений + свой
     * 2 - Получать только 1 самый дешевый оффер о продаже
     */
    private Sell sell;

    /**
     * 0 - Не получать запросы на покупку (ордера)
     * 1 - Получать топ 50 самых высоких запросов на покупку (ордеров) + свой
     * 2 - Получать только 1 самый высокий запрос на покупку
     */
    private Buy buy;

    /**
     * 0 - Не получать история торгов по предмету
     * 1 - Получить информацию о последних 100 продажах
     * 2 - Получить информацию о последних 10 продажах
     */
    private History history;

    /**
     * 0 - Не получать информацию о предмете
     * 1 - Получить базовую информацию (название, тип)
     * 2 - Получить дополнительно хэш для покупки, ссылку на картинку
     * 3 - Получать дополнительно описание предмета и теги из Steam
     */
    private Info info;

    /**
     * Список предметов (classid_instanceid)
     */
    private List<String> list;

    public Sell getSell() {
        return sell;
    }

    public void setSell(Sell sell) {
        this.sell = sell;
    }

    public Buy getBuy() {
        return buy;
    }

    public void setBuy(Buy buy) {
        this.buy = buy;
    }

    public History getHistory() {
        return history;
    }

    public void setHistory(History history) {
        this.history = history;
    }

    public Info getInfo() {
        return info;
    }

    public void setInfo(Info info) {
        this.info = info;
    }

    public List<String> getList() {
        return list;
    }

    public void setList(List<String> list) {
        this.list = list;
    }

    /**
     * Параметр, устанавливающий количество выводимых запросов на продажу предмета.
     */
    public enum Sell {
        /**
         * Не получать предложения о продаже
         */
        NONE,

        /**
         * Получать топ 50 дешевых предложений + свой
         */
        TOP_50,

        /**
         * Получать только 1 самый дешевый оффер о продаже
         */
        TOP_1
    }

    public enum Buy {
        NONE, TOP_50, TOP_1
    }

    public enum History {
        NONE, TOP_100, TOP_10
    }

    public enum Info {
        NONE, BASE, HASH_AND_PICTURE, STEAM
    }
}
