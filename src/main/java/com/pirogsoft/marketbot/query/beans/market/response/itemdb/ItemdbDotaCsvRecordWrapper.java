package com.pirogsoft.marketbot.query.beans.market.response.itemdb;

import com.pirogsoft.marketbot.common.ClassInstanceId;
import com.pirogsoft.marketbot.query.beans.market.response.MarketItemFromCsv;
import com.pirogsoft.marketbot.query.parser.csv.CsvRecordWrapperImpl;

/**
 * CSV строка из БД маркета
 */
public class ItemdbDotaCsvRecordWrapper extends CsvRecordWrapperImpl implements MarketItemFromCsv {

    @Override
    public ClassInstanceId obtainClassInstanceId() {
        return new ClassInstanceId(getCClassid(), getCInstanceid());
    }

    public static class PropertyIndices {

        public static final int C_CLASSID = 0;

        public static final int C_INSTANCEID = 1;

        public static final int C_PRICE = 2;

        public static final int C_OFFERS = 3;

        public static final int C_POPULARITY = 4;

        public static final int C_RARITY = 5;

        public static final int C_QUALITY = 6;

        public static final int C_HEROID = 7;

        public static final int C_MARKET_NAME = 8;

        public static final int C_MARKET_NAME_EN = 9;

        public static final int C_MARKET_HASH_NAME = 10;

        public static final int C_NAME_COLOR = 11;

        public static final int C_PRICE_UPDATED = 12;

        public static final int C_POP = 13;
    }

    public String getCClassid() {
        return getString(PropertyIndices.C_CLASSID);
    }

    public String getCInstanceid() {
        return getString(PropertyIndices.C_INSTANCEID);
    }

    public Integer getCPrice() {
        return getInteger(PropertyIndices.C_PRICE);
    }

    public String getCOffers() {
        return getString(PropertyIndices.C_OFFERS);
    }

    public Integer getCPopularity() {
        return getInteger(PropertyIndices.C_POPULARITY);
    }

    public String getCRarity() {
        return getString(PropertyIndices.C_RARITY);
    }

    public String getCQuality() {
        return getString(PropertyIndices.C_QUALITY);
    }

    public String getCHeroid() {
        return getString(PropertyIndices.C_HEROID);
    }

    public String getCMarketName() {
        return getStringWithoutQuotes(PropertyIndices.C_MARKET_NAME);
    }

    public String getCMarketNameEn() {
        return getStringWithoutQuotes(PropertyIndices.C_MARKET_NAME_EN);
    }

    public String getCMarketHashName() {
        String result =  getStringWithoutQuotes(PropertyIndices.C_MARKET_HASH_NAME);
        if(result == null || result.isEmpty() || result.equals("\"")) {
            return null;
        }
        return result;
    }

    public String getCNameColor() {
        return getString(PropertyIndices.C_NAME_COLOR);
    }

    public String getCPriceUpdated() {
        return getString(PropertyIndices.C_PRICE_UPDATED);
    }

    public String getCPop() {
        return getString(PropertyIndices.C_POP);
    }
}
