package com.pirogsoft.marketbot.query.beans.market.response;

import com.pirogsoft.marketbot.common.ClassInstanceId;
import com.pirogsoft.marketbot.query.parser.csv.CsvRecordWrappersCollection;

import java.util.ArrayList;
import java.util.List;

/**
 * Вещь из БД маркета.
 */
public class MarketCsvRecordWrapperImpl implements MarketItemFromCsv {

    private String classId;

    private String InstanceId;

    private int popularity;

    private int price;

    private String marketHashName;

    private String marketName;

    public MarketCsvRecordWrapperImpl() {
    }

    public MarketCsvRecordWrapperImpl(String classId, String instanceId, int popularity, int price, String marketHashName, String marketName) {
        this.classId = classId;
        InstanceId = instanceId;
        this.popularity = popularity;
        this.price = price;
        this.marketHashName = marketHashName;
        this.marketName = marketName;
    }

    public void setCClassid(String classId) {
        this.classId = classId;
    }

    public void setCInstanceid(String instanceId) {
        InstanceId = instanceId;
    }

    public void setCPopularity(int popularity) {
        this.popularity = popularity;
    }

    public void setCPrice(int price) {
        this.price = price;
    }

    public void setCMarketHashName(String marketHashName) {
        this.marketHashName = marketHashName;
    }

    public void setCMarketName(String marketName) {
        this.marketName = marketName;
    }

    @Override
    public String getCClassid() {
        return classId;
    }

    @Override
    public String getCInstanceid() {
        return InstanceId;
    }

    @Override
    public Integer getCPopularity() {
        return popularity;
    }

    @Override
    public Integer getCPrice() {
        return price;
    }

    @Override
    public String getCMarketHashName() {
        return marketHashName;
    }

    @Override
    public String getCMarketName() {
        return marketName;
    }

    @Override
    public ClassInstanceId obtainClassInstanceId() {
        return new ClassInstanceId(getCClassid(), getCInstanceid());
    }

    public static List<MarketItemFromCsv> getCsvRecordsList(CsvRecordWrappersCollection<? extends MarketItemFromCsv> source){
        ArrayList<MarketItemFromCsv> result = new ArrayList<>();
        for(MarketItemFromCsv csvRecord : source) {
            result.add(new MarketCsvRecordWrapperImpl(
                    csvRecord.getCClassid(),
                    csvRecord.getCInstanceid(),
                    csvRecord.getCPopularity(),
                    csvRecord.getCPrice(),
                    csvRecord.getCMarketHashName(),
                    csvRecord.getCMarketName()));
        }
        return result;
    }

    public static void sortByPopularity(List<MarketItemFromCsv> list) {
        list.sort((o1, o2) -> {
            if(o1.getCPopularity() < o2.getCPopularity()) {
                return 1;
            } else if (o1.getCPopularity() > o2.getCPopularity()) {
                return -1;
            } else {
                return 0;
            }
        });
    }

    public static List<MarketItemFromCsv> getCsvRecordsListSortedByPopularity(CsvRecordWrappersCollection<? extends MarketItemFromCsv> source) {
        List<MarketItemFromCsv> result = getCsvRecordsList(source);
        sortByPopularity(result);
        return result;
    }
}
