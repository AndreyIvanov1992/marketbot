package com.pirogsoft.marketbot.query.beans.market.response.getmyselloffers;

import com.pirogsoft.marketbot.common.ClassInstanceId;
import com.pirogsoft.marketbot.common.ClassInstanceIdSupport;
import com.pirogsoft.marketbot.query.parser.json.JsonMapWrapper;

/**
 * объект выставленного оффера
 * Created by AnIvanov on 01.05.2018.
 */
public class MySellOffer extends JsonMapWrapper implements ClassInstanceIdSupport {

    @Override
    public ClassInstanceId obtainClassInstanceId() {
        return new ClassInstanceId(getIClassid(), getIInstanceid());
    }

    public static class PropertyNames {
        public static final String UI_ID = "ui_id";
        public static final String I_NAME = "i_name";
        public static final String I_MARKET_NAME = "i_market_name";
        public static final String I_NAME_COLOR = "i_name_color";
        public static final String I_RARITY = "i_rarity";
        public static final String I_DESCRIPTIONS = "i_descriptions";
        public static final String UI_STATUS = "ui_status";
        public static final String HE_NAME = "he_name";
        public static final String UI_PRICE = "ui_price";
        public static final String I_CLASSID = "i_classid";
        public static final String I_INSTANCEID = "i_instanceid";
        public static final String UI_REAL_INSTANCE = "ui_real_instance";
        public static final String I_MARKET_PRICE = "i_market_price";
        public static final String POSITION = "position";
        public static final String MIN_PRICE = "min_price";
        public static final String UI_BID = "ui_bid";
        public static final String UI_ASSET = "ui_asset";
        public static final String TYPE = "type";
        public static final String UI_PRICE_TEXT = "ui_price_text";
        public static final String MIN_PRICE_TEXT = "min_price_text";
        public static final String I_MARKET_PRICE_TEXT = "i_market_price_text";
        public static final String OFFER_LIVE_TIME = "offer_live_time";
        public static final String PLACED = "placed";
    }

    public String getUiId() {
        return getString(PropertyNames.UI_ID);
    }

    public String getIName() {
        return getString(PropertyNames.I_NAME);
    }

    public String getIMarketName() {
        return getString(PropertyNames.I_MARKET_NAME);
    }

    public String getINameColor() {
        return getString(PropertyNames.I_NAME_COLOR);
    }

    public String getIRarity() {
        return getString(PropertyNames.I_RARITY);
    }

    public String getIDescription() {
        return getString(PropertyNames.I_DESCRIPTIONS);
    }

    public String getUiStatus() {
        return getString(PropertyNames.UI_STATUS);
    }

    public String getHeName() {
        return getString(PropertyNames.HE_NAME);
    }

    /**
     * Получить цену в рублях
     *
     * @return цена в рублях
     */
    public Double getUiPrice() {
        return getDouble(PropertyNames.UI_PRICE);
    }

    public String getIClassid() {
        return getString(PropertyNames.I_CLASSID);
    }

    public String getIInstanceid() {
        return getString(PropertyNames.I_INSTANCEID);
    }

    public String getUiRealInstance() {
        return getString(PropertyNames.UI_REAL_INSTANCE);
    }

    public Double getIMarketPrice() {
        return getDouble(PropertyNames.I_MARKET_PRICE);
    }

    public Integer getPosition() {
        return getDoubleAsInteger(PropertyNames.POSITION);
    }

    public Double getMinPrice() {
        return getDouble(PropertyNames.MIN_PRICE);
    }

    public String getUiBid() {
        return getString(PropertyNames.UI_BID);
    }

    public String getUiAsset() {
        return getString(PropertyNames.UI_ASSET);
    }

    public String getType() {
        return getString(PropertyNames.TYPE);
    }

    public String getUiPriceText() {
        return getString(PropertyNames.UI_PRICE_TEXT);
    }

    public String getMinPriceText() {
        return getString(PropertyNames.MIN_PRICE_TEXT);
    }

    public String getIMarketPriceText() {
        return getString(PropertyNames.I_MARKET_PRICE_TEXT);
    }

    public Integer getOfferLiveTime() {
        return getDoubleAsInteger(PropertyNames.OFFER_LIVE_TIME);
    }

    public String getPlaced() {
        return getString(PropertyNames.PLACED);
    }
}