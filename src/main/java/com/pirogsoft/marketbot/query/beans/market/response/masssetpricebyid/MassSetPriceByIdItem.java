package com.pirogsoft.marketbot.query.beans.market.response.masssetpricebyid;

import com.pirogsoft.marketbot.query.parser.json.JsonMapWrapper;

/**
 * Предмет
 * Created by AnIvanov on 02.05.2018.
 */
public class MassSetPriceByIdItem extends JsonMapWrapper {

    /**
     * Название пропертей
     */
    public static class PropertyNames {

        public static final String ID = "id";

        public static final String HASH = "hash";

        public static final String OLD_PRICE = "oldPrice";

        public static final String NEW_PRICE = "newPrice";

        public static final String MARKET_HASH_NAME = "market_hash_name";

        public static final String STATUS = "status";
    }

    public Integer getId() {
        return getDoubleAsInteger(PropertyNames.ID);
    }

    public String getHash() {
        return getString(PropertyNames.HASH);
    }

    public Integer getOldPrice() {
        return getDoubleAsInteger(PropertyNames.OLD_PRICE);
    }

    public Integer getNewPrice() {
        return getStringAsInteger(PropertyNames.NEW_PRICE);
    }

    public String getMarketHashName() {
        return getString(PropertyNames.MARKET_HASH_NAME);
    }

    public MassSetPriceByIdStatus getStatus() {
        return getJsonMapWrapper(PropertyNames.STATUS, MassSetPriceByIdStatus.class);
    }
}
