package com.pirogsoft.marketbot.query.beans.market.request.getorders;

import com.pirogsoft.marketbot.query.beans.market.request.common.MarketSecurableRequestParametersBean;

/**
 * Объект параметров запроса на получение "моих ордеров"
 * Created by AnIvanov on 06.05.2018.
 */
public class GetOrdersRequestParametersBean extends MarketSecurableRequestParametersBean {

    /**
     * Номер страницы, на странице находится 500 предметов.
     * Опциональный параметр, если он не будет указан в ответ Вы получите только 2000 ваших заявок.
     */
    private Integer page;

    /**
     * Получает номер страницы
     *
     * @return номер страницы
     */
    public Integer getPage() {
        return page;
    }

    /**
     * Устанавливает номер страницы
     *
     * @param page номер страницы
     */
    public void setPage(Integer page) {
        this.page = page;
    }
}
