package com.pirogsoft.marketbot.query.beans.market.response;

import com.pirogsoft.marketbot.common.ClassInstanceIdSupport;

/**
 * Csv строка - вещь из маркета
 */
public interface MarketItemFromCsv extends ClassInstanceIdSupport {

    /**
     * Получение ClassId
     *
     * @return ClassId
     */
    String getCClassid();

    /**
     * Получение InstanceId
     *
     * @return InstanceId
     */
    String getCInstanceid();

    /**
     * Получение индекса популярности предмета. Чем больше индект - тем популярнее предмент
     *
     * @return индект популярности предмета
     */
    Integer getCPopularity();

    /**
     * Получение цены предмета в копейках
     *
     * @return Цена предмета в копейках
     */
    Integer getCPrice();

    /**
     * Получение уникальной строки вещи в стиме
     *
     * @return уникальная строка вещи в стиме
     */
    String getCMarketHashName();

    /**
     * Получение названия предмета на маркете
     *
     * @return название предмета на маректе
     */
    String getCMarketName();
}
