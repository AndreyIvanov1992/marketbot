package com.pirogsoft.marketbot.query.beans.market.response.massinfo;

import com.pirogsoft.marketbot.query.parser.json.JsonListWrapper;

import java.time.Instant;
import java.time.LocalDateTime;
import java.util.TimeZone;

/**
 * Created by AnIvanov on 02.05.2018.
 */
public class MassInfoHistoryItem extends JsonListWrapper {

    /**
     * Индексы свойств
     */
    public static class PropertyIndexes {

        public static final Integer DATE_TIME = 0;

        public static final Integer PRICE = 1;
    }

    /**
     * Получение таймштампа продажи предмета
     *
     * @return таймштамп продажи предмета
     */
    public Integer getTimeStamp() {
        return getDoubleAsInteger(PropertyIndexes.DATE_TIME);
    }

    /**
     * Получение даты и времени продажи предмета
     *
     * @return дата и время пордажи предмета
     */
    public LocalDateTime getDateTime() {
        return LocalDateTime.ofInstant(Instant.ofEpochSecond(getTimeStamp()),
                TimeZone.getDefault().toZoneId());
    }

    /**
     * Получение цены, по которой предмет был продан
     *
     * @return цена по которой предмет был продан
     */
    public Integer getPrice() {
        return getDoubleAsInteger(PropertyIndexes.PRICE);
    }
}
