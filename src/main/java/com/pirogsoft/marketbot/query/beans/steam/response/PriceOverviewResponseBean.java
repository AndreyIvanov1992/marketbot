package com.pirogsoft.marketbot.query.beans.steam.response;

import com.pirogsoft.marketbot.query.parser.json.JsonMapWrapper;

import java.util.Arrays;

/**
 * Бин ответа на запрос в стим
 * Created by AnIvanov on 10.05.2018.
 */
public class PriceOverviewResponseBean extends JsonMapWrapper {

    /**
     * Название пропертей
     */
    public static class PropertyNames {

        public static final String SUCCESS = "success";

        public static final String LOWEST_PRICE = "lowest_price";

        public static final String VOLUME = "volume";

        public static final String MEDIAN_PRICE = "median_price";
    }

    /**
     * Получение успешности выполнения запроса
     *
     * @return успешность выполнения запроса
     */
    public Boolean isSuccess() {
        return getBoolean(PropertyNames.SUCCESS);
    }

    /**
     * Получение сообщения об ошибке
     *
     * @return сообщение об ошибке
     */
    public String getLowerPrice() {
        return getString(PropertyNames.LOWEST_PRICE);
    }

    /**
     * Получение сообщения об ошибке
     *
     * @return сообщение об ошибке
     */
    public String getVolume() {
        return getString(PropertyNames.VOLUME);
    }

    /**
     * Получение сообщения об ошибке
     *
     * @return сообщение об ошибке
     */
    public String getMedianPrice() {
        return getString(PropertyNames.MEDIAN_PRICE);
    }

    /**
     * Получение минимальной цены в копейках
     *
     * @return цена в копейках
     */
    public Integer obtainLowerPriceInteger() {
        String lowerPrice = getLowerPrice();
        if (lowerPrice == null) {
            return null;
        }
        String stringPrice = Arrays.asList(lowerPrice.split(" ")).get(0);
        stringPrice = stringPrice.replace(",", ".");
        return (int) (Double.parseDouble(stringPrice) * 100);
    }
}
