package com.pirogsoft.marketbot.query.beans.market.response.massinfo;

import com.pirogsoft.marketbot.query.parser.json.JsonMapWrapper;

import java.util.List;

/**
 * Created by AnIvanov on 02.05.2018.
 */
public class MassInfoHistory extends JsonMapWrapper {

    /**
     * Название пропертей
     */
    public static class PropertyNames {

        public static final String MAX = "max";

        public static final String MIN = "min";

        public static final String AVERAGE = "average";

        public static final String NUMBER = "number";

        public static final String HISTORY = "history";
    }

    /**
     * Получение максимальной цены прожажи предмета в копейках
     *
     * @return максиамльная цена проажи продмета в копейках
     */
    public Integer getMax() {
        return getDoubleAsInteger(PropertyNames.MAX);
    }

    /**
     * Получение минимальной цены прожажи предмета в копейках
     *
     * @return минимальная цена проажи продмета в копейках
     */
    public Integer getMin() {
        return getDoubleAsInteger(PropertyNames.MIN);
    }

    /**
     * Получение средней цены прожажи предмета в копейках
     *
     * @return спедняя цена проажи продмета в копейках
     */
    public Integer getAberage() {
        return getDoubleAsInteger(PropertyNames.AVERAGE);
    }

    /**
     * Получение количества выводимых элементов списка продаж
     *
     * @return Количество выводимых элементов списка продаж
     */
    public Integer getNumber() {
        return getDoubleAsInteger(PropertyNames.NUMBER);
    }

    /**
     * Получение списка продаж
     *
     * @return список продаж
     */
    public List<MassInfoHistoryItem> getHistory() {
        return getJsonListWrapperList(PropertyNames.HISTORY, MassInfoHistoryItem.class);
    }
}
