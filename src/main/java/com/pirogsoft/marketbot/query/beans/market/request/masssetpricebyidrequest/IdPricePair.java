package com.pirogsoft.marketbot.query.beans.market.request.masssetpricebyidrequest;

/**
 * Пара ui_id - цена
 * Created by AnIvanov on 20.04.2018.
 */
public class IdPricePair {

    /**
     * ui_id
     */
    private int id;

    /**
     * Цена в вкопейках
     */
    private int price;

    public IdPricePair() {
    }

    /**
     * Коструктор
     *
     * @param id    ui_id
     * @param price цена
     */
    public IdPricePair(int id, int price) {
        this.id = id;
        this.price = price;
    }

    /**
     * Получение ui_id
     *
     * @return ui_id
     */
    public int getId() {
        return id;
    }

    /**
     * Установка ui_id
     *
     * @param id ui_id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * получение цены в копейках
     *
     * @return цена
     */
    public int getPrice() {
        return price;
    }

    /**
     * Установка цены в копейках
     *
     * @param price цена
     */
    public void setPrice(int price) {
        this.price = price;
    }
}