package com.pirogsoft.marketbot.query.beans;

import com.pirogsoft.marketbot.query.parser.json.JsonMapWrapper;


public class TestIpResponse extends JsonMapWrapper {

    /**
     * Название пропертей
     */
    public static class PropertyNames {

        public static final String IP = "ip";
    }

    public String getIp() {
        return getString(PropertyNames.IP);
    }

}
