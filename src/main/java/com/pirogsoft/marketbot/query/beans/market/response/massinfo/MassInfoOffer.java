package com.pirogsoft.marketbot.query.beans.market.response.massinfo;

import com.pirogsoft.marketbot.query.parser.json.JsonListWrapper;

/**
 * Предложение о пукепке или продаже
 * Created by AnIvanov on 02.05.2018.
 */
public class MassInfoOffer extends JsonListWrapper {

    /**
     * Индексы свойств
     */
    public static class PropertyIndexes {

        public static final Integer PRICE = 0;

        public static final Integer AMOUNT = 1;
    }

    /**
     * Получение цены в копейках
     *
     * @return цена в копейках
     */
    public Integer getPrice() {
        return getDoubleAsInteger(PropertyIndexes.PRICE);
    }

    /**
     * Получение количества по этому предложению
     *
     * @return количество по этому предложению
     */
    public Integer getAmount() {
        return getDoubleAsInteger(PropertyIndexes.AMOUNT);
    }
}
