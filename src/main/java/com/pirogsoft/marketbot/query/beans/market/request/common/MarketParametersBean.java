package com.pirogsoft.marketbot.query.beans.market.request.common;

import com.pirogsoft.marketbot.service.GameApp;

/**
 * Базовый класс бина параметров зароса к апи маркета
 * Created by AnIvanov on 05.04.2018.
 */
public class MarketParametersBean {

    /**
     * Игра
     */
    private GameApp gameApp;

    public MarketParametersBean() {
    }

    /**
     * Получение игры
     *
     * @return Игра
     */
    public GameApp getGameApp() {
        return gameApp;
    }

    /**
     * Установка игры
     *
     * @param gameApp Игра
     */
    public void setGameApp(GameApp gameApp) {
        this.gameApp = gameApp;
    }
}
