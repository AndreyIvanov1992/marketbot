package com.pirogsoft.marketbot.query.beans.market.response.getorders;

import com.pirogsoft.marketbot.common.ClassInstanceId;
import com.pirogsoft.marketbot.common.ClassInstanceIdSupport;
import com.pirogsoft.marketbot.query.parser.json.JsonMapWrapper;

/**
 * "мой" Запрос на покупку
 * Created by AnIvanov on 06.05.2018.
 */
public class MyOrder extends JsonMapWrapper implements ClassInstanceIdSupport {

    public static class PropertyNames {

        /**
         * classId предмета
         */
        public static final String I_CLASSID = "i_classid";

        /**
         * instaneId предмета
         */
        public static final String I_INSTANCEID = "i_instanceid";

        /**
         * Hash name предмета в стиме
         */
        public static final String I_MARKET_HASH_NAME = "i_market_hash_name";

        /**
         * название на маркете
         */
        public static final String I_MARKET_NAME = "i_market_name";

        /**
         * цена, которую мы выставили в копейках
         */
        public static final String O_PRICE = "o_price";

        /**
         * состояние, хз где расшифровка
         */
        public static final String O_STATE = "o_state";
    }

    /**
     * Получение classId предмета
     *
     * @return ClassId предмета
     */
    public String getIClassid() {
        return getString(PropertyNames.I_CLASSID);
    }

    /**
     * Поучение instaneId предмета
     *
     * @return InstaneId предмета
     */
    public String getIInstanceid() {
        return getString(PropertyNames.I_INSTANCEID);
    }

    /**
     * Получение Hash name предмета в стиме
     *
     * @return Hash name предмета в стиме
     */
    public String getIMarketHashName() {
        return getString(PropertyNames.I_MARKET_HASH_NAME);
    }

    /**
     * Получение названия на маркете
     *
     * @return Название на маркете
     */
    public String getIMarketName() {
        return getString(PropertyNames.I_MARKET_NAME);
    }

    /**
     * Получение цены, которую мы установили, в копейках
     *
     * @return цена
     */
    public Integer getOPrice() {
        return getStringAsInteger(PropertyNames.O_PRICE);
    }

    /**
     * Получение состояния
     *
     * @return состояние
     */
    public String getOState() {
        return getString(PropertyNames.O_STATE);
    }


    @Override
    public ClassInstanceId obtainClassInstanceId() {
        return new ClassInstanceId(getIClassid(), getIInstanceid());
    }

}
