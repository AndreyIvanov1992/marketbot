package com.pirogsoft.marketbot.query.service.impl;

import com.pirogsoft.marketbot.query.service.api.ResponseStringParsesService;
import org.apache.http.HttpResponse;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Имплементация сервиса получения строки из ответа
 * Created by AnIvanov on 14.04.2018.
 */

@Service
public class ResponseStringParsesServiceImpl implements ResponseStringParsesService {

    @Override
    public String obtainStringResponse(HttpResponse response) throws IOException {
        BufferedReader rd = new BufferedReader(
                new InputStreamReader(response.getEntity().getContent()));

        StringBuffer result = new StringBuffer();
        String line = "";
        while ((line = rd.readLine()) != null) {
            result.append(line);
        }
        return result.toString();
    }
}
