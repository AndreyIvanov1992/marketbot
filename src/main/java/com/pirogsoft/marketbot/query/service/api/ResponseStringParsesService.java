package com.pirogsoft.marketbot.query.service.api;

import org.apache.http.HttpResponse;

import java.io.IOException;

/**
 * Сервис для получение строки из ответа запроса.
 * Created by AnIvanov on 14.04.2018.
 */
public interface ResponseStringParsesService {
    /**
     * Получение строки из ответа запроса
     * @param response ответ запроса
     * @return строка ответа
     */
    String obtainStringResponse(HttpResponse response) throws IOException;
}
