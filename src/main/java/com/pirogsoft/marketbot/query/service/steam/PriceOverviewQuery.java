package com.pirogsoft.marketbot.query.service.steam;

import com.pirogsoft.marketbot.ContextContainer;
import com.pirogsoft.marketbot.LogUtils;
import com.pirogsoft.marketbot.query.Query;
import com.pirogsoft.marketbot.query.TorChainRefresher;
import com.pirogsoft.marketbot.query.beans.TestIpResponse;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.client.utils.HttpClientUtils;
import org.apache.http.conn.ConnectionPoolTimeoutException;
import org.apache.log4j.Logger;

import java.io.IOException;

/**
 * Запрос на получение цены предмета в стим
 * Created by Andrey on 24.05.2018.
 */
public class PriceOverviewQuery<S, U> extends Query<S, U> {

    static Logger logger = Logger.getLogger(PriceOverviewQuery.class);

    /**
     * Код ошибки "Слишком много запросов"
     */
    public static final int TO_MENY_REQUESTS_CODE = 429;

    /**
     * Время на "подождать" после обновления цепи тор в милисекундах
     */
    public static final int TIME_FOR_WAIT_AFTER_REFRESH_TOR_CHAIN = 5000;

    /**
     * Обновлятель цепочки тор, для получения нового IP
     */
    private TorChainRefresher torChainRefresher;

    /**
     * Максимальное количество попыток получения ответа
     */
    private int maxAmountAttempts;

    /**
     * Установка обновлятеля цепочки тор
     *
     * @param torChainRefresher Обновлятель цепочки тор
     */
    public void setTorChainRefresher(TorChainRefresher torChainRefresher) {
        this.torChainRefresher = torChainRefresher;
    }

    /**
     * Установка максимального количества попыток получения ответа
     *
     * @param maxAmountAttempts Максимальное количество попыток получения ответа
     */
    public void setMaxAmountAttempts(int maxAmountAttempts) {
        this.maxAmountAttempts = maxAmountAttempts;
    }


    @Override
    public U execute(S params) throws IOException {
        boolean success = false;
        int attempts = 0;
        U result;
        do {
            result = super.execute(params);
//            TestIpResponse testIpResponse = (TestIpResponse) ((Query)ContextContainer.getInstance().getContext().getBean("testIpQuery")).execute(null);
//            logger.info(LogUtils.obtainLogMessage("my ip is : " + testIpResponse.getIp()));

            //Если 500, долбиться смысла нет.
            success = getLastReponseStatus() == HttpStatus.SC_OK || getLastReponseStatus() == HttpStatus.SC_INTERNAL_SERVER_ERROR;
            if(!success) {
                if(getLastReponseStatus() == HttpStatus.SC_FORBIDDEN || getLastReponseStatus() == TO_MENY_REQUESTS_CODE) {
                    torChainRefresher.refreshChain();
                }
                try {
                    Thread.sleep(TIME_FOR_WAIT_AFTER_REFRESH_TOR_CHAIN);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
            attempts++;
        } while (!success && attempts < maxAmountAttempts);
        return result;
    }

    @Override
    protected boolean parseIfNotOk() {
        return false;
    }


}
