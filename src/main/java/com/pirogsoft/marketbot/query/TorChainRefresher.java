package com.pirogsoft.marketbot.query;

import com.pirogsoft.marketbot.LogUtils;
import org.apache.log4j.Logger;

import java.io.*;
import java.net.InetAddress;
import java.net.Socket;

/**
 * Обновлятель цепочки тор
 */
public class TorChainRefresher {

    Logger logger = Logger.getLogger(TorChainRefresher.class);

    /**
     * Код успешного ответа
     */
    public static String SUCCESS_CODE = "250";

    /**
     * Контент запроса в тор для авторизации
     */
    public static String AUTHENTICATE = "AUTHENTICATE  \r\n";

    /**
     * Контент запроса в тор об обновлении цепочки
     */
    public static String SIGNAL_NEWNYM = "signal NEWNYM \r\n";

    /**
     * Хост контроллера тора
     */
    private String hostname;

    /**
     * Порт контроллера тора
     */
    private int port;

    public TorChainRefresher() {
    }

    /**
     * Обновление цепи тор
     *
     * @return Успешность обновления
     * @throws IOException ошибка ввода вывода
     */
    public boolean refreshChain() throws IOException {
        InetAddress ipAddress = InetAddress.getByName(hostname);
        Socket socket = new Socket(ipAddress, port);
        try {
            InputStream input = socket.getInputStream();
            OutputStream sout = socket.getOutputStream();

            BufferedReader reader = new BufferedReader(new InputStreamReader(input));

            DataOutputStream out = new DataOutputStream(sout);

            if (!sendSignal(AUTHENTICATE, out, reader)) {
                return false;
            }
            if (!sendSignal(SIGNAL_NEWNYM, out, reader)) {
                return false;
            }
            return true;
        }
        finally {
            socket.close();
        }
    }

    /**
     * Посылает запрос в тор
     *
     * @param signal Сообщение
     * @param out    Поток ввода
     * @param reader Читатель потока вывода
     * @return Успешность выполенния запроса
     * @throws IOException Ошибка ввода вывода
     */
    private boolean sendSignal(String signal, DataOutputStream out, BufferedReader reader) throws IOException {
        out.writeBytes(signal);
        out.flush();
        String response = reader.readLine();
        String code = obtainCode(response);
        if (code.isEmpty()) {
            logger.error(LogUtils.obtainLogMessage("No code hss come during send signal: " + signal + " to tor."));
            return false;
        }
        if (!isSuccessCode(code)) {
            logger.error(LogUtils.obtainLogMessage("Responce code from tor id: " + code + " during send signal:" + signal + " to tor."));
            return false;
        }
        return true;
    }

    /**
     * Проверка успешный ли код ответа
     *
     * @param code Код ответа
     * @return Результат проверки
     */
    private boolean isSuccessCode(String code) {
        return code.equals(SUCCESS_CODE);
    }

    private String obtainCode(String response) {
        if (response == null) {
            return "";
        }
        String[] partsOfResponse = response.split(" ");
        if (partsOfResponse.length == 0) {
            return "";
        }
        return partsOfResponse[0];
    }

    /**
     * Установка хоста контроллера тора
     *
     * @param hostname Хост контроллера тора
     */
    public void setHostname(String hostname) {
        this.hostname = hostname;
    }

    /**
     * Установка порта контроллера тора
     *
     * @param port Порт контроллера тора
     */
    public void setPort(int port) {
        this.port = port;
    }
}
