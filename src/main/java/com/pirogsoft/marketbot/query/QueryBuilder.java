package com.pirogsoft.marketbot.query;

import org.apache.http.client.methods.HttpRequestBase;

/**
 * Интерфейс для постраения объекта HTTP запроса, в зависимости от параметров
 *
 * @param <T> Тип запроса
 * @param <S> Тип объекта параметрав
 * @autor AnIvanov
 */
public interface QueryBuilder<T extends HttpRequestBase, S> {

    /**
     * Строит объект HTTP запроса, на основании параметров
     *
     * @param params параметры
     * @return объект HTTP запроса
     */
    T obtainRequest(S params);
}
