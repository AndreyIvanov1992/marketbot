package com.pirogsoft.marketbot.method.market.recommendations;

import com.pirogsoft.marketbot.common.ClassInstanceId;
import com.pirogsoft.marketbot.database.entities.Game;
import com.pirogsoft.marketbot.database.entities.MarketItem;
import com.pirogsoft.marketbot.database.entities.SteamItem;
import com.pirogsoft.marketbot.database.repository.GameRepository;
import com.pirogsoft.marketbot.database.repository.MarketItemRepository;
import com.pirogsoft.marketbot.database.repository.SteamItemRepository;
import com.pirogsoft.marketbot.exception.MarketBadKeyException;
import com.pirogsoft.marketbot.job.api.Method;
import com.pirogsoft.marketbot.method.market.recommendations.view.ReportView;
import com.pirogsoft.marketbot.query.beans.market.request.massinfo.MassInfoRequestParametersBean;
import com.pirogsoft.marketbot.query.beans.market.response.massinfo.MassInfoResponseBean;
import com.pirogsoft.marketbot.query.beans.market.response.massinfo.MassInfoResult;
import com.pirogsoft.marketbot.service.GameApp;
import com.pirogsoft.marketbot.service.MarketQueryService;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;
import java.util.*;

/**
 * Генератор отчета по рекомендациям покупок
 * Created by Andrey on 05.06.2018.
 */
public class MakeReportProcessor implements Method {

    /**
     * Вещи, которые были куплены меньше раз чем это значение, не рассматриваются
     */
    public static final int MIN_BUYS_AMOUNT = 50;

    /**
     * Игра
     */
    private GameApp gameApp;

    /**
     * Объект игры из БД
     */
    private Game game;

    private GameRepository gameRepository;

    private MarketItemRepository marketItemRepository;

    private SteamItemRepository steamItemRepository;

    private MarketQueryService marketQueryService;

    /**
     * Представление отчета
     */
    private ReportView reportView;

    /**
     * Лимит цены. ниже этого лимита вещи не рассматриваются
     */
    private int priceLimit;

    public void setGameApp(GameApp gameApp) {
        this.gameApp = gameApp;
    }

    @Autowired
    public void setGameRepository(GameRepository gameRepository) {
        this.gameRepository = gameRepository;
    }

    @Autowired
    public void setMarketItemRepository(MarketItemRepository marketItemRepository) {
        this.marketItemRepository = marketItemRepository;
    }

    @Autowired
    public void setSteamItemRepository(SteamItemRepository steamItemRepository) {
        this.steamItemRepository = steamItemRepository;
    }

    @Autowired
    public void setMarketQueryService(MarketQueryService marketQueryService) {
        this.marketQueryService = marketQueryService;
    }

    public void setReportView(ReportView reportView) {
        this.reportView = reportView;
    }

    public void setPriceLimit(int priceLimit) {
        this.priceLimit = priceLimit;
    }

    @PostConstruct
    public void init() {
        game = gameRepository.findByName(gameApp.toString());
    }

    @Override
    public void execute() {
        try {
            List<MarketItem> marketItems = marketItemRepository.findItemByGame(game);
            ArrayList<String> itemIds = new ArrayList<>();
            for (MarketItem marketItem : marketItems) {
                itemIds.add(marketItem.obtainClassInstanceId().toString());
            }
            List<MassInfoResponseBean> massInfoResponseBeans = marketQueryService.massInfo(gameApp, MassInfoRequestParametersBean.Sell.NONE, MassInfoRequestParametersBean.Buy.NONE, MassInfoRequestParametersBean.History.TOP_100, MassInfoRequestParametersBean.Info.BASE, itemIds);
            Map<ClassInstanceId, MassInfoResult> massInfoResultMap = obtainMapOfMassInfoResult(massInfoResponseBeans);

            List<SteamItem> steamItems = steamItemRepository.findByGame(game);

            List<SteamItemInfo> steamItemInfoList = obtainSteamItemInfoList(steamItems, massInfoResultMap);
            steamItemInfoList.sort((o1, o2) -> {
                if (o1.getProfit() > o2.getProfit()) {
                    return -1;
                } else if (o1.getProfit() < o2.getProfit()) {
                    return 1;
                } else {
                    return 0;
                }
            });
            reportView.generateView(steamItemInfoList, gameApp);
            //Тут у нас готовый к использованию объект.

        } catch (MarketBadKeyException e) {
            throw new RuntimeException(e);
        }

    }

    private Map<ClassInstanceId, MassInfoResult> obtainMapOfMassInfoResult(List<MassInfoResponseBean> responseBeans) {
        Map<ClassInstanceId, MassInfoResult> result = new HashMap<>();
        for (MassInfoResponseBean responseBean : responseBeans) {
            for (MassInfoResult massInfoResult : responseBean.getResult()) {
                result.put(massInfoResult.obtainClassInstanceId(), massInfoResult);
            }
        }
        return result;
    }

    private List<SteamItemInfo> obtainSteamItemInfoList(List<SteamItem> steamItems, Map<ClassInstanceId, MassInfoResult> massInfoResultMap) {
        List<SteamItemInfo> steamItemInfoList = new ArrayList<>();
        for (SteamItem steamItem : steamItems) {
            if(steamItem == null || steamItem.getPrice() == null) {
                continue;
            }
            if(steamItem.getPrice() < priceLimit) {
                continue;
            }

            List<MarketItemInfo> marketItemInfoList = new ArrayList<>();

            for (MarketItem marketItem : steamItem.getMarketItemsByMarketHashName()) {
                MassInfoResult infoResult =  massInfoResultMap.get(marketItem.obtainClassInstanceId());
                if(infoResult == null || infoResult.getHistory() == null || infoResult.getHistory().getNumber() == null || infoResult.getHistory().getNumber() < MIN_BUYS_AMOUNT) {
                    continue;
                }
                MarketItemInfo marketItemInfo = new MarketItemInfo(infoResult, steamItem);
                marketItemInfoList.add(marketItemInfo);
            }
            steamItemInfoList.add(new SteamItemInfo(steamItem, marketItemInfoList));

        }
        return steamItemInfoList;
    }
}
