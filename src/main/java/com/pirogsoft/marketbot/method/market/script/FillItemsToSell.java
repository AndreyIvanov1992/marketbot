package com.pirogsoft.marketbot.method.market.script;

import com.pirogsoft.marketbot.common.ClassInstanceId;
import com.pirogsoft.marketbot.database.entities.Game;
import com.pirogsoft.marketbot.database.repository.GameRepository;
import com.pirogsoft.marketbot.database.service.fill.CreateDBItemsByQueryResponseService;
import com.pirogsoft.marketbot.exception.MarketBadKeyException;
import com.pirogsoft.marketbot.job.api.Method;
import com.pirogsoft.marketbot.query.beans.market.request.massinfo.MassInfoRequestParametersBean;
import com.pirogsoft.marketbot.query.beans.market.response.getmyselloffers.GetMySellOffersResponseBean;
import com.pirogsoft.marketbot.query.beans.market.response.getmyselloffers.MySellOffer;
import com.pirogsoft.marketbot.query.beans.market.response.massinfo.MassInfoResponseBean;
import com.pirogsoft.marketbot.query.beans.market.response.massinfo.MassInfoResult;
import com.pirogsoft.marketbot.service.GameApp;
import com.pirogsoft.marketbot.service.MarketQueryService;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

/**
 * Скрипт заполнения БД выставленными на продажу предметами
 * Created by AnIvanov on 07.05.2018.
 */
public class FillItemsToSell implements Method {

    /**
     * Приоретет продажи для добавленных предметов
     */
    public static final int SELL_PRIORITY = 1;

    /**
     * Приоретет покупки для добавленных предметов
     */
    public static final int BUY_PRIORITY = 0;

    /**
     * Лимит при котором предмет ставиться на продажу
     */
    public static final int HISTORY_LIMIT = 10;

    /**
     * Игра
     */
    private GameApp gameApp;

    /**
     * Объект игры БД
     */
    private Game game;

    /**
     * CRUD-сервис для предметов из маркета
     */
    private MarketQueryService marketQueryService;

    /**
     * CRUD-сервис для игр
     */
    private GameRepository gameRepository;

    /**
     * Сервис для добавление предметов в бд по ответам запросов на маркет
     */
    private CreateDBItemsByQueryResponseService createDBItemsByQueryResponseService;

    /**
     * Установка игры
     *
     * @param gameApp игра
     */
    public void setGameApp(GameApp gameApp) {
        this.gameApp = gameApp;
    }

    /**
     * Установка CRUD-сервиса для предметов из маркета
     *
     * @param marketQueryService CRUD-сервис для предметов из маркета
     */
    @Autowired
    public void setMarketQueryService(MarketQueryService marketQueryService) {
        this.marketQueryService = marketQueryService;
    }

    /**
     * Установка CRUD-сервиса для игр
     *
     * @param gameRepository CRUD-сервис для игр
     */
    @Autowired
    public void setGameRepository(GameRepository gameRepository) {
        this.gameRepository = gameRepository;
    }

    /**
     * Установка сервиса для добавление предметов в бд по ответам запросов на маркет
     *
     * @param createDBItemsByQueryResponseService Сервис для добавление предметов в бд по ответам запросов на маркет
     */
    @Autowired
    public void setCreateDBItemsByQueryResponseService(CreateDBItemsByQueryResponseService createDBItemsByQueryResponseService) {
        this.createDBItemsByQueryResponseService = createDBItemsByQueryResponseService;
    }

    @PostConstruct
    public void init() {
        game = gameRepository.findByName(gameApp.toString());
    }


    @Override
    public void execute() {
        try {
            GetMySellOffersResponseBean responseBean = marketQueryService.getMySellOffers(gameApp);

            ArrayList<String> itemClassInstanceIdList = new ArrayList<>();
            for (MySellOffer mySellOffer : responseBean.getOffers()) {
                ClassInstanceId classInstanceId = mySellOffer.obtainClassInstanceId();
                itemClassInstanceIdList.add(classInstanceId.obtainClassInstanceIdString());
            }


            List<MassInfoResponseBean> responses = marketQueryService.massInfo(gameApp,
                    MassInfoRequestParametersBean.Sell.NONE,
                    MassInfoRequestParametersBean.Buy.NONE,
                    MassInfoRequestParametersBean.History.TOP_100,
                    MassInfoRequestParametersBean.Info.BASE, itemClassInstanceIdList);
            for (MassInfoResponseBean massInfoResponseBean : responses) {
                for (MassInfoResult result : massInfoResponseBean.getResult()) {
                    addToDb(result);
                }
            }
        } catch (MarketBadKeyException e) {
            e.printStackTrace();
        }
    }

    /**
     * Добавление элемента в БД
     *
     * @param itemInfo информация о предмете
     */
    private void addToDb(MassInfoResult itemInfo) {
        if (itemInfo.getHistory() != null && itemInfo.getHistory().getHistory() != null && itemInfo.getHistory().getHistory().size() > HISTORY_LIMIT) {
            createDBItemsByQueryResponseService.addMarketItemToDb(itemInfo, game, SELL_PRIORITY, BUY_PRIORITY, true);
        } else {
            createDBItemsByQueryResponseService.addMarketItemToDb(itemInfo, game, 0, BUY_PRIORITY, false);
        }
    }

}
