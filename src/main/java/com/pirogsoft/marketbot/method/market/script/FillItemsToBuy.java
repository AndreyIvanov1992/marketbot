package com.pirogsoft.marketbot.method.market.script;

import com.pirogsoft.marketbot.common.ClassInstanceId;
import com.pirogsoft.marketbot.database.entities.Game;
import com.pirogsoft.marketbot.database.repository.GameRepository;
import com.pirogsoft.marketbot.database.service.fill.CreateDBItemsByQueryResponseService;
import com.pirogsoft.marketbot.exception.MarketBadKeyException;
import com.pirogsoft.marketbot.job.api.Method;
import com.pirogsoft.marketbot.query.beans.market.request.massinfo.MassInfoRequestParametersBean;
import com.pirogsoft.marketbot.query.beans.market.response.getorders.GetOrdersResponseBean;
import com.pirogsoft.marketbot.query.beans.market.response.getorders.MyOrder;
import com.pirogsoft.marketbot.query.beans.market.response.massinfo.MassInfoResponseBean;
import com.pirogsoft.marketbot.query.beans.market.response.massinfo.MassInfoResult;
import com.pirogsoft.marketbot.service.GameApp;
import com.pirogsoft.marketbot.service.MarketQueryService;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

/**
 * Добавление вещей с ордерами в БД.
 * Created by AnIvanov on 10.05.2018.
 */
public class FillItemsToBuy implements Method {

    /**
     * Приоретет продажи для добавленных предметов
     */
    public static final int SELL_PRIORITY = 0;

    /**
     * Приоретет покупки для добавленных предметов
     */
    public static final int BUY_PRIORITY = 1;

    /**
     * Игра
     */
    private GameApp gameApp;

    /**
     * Объект игры БД
     */
    private Game game;

    /**
     * CRUD-сервис для игр
     */
    private GameRepository gameRepository;

    /**
     * CRUD-сервис для предметов из маркета
     */
    private MarketQueryService marketQueryService;

    /**
     * Сервис для добавление предметов в бд по ответам запросов на маркет
     */
    private CreateDBItemsByQueryResponseService createDBItemsByQueryResponseService;

    /**
     * Установка игры
     *
     * @param gameApp игра
     */
    public void setGameApp(GameApp gameApp) {
        this.gameApp = gameApp;
    }

    /**
     * Установка CRUD-сервиса для игр
     * @param gameRepository CRUD-сервис для игр
     */
    @Autowired
    public void setGameRepository(GameRepository gameRepository) {
        this.gameRepository = gameRepository;
    }

    /**
     * Установка CRUD-сервиса для предметов из маркета
     * @param marketQueryService CRUD-сервис для предметов из маркета
     */
    @Autowired
    public void setMarketQueryService(MarketQueryService marketQueryService) {
        this.marketQueryService = marketQueryService;
    }

    /**
     * Установка сервиса для добавление предметов в бд по ответам запросов на маркет
     * @param createDBItemsByQueryResponseService Сервис для добавление предметов в бд по ответам запросов на маркет
     */
    @Autowired
    public void setCreateDBItemsByQueryResponseService(CreateDBItemsByQueryResponseService createDBItemsByQueryResponseService) {
        this.createDBItemsByQueryResponseService = createDBItemsByQueryResponseService;
    }

    @PostConstruct
    public void init() {
        game = gameRepository.findByName(gameApp.toString());
    }

    @Override
    public void execute() {
        try {
            List<GetOrdersResponseBean> responseBeans = marketQueryService.getOrdersInParts(gameApp);
            ArrayList<String> itemClassInstanceIdList = new ArrayList<>();
            for(GetOrdersResponseBean responseBean : responseBeans) {
                for(MyOrder myOrder : responseBean.getOrders()) {
                    ClassInstanceId classInstanceId = myOrder.obtainClassInstanceId();
                    itemClassInstanceIdList.add(classInstanceId.obtainClassInstanceIdString());
                }
            }

            List<MassInfoResponseBean> responses = marketQueryService.massInfo(gameApp,
                    MassInfoRequestParametersBean.Sell.NONE,
                    MassInfoRequestParametersBean.Buy.NONE,
                    MassInfoRequestParametersBean.History.TOP_100,
                    MassInfoRequestParametersBean.Info.BASE, itemClassInstanceIdList);
            for (MassInfoResponseBean massInfoResponseBean : responses) {
                for (MassInfoResult itemInfo : massInfoResponseBean.getResult()) {
                    createDBItemsByQueryResponseService.addMarketItemToDb(itemInfo, game, SELL_PRIORITY, BUY_PRIORITY, true);
                }
            }

        } catch (MarketBadKeyException e) {
            e.printStackTrace();
        }
    }


}
