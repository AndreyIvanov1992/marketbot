package com.pirogsoft.marketbot.method.market.script;

import com.pirogsoft.marketbot.database.entities.Game;
import com.pirogsoft.marketbot.database.repository.GameRepository;
import com.pirogsoft.marketbot.database.repository.MarketItemRepository;
import com.pirogsoft.marketbot.database.repository.SteamItemRepository;
import com.pirogsoft.marketbot.database.service.fill.CreateDBItemsByQueryResponseService;
import com.pirogsoft.marketbot.job.api.Method;
import com.pirogsoft.marketbot.query.beans.market.response.MarketCsvRecordWrapperImpl;
import com.pirogsoft.marketbot.query.beans.market.response.itemdb.ItemdbDotaCsvRecordWrapper;
import com.pirogsoft.marketbot.query.parser.csv.CsvRecordWrappersCollection;
import com.pirogsoft.marketbot.query.beans.market.response.MarketItemFromCsv;
import com.pirogsoft.marketbot.service.GameApp;
import com.pirogsoft.marketbot.service.MarketQueryService;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;

/**
 * Метод заполнения БД из csv
 */
public class FillItemsFromMarket implements Method {

    public static Logger logger = Logger.getLogger(FillItemsFromMarket.class);

    /**
     * Нижняя граница цены в копейках ()
     */
    private int pirceFrontier;

    /**
     * Граница количества (берем топ countFrontier популярных вещей)
     */
    private int countFrontier;

    private MarketQueryService marketQueryService;

    /**
     * Игра
     */
    private GameApp gameApp;

    /**
     * Объект игры из БД
     */
    private Game game;

    private GameRepository gameRepository;

    /**
     * Разделитель в CSV файле
     */
    private char delimiter;

    /**
     * Путь к файлу
     */
    private String fileName;

    private CreateDBItemsByQueryResponseService createDBItemsByQueryResponseService;

    public void setPirceFrontier(int pirceFrontier) {
        this.pirceFrontier = pirceFrontier;
    }

    public void setGameApp(GameApp gameApp) {
        this.gameApp = gameApp;
    }

    public void setCountFrontier(int countFrontier) {
        this.countFrontier = countFrontier;
    }

    public void setDelimiter(char delimiter) {
        this.delimiter = delimiter;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    @Autowired
    public void setGameRepository(GameRepository gameRepository) {
        this.gameRepository = gameRepository;
    }

    @Autowired
    public void setMarketQueryService(MarketQueryService marketQueryService) {
        this.marketQueryService = marketQueryService;
    }

    @Autowired
    public void setCreateDBItemsByQueryResponseService(CreateDBItemsByQueryResponseService createDBItemsByQueryResponseService) {
        this.createDBItemsByQueryResponseService = createDBItemsByQueryResponseService;
    }

    @PostConstruct
    public void init() {
        game = gameRepository.findByName(gameApp.toString());
    }

    @Override
    public void execute() {

        try {
            CSVFormat csvFormat = CSVFormat.newFormat(delimiter);
            CSVParser csvParser = csvFormat.parse(new FileReader(fileName));
            CsvRecordWrappersCollection recordWrappersCollection = new CsvRecordWrappersCollection();
            recordWrappersCollection.setCsvParser(csvParser);

            //TODO: резолвер. вмето этой срани
            if(gameApp == GameApp.DOTA) {
                recordWrappersCollection.setCsvRecordClass(ItemdbDotaCsvRecordWrapper.class);
            } else {
                throw new RuntimeException("no record class for game " + gameApp);
            }

            List<MarketItemFromCsv> records = MarketCsvRecordWrapperImpl.getCsvRecordsListSortedByPopularity(recordWrappersCollection);
            int counter = 0;
            for(MarketItemFromCsv record : records) {
                if(record.getCPrice() < pirceFrontier) {
                    continue;
                }
                if(record.getCMarketHashName() != null) {
                    logger.info("add " + record.obtainClassInstanceId() + " ("+record.getCMarketHashName() +")");
                    createDBItemsByQueryResponseService.addMarketItemToDb(record, record.getCMarketHashName(), record.getCMarketName(), game, 0, 0, false);
                }
                counter++;
                if(counter > countFrontier) {
                    break;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }


    }
}
