package com.pirogsoft.marketbot.method.market;

import com.pirogsoft.marketbot.job.api.Method;
import com.pirogsoft.marketbot.method.market.recommendations.MakeReportProcessor;
import com.pirogsoft.marketbot.method.steam.UpdatePriceMethod;

/**
 * Метод, обновляющий цены стим и формирующий актуальный отчет.
 * Created by Andrey on 08.06.2018.
 */
public class UpdateSteamPriceAndMakeReport implements Method{

    private UpdatePriceMethod updatePriceMethod;

    private MakeReportProcessor makeReportProcessor;

    public void setUpdatePriceMethod(UpdatePriceMethod updatePriceMethod) {
        this.updatePriceMethod = updatePriceMethod;
    }

    public void setMakeReportProcessor(MakeReportProcessor makeReportProcessor) {
        this.makeReportProcessor = makeReportProcessor;
    }

    @Override
    public void execute() {
        updatePriceMethod.execute();
        makeReportProcessor.execute();
    }
}
