package com.pirogsoft.marketbot.method.market.correctprice;

import com.pirogsoft.marketbot.LogUtils;
import com.pirogsoft.marketbot.common.ClassInstanceId;
import com.pirogsoft.marketbot.database.entities.Game;
import com.pirogsoft.marketbot.database.entities.MarketItem;
import com.pirogsoft.marketbot.database.entities.SteamItem;
import com.pirogsoft.marketbot.database.repository.GameRepository;
import com.pirogsoft.marketbot.database.repository.MarketItemRepository;
import com.pirogsoft.marketbot.database.repository.SteamItemRepository;
import com.pirogsoft.marketbot.database.service.VariableService;
import com.pirogsoft.marketbot.exception.MarketBadKeyException;
import com.pirogsoft.marketbot.job.api.Method;
import com.pirogsoft.marketbot.query.beans.market.request.massinfo.MassInfoRequestParametersBean;
import com.pirogsoft.marketbot.query.beans.market.request.masssetpricebyidrequest.IdPricePair;
import com.pirogsoft.marketbot.query.beans.market.response.getmyselloffers.GetMySellOffersResponseBean;
import com.pirogsoft.marketbot.query.beans.market.response.getmyselloffers.MySellOffer;
import com.pirogsoft.marketbot.query.beans.market.response.massinfo.MassInfoHistoryItem;
import com.pirogsoft.marketbot.query.beans.market.response.massinfo.MassInfoResponseBean;
import com.pirogsoft.marketbot.query.beans.market.response.massinfo.MassInfoResult;
import com.pirogsoft.marketbot.query.beans.market.response.masssetpricebyid.MassSetPriceByIdResponseBean;
import com.pirogsoft.marketbot.service.GameApp;
import com.pirogsoft.marketbot.service.MarketQueryService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;
import java.util.*;

/**
 * Метод корректировки цен в контексте одного "приоритета" и игры.
 * Created by AnIvanov on 28.04.2018.
 */
public class CorrectPriceMethod implements Method {

    public static final Logger logger = Logger.getLogger(CorrectPriceMethod.class);

    /**
     * Количество процентов, на которое может откланяться цена от средней вниз
     */
    public static final double BOTTOM_BORDER_PERCENT = 10;

    /**
     * Количество процентов, на которое может откланяться цена от средней вверх
     */
    public static final double TOP_BORDER_PERCENT = 40;

    /**
     * Минимально возможная цена на маркете.
     */
    public static final int MIN_MARKET_PRICE = 50;

    /**
     * Предельный коэффициент различия цен в стиме и маркете, который нас устраивает
     */
    public static final double LESSION_COEFFICIENT = 0.72;

    /**
     * CRUD сервис ддя работы с предметами маркета
     */
    private MarketItemRepository marketItemRepository;

    /**
     * CRUD сервис ддя работы с играми
     */
    private GameRepository gameRepository;

    /**
     * Сервис для запросов на маркет
     */
    private MarketQueryService marketQueryService;

    /**
     * Приоритет. Метод работает только в рамках вещей одного приоритета.
     */
    private int priority;

    /**
     * Объект игры.
     */
    private GameApp gameApp;

    /**
     * Список вещей, которые у которых нужно корректировать цену.
     */
    private Map<ClassInstanceId, MarketItem> itemDbMap;

    /**
     * Сервис управления записями предметов STEAM
     */
    private SteamItemRepository steamItemRepository;

    /**
     * Коффициент коммиссии маркета
     */
    private double discountCoefficient;

    /**
     * Установка сервиса запросов на маркет. Устанавливается спрингом.
     *
     * @param marketQueryService Сервис для запросов на маркет
     */
    @Autowired
    public void setMarketQueryService(MarketQueryService marketQueryService) {
        this.marketQueryService = marketQueryService;
    }

    /**
     * Установка CRUD сервиса ддя работы предметами маркета. Устанавливается спрингом.
     *
     * @param marketItemRepository CRUD сервис ддя работы с предметами маркета
     */
    @Autowired
    public void setMarketItemRepository(MarketItemRepository marketItemRepository) {
        this.marketItemRepository = marketItemRepository;
    }

    @Autowired
    public void setGameRepository(GameRepository gameRepository) {
        this.gameRepository = gameRepository;
    }

    @Autowired
    public void setSteamItemRepository(SteamItemRepository steamItemRepository) {
        this.steamItemRepository = steamItemRepository;
    }

    /**
     * Получает приоритет
     *
     * @return приоритет
     */
    public int getPriority() {
        return priority;
    }

    /**
     * Устанавливает приоритет
     *
     * @param priority приоритет
     */
    public void setPriority(int priority) {
        this.priority = priority;
    }


    /**
     * Получение игры
     *
     * @return игра
     */
    public GameApp getGameApp() {
        return gameApp;
    }

    /**
     * Установка игры
     *
     * @param gameApp игра
     */
    public void setGameApp(GameApp gameApp) {
        this.gameApp = gameApp;
    }

    @PostConstruct
    public void init() {
        Game game = gameRepository.findByName(gameApp.toString());
        itemDbMap = new HashMap<>();
        List<MarketItem> itemList = marketItemRepository.findItemByGameAndSellPriority(game, priority);
        for (MarketItem marketItem : itemList) {
            itemDbMap.put(marketItem.obtainClassInstanceId(), marketItem);
        }

        double discount = game.getMarketCommission();
        discountCoefficient = (100 - discount) / 100;
    }


    @Override
    public void execute() {
        try {

            //Карта предмет -> список офеов
            Map<ClassInstanceId, List<MySellOffer>> itemsToSell = obtainMySellItemsMap();

            //список информаций по выставленным на продажу предметаим
            List<MassInfoResponseBean> massInfoResponses = obtainMassInfoResponses(itemsToSell.keySet());

            //Корректировка цен
            smartCorrectPrice(itemsToSell, massInfoResponses);

        } catch (MarketBadKeyException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Корректирует предметы из карты itemsToSell на основании информации по этим предметам из massInfoResponses
     *
     * @param itemsToSell       карта предметы->списки предложений, для которых, возможно, нужно менять цену
     * @param massInfoResponses результаты запросов на получение информации по предметам
     * @throws MarketBadKeyException Ключ апи маркета не действителен
     */
    private void smartCorrectPrice(Map<ClassInstanceId, List<MySellOffer>> itemsToSell, List<MassInfoResponseBean> massInfoResponses) throws MarketBadKeyException {
        ArrayList<IdPricePair> idPricePairs = new ArrayList<>();

        for (MassInfoResponseBean massInfoResponse : massInfoResponses) {
            if (marketQueryService.isSuccess(massInfoResponse)) {
                for (MassInfoResult massInfoResult : massInfoResponse.getResult()) {
                    int newPrice = calculatePrice(massInfoResult);
                    ClassInstanceId classInstanceId = massInfoResult.obtainClassInstanceId();
                    List<MySellOffer> mySellOffers = itemsToSell.get(classInstanceId);
                    for (MySellOffer mySellOffer : mySellOffers) {
                        int oldPrice = (int) (mySellOffer.getUiPrice() * 100);
                        if (oldPrice != newPrice) {
                            idPricePairs.add(new IdPricePair(Integer.parseInt(mySellOffer.getUiId()), newPrice));
                        }
                    }
                }
            } else {
                logger.error(LogUtils.obtainLogMessage("One of massInfoResponse is null or is not success"));
                marketQueryService.logErrorMessage(logger, massInfoResponse);
            }
        }

        List<MassSetPriceByIdResponseBean> responces = marketQueryService.massSetPriceById(gameApp, idPricePairs);
        for(MassSetPriceByIdResponseBean responce : responces) {
            if(!marketQueryService.isSuccess(responce)) {
                logger.error(LogUtils.obtainLogMessage("One of massInfoResponse is null or is not success"));
                marketQueryService.logErrorMessage(logger, responce);
            }
        }
    }

    /**
     * Получение информации по предметам на основании сета предметов
     *
     * @param mySellItems сет предметов
     * @return список информаций по предметам
     * @throws MarketBadKeyException Ключ апи маркета не действителен
     */
    private List<MassInfoResponseBean> obtainMassInfoResponses(Set<ClassInstanceId> mySellItems) throws MarketBadKeyException {
        ArrayList<String> classInstanceIdList = new ArrayList<>();
        for (ClassInstanceId classInstanceId : mySellItems) {
            classInstanceIdList.add(classInstanceId.obtainClassInstanceIdString());
        }

        return marketQueryService.massInfo(gameApp,
                MassInfoRequestParametersBean.Sell.TOP_1,
                MassInfoRequestParametersBean.Buy.NONE,
                MassInfoRequestParametersBean.History.TOP_10,
                MassInfoRequestParametersBean.Info.BASE,
                classInstanceIdList);
    }

    /**
     * Получение карты ClassInstanceId - список предложений. Только для предметов из itemDbMap
     *
     * @return Карта ClassInstanceId - список предложений.
     * @throws MarketBadKeyException Ключ апи маркета не действителен
     */
    private Map<ClassInstanceId, List<MySellOffer>> obtainMySellItemsMap() throws MarketBadKeyException {
        GetMySellOffersResponseBean responseBean = marketQueryService.getMySellOffers(gameApp);
        if (!marketQueryService.isSuccess(responseBean)) {
            logger.error(LogUtils.obtainLogMessage("Market error was during execution getMySellOffers query."));
            marketQueryService.logErrorMessage(logger, responseBean);
        }
        Map<ClassInstanceId, List<MySellOffer>> itemsToSell = new HashMap<>();
        for (MySellOffer mySellOffer : responseBean.getOffers()) {
            ClassInstanceId classInstanceId = mySellOffer.obtainClassInstanceId();
            if (itemDbMap.get(classInstanceId) != null) {
                List<MySellOffer> offers = itemsToSell.get(classInstanceId);
                if (offers == null) {
                    offers = new ArrayList<>();
                    itemsToSell.put(classInstanceId, offers);
                }
                offers.add(mySellOffer);
            }
        }
        return itemsToSell;
    }

    /**
     * Вычисляет новую цену для предмета
     *
     * @param massInfoResult результат получения информации по предмету
     * @return новая цена
     */
    private int calculatePrice(MassInfoResult massInfoResult) {
        int newPrice;
        int avgTop10Price = calculateAvragePrice(massInfoResult.getHistory().getHistory());
        int bottomBorder = (int) (avgTop10Price - (avgTop10Price * (BOTTOM_BORDER_PERCENT / 100)));
        int topBorder = (int) (avgTop10Price + (avgTop10Price * (TOP_BORDER_PERCENT / 100)));
        Integer bestOffer = null;
        if (massInfoResult.getSellOffers() != null) {
            bestOffer = massInfoResult.getSellOffers().getBestOffer();
        }
        SteamItem steamItem = steamItemRepository.findByMarketHashName(massInfoResult.getInfo().getMarketHashName());
        if(steamItem.getPrice() != null) {
            bottomBorder = Math.max(bottomBorder, calculateBottomSteamBorder(steamItem.getPrice()));
        }
        if (bestOffer == null || (bestOffer <= bottomBorder || bestOffer >= topBorder)) {
            newPrice = topBorder;
        } else {
            newPrice = bestOffer - 1;
            if (newPrice < MIN_MARKET_PRICE) {
                newPrice = MIN_MARKET_PRICE;
            }
        }
        return newPrice;
    }

    /**
     * Вычисляет среднюю цену из массива history
     *
     * @param history список продаж предмета
     * @return средняя цена
     */
    private int calculateAvragePrice(List<MassInfoHistoryItem> history) {
        int sum = 0;
        for (MassInfoHistoryItem historyItem : history) {
            sum += historyItem.getPrice();
        }
        return sum / history.size();
    }

    private int calculateBottomSteamBorder(int steamPrice) {
        return (int)(steamPrice * LESSION_COEFFICIENT / discountCoefficient);
    }

}
