package com.pirogsoft.marketbot.method.market.recommendations.view;

import com.pirogsoft.marketbot.method.market.recommendations.SteamItemInfo;
import com.pirogsoft.marketbot.service.GameApp;

import java.util.List;

/**
 * Представление отчета по рекомендациям покупок
 * Created by Andrey on 07.06.2018.
 */
public interface ReportView {
    /**
     * Генерация отчета
     *
     * @param steamItemInfoList список предметов Steam
     * @param gameApp игра
     */
    void generateView(List<SteamItemInfo> steamItemInfoList, GameApp gameApp);
}
