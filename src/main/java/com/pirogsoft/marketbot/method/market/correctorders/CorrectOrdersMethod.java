package com.pirogsoft.marketbot.method.market.correctorders;

import com.pirogsoft.marketbot.LogUtils;
import com.pirogsoft.marketbot.common.ClassInstanceId;
import com.pirogsoft.marketbot.database.entities.Game;
import com.pirogsoft.marketbot.database.entities.MarketItem;
import com.pirogsoft.marketbot.database.repository.GameRepository;
import com.pirogsoft.marketbot.database.repository.MarketItemRepository;
import com.pirogsoft.marketbot.exception.MarketBadKeyException;
import com.pirogsoft.marketbot.job.api.Method;
import com.pirogsoft.marketbot.query.beans.market.request.massinfo.MassInfoRequestParametersBean;
import com.pirogsoft.marketbot.query.beans.market.response.getorders.GetOrdersResponseBean;
import com.pirogsoft.marketbot.query.beans.market.response.getorders.MyOrder;
import com.pirogsoft.marketbot.query.beans.market.response.massinfo.MassInfoBuyOffers;
import com.pirogsoft.marketbot.query.beans.market.response.massinfo.MassInfoResponseBean;
import com.pirogsoft.marketbot.query.beans.market.response.massinfo.MassInfoResult;
import com.pirogsoft.marketbot.query.beans.market.response.processorder.ProcessOrderResponseBean;
import com.pirogsoft.marketbot.service.GameApp;
import com.pirogsoft.marketbot.service.MarketQueryService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;
import java.util.*;

/**
 * Корректирует цены запросов на покупку.
 * Created by AnIvanov on 06.05.2018.
 */
public class CorrectOrdersMethod implements Method {

    //TODO: некоторые все константы в свойства бина или в БД

    /**
     * Логгер
     */
    public static Logger logger = Logger.getLogger(CorrectOrdersMethod.class);

    /**
     * Мнежетель нижней границы цены по отношению к цене в стиме с учетом комиссии
     */
    public static final double BOTTOM_BORDER_MULTIPLIER = 0.5;

    /**
     * Мнежетель верхней границы цены по отношению к цене в стиме с учетом комиссии
     */
    public static final double TOP_BORDER_MULTIPLIER = 0.7;

    /**
     * Множетель комиссии в STEAM
     */
    public static final double STEAM_COMMISSION_MULTIPLIER = 0.87;

    /**
     * Средняя цена удара рунного молотка
     */
    public static final double HUMMER_HIT_PRICE = 3.5;

    /**
     * При флаге true заказы должны поучаться с помощью нескольких постраничный запросов
     */
    public static final boolean ORDERS_AMOUNT_GT_2000 = false;

    /**
     * CRUD сервис ддя работы с предметами маркета
     */
    private MarketItemRepository marketItemRepository;

    /**
     * CRUD сервис ддя работы с играми
     */
    private GameRepository gameRepository;

    /**
     * Сервис для запросов на маркет
     */
    private MarketQueryService marketQueryService;

    /**
     * Приоритет. Метод работает только в рамках вещей одного приоритета.
     */
    private int priority;

    /**
     * Объект игры.
     */
    private GameApp gameApp;

    /**
     * Объект игры из БД
     */
    private Game game;

    /**
     * Список вещей, которые у которых нужно корректировать цену.
     */
    private Map<ClassInstanceId, MarketItem> itemDbMap;

    /**
     * Список строковых представлений classInstanceId предметов из БД, для которых нужно выставлять цену.
     */
    private ArrayList<String> classInstanceIdStringDbList;

    /**
     * Установка сервиса запросов на маркет. Устанавливается спрингом.
     *
     * @param marketQueryService Сервис для запросов на маркет
     */
    @Autowired
    public void setMarketQueryService(MarketQueryService marketQueryService) {
        this.marketQueryService = marketQueryService;
    }

    /**
     * Установка CRUD сервиса ддя работы предметами маркета. Устанавливается спрингом.
     *
     * @param marketItemRepository CRUD сервис ддя работы с предметами маркета
     */
    @Autowired
    public void setMarketItemRepository(MarketItemRepository marketItemRepository) {
        this.marketItemRepository = marketItemRepository;
    }

    @Autowired
    public void setGameRepository(GameRepository gameRepository) {
        this.gameRepository = gameRepository;
    }

    /**
     * Получает приоритет
     *
     * @return приоритет
     */
    public int getPriority() {
        return priority;
    }

    /**
     * Устанавливает приоритет
     *
     * @param priority приоритет
     */
    public void setPriority(int priority) {
        this.priority = priority;
    }

    /**
     * Получение игры
     *
     * @return игра
     */
    public GameApp getGameApp() {
        return gameApp;
    }

    /**
     * Установка игры
     *
     * @param gameApp игра
     */
    public void setGameApp(GameApp gameApp) {
        this.gameApp = gameApp;
    }

    @PostConstruct
    public void init() {
        game = gameRepository.findByName(gameApp.toString());
        itemDbMap = new HashMap<>();
        List<MarketItem> itemList = marketItemRepository.findItemByGameAndBuyPriority(game, priority);
        classInstanceIdStringDbList = new ArrayList<>();
        for (MarketItem marketItem : itemList) {
            itemDbMap.put(marketItem.obtainClassInstanceId(), marketItem);
            classInstanceIdStringDbList.add(marketItem.obtainClassInstanceId().obtainClassInstanceIdString());
        }
    }

    @Override
    public void execute() {
        //Карта предмет -> ордер
        try {
            Map<ClassInstanceId, MyOrder> itemsToBuy = obtainMySellItemsMap();

            //список информаций по предметам, для которых нужны запросы на покупку.
            List<MassInfoResponseBean> massInfoResponses = marketQueryService.massInfo(gameApp,
                    MassInfoRequestParametersBean.Sell.NONE,
                    MassInfoRequestParametersBean.Buy.TOP_1,
                    MassInfoRequestParametersBean.History.NONE,
                    MassInfoRequestParametersBean.Info.BASE,
                    classInstanceIdStringDbList);

            //Корректировка цен
            smartCorrectPrice(itemsToBuy, massInfoResponses);
        } catch (MarketBadKeyException e) {
            //Останавливаем все
            logger.fatal(LogUtils.obtainLogMessage("Wrong market key, stopping bot."), e);
            throw new RuntimeException(e);
        }
    }

    /**
     * Получение карты всех "Моих ордеров".
     *
     * @return карта "моих ордеров"
     * @throws MarketBadKeyException Ключ апи маркета не действителен
     */
    private Map<ClassInstanceId, MyOrder> obtainMySellItemsMap() throws MarketBadKeyException {
        Map<ClassInstanceId, MyOrder> myOrderMap = new HashMap<>();

        if (ORDERS_AMOUNT_GT_2000) {
            List<GetOrdersResponseBean> responses = marketQueryService.getOrdersInParts(gameApp);
            for (GetOrdersResponseBean responseBean : responses) {
                fillMyOrderMap(myOrderMap, responseBean);
            }
        } else {
            fillMyOrderMap(myOrderMap, marketQueryService.getOrders(gameApp));
        }
        return myOrderMap;
    }

    /**
     * Заполняет карту "Моих ордеров" информацией из бина ответа на запрос получения всех "Моих ордеров"
     *
     * @param myOrderMap   Карта "моих ордеров"
     * @param responseBean Бин ответа
     */
    private void fillMyOrderMap(Map<ClassInstanceId, MyOrder> myOrderMap, GetOrdersResponseBean responseBean) {
        if(!marketQueryService.isSuccess(responseBean)) {
            logger.error(LogUtils.obtainLogMessage("An error occurred during getting GetOrdersResponseBean."));
            marketQueryService.logErrorMessage(logger, responseBean);
            return;
        }
        for (MyOrder myOrder : responseBean.getOrders()) {
            ClassInstanceId classInstanceId = myOrder.obtainClassInstanceId();
            if (itemDbMap.get(classInstanceId) != null) {
                myOrderMap.put(classInstanceId, myOrder);
            }
        }
    }

    /**
     * Изменение цены ордера. Ставит на копейку больше чем лучшее предложение,
     * но только в том случае, если цена попадает в настраиваемый интервал, который зависит от цены в стиме,
     * скидки на маркете и комиссии в стиме.
     *
     * @param itemsToBuy        карта "моих ордеров"
     * @param massInfoResponses результаты запросов на получение массовой информации по предметам,
     *                          для которых нужны запросы на покупку.
     * @throws MarketBadKeyException Ключ апи маркета не действителен
     */
    private void smartCorrectPrice(Map<ClassInstanceId, MyOrder> itemsToBuy, List<MassInfoResponseBean> massInfoResponses) throws MarketBadKeyException {
        for (MassInfoResponseBean massInfoResponse : massInfoResponses) {
            for (MassInfoResult massInfoResult : massInfoResponse.getResult()) {
                int newPrice = calculatePrice(massInfoResult);
                ClassInstanceId classInstanceId = massInfoResult.obtainClassInstanceId();
                MyOrder myOrder = itemsToBuy.get(classInstanceId);
                Integer myOrderPrice;
                if(myOrder == null || myOrder.getOPrice()  == null) {
                    myOrderPrice = 0;
                } else {
                    myOrderPrice = myOrder.getOPrice();
                }
                if (newPrice != myOrderPrice) {
                    ProcessOrderResponseBean responseBean;
                    try {
                        responseBean = marketQueryService.processOrder(gameApp, massInfoResult.getClassid(), massInfoResult.getInstanceid(), newPrice);
                    } catch (Exception e) {
                        logger.error(LogUtils.obtainLogMessage("An ecxeption occurred during execute ProcessOrder method for item " + classInstanceId), e);
                        continue;
                    }
                    if (marketQueryService.isSuccess(responseBean)) {
                        logger.debug(LogUtils.obtainLogMessage("Old Price " + myOrderPrice + " changed. New price is: " + newPrice + " for item " + classInstanceId));
                    } else {
                        logger.error(LogUtils.obtainLogMessage("An error occurred during execute ProcessOrder method for item " + classInstanceId));
                        marketQueryService.logErrorMessage(logger, responseBean);
                    }
                }
            }
        }
    }

    /**
     * Вычисляет новую цену ордера.
     *
     * @param massInfoResult предмет из ответа на запрос на получение массовой информации по предметам
     * @return новая цена
     */
    private int calculatePrice(MassInfoResult massInfoResult) {
        int newOrderPrice;
        ClassInstanceId classInstanceId = massInfoResult.obtainClassInstanceId();
        MarketItem databaseItem = itemDbMap.get(classInstanceId);
        boolean inscribed = databaseItem.getSteamItemByMarketHashName().isInscribed();
        int steamPrice;
        double steamProceeds;
        if (inscribed && databaseItem.getSteamItemByMarketHashName().getReferenceItemHashName() != null) {
            steamPrice = databaseItem.getSteamItemByMarketHashName().getReferenceSteamItem().getPrice();
            steamProceeds = steamPrice * STEAM_COMMISSION_MULTIPLIER - HUMMER_HIT_PRICE;
        } else {
            steamPrice = databaseItem.getSteamItemByMarketHashName().getPrice();
            steamProceeds = steamPrice * STEAM_COMMISSION_MULTIPLIER;
        }
        double discount = game.getMarketDiscount();
        double discountMultiplier = (100 - discount) / 100;
        int bottomBorder = (int) (steamProceeds / discountMultiplier * BOTTOM_BORDER_MULTIPLIER);
        int topBorder = (int) (steamProceeds / discountMultiplier * TOP_BORDER_MULTIPLIER);
        Integer bestOrderPrice = null;
        MassInfoBuyOffers massInfoBuyOffers = massInfoResult.getBuyOffers();
        if (massInfoBuyOffers != null) {
            bestOrderPrice = massInfoBuyOffers.getBestOffer();
        }
        if (bestOrderPrice == null || (bestOrderPrice <= bottomBorder || bestOrderPrice >= topBorder)) {
            newOrderPrice = bottomBorder;
        } else {
            newOrderPrice = bestOrderPrice + 1;
        }
        return newOrderPrice;
    }
}
