package com.pirogsoft.marketbot.method.market.recommendations.view;

import com.pirogsoft.marketbot.method.market.recommendations.MarketItemInfo;
import com.pirogsoft.marketbot.method.market.recommendations.SteamItemInfo;
import com.pirogsoft.marketbot.service.GameApp;

import java.io.FileWriter;
import java.io.IOException;
import java.text.MessageFormat;
import java.util.List;

/**
 * Вью для создания HTML отчета рекомендаций к покупке
 * Created by Andrey on 07.06.2018.
 */
public class HtmlReportView implements ReportView {

    /**
     * Название генерируемого файла
     */
    private String outputFileName;

    /**
     * Установка названия генерируемого фалйа
     * @param outputFileName
     */
    public void setOutputFileName(String outputFileName) {
        this.outputFileName = outputFileName;
    }

    /**
     * Шаблон HTML документа
     */
    public static final String DOCUMENT_TEMPLATE = "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01//EN\" \"http://www.w3.org/TR/html4/strict.dtd\">\n" +
            "<html>\n" +
            " <head>\n" +
            "  <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">\n" +
            "  <title>{0}</title>\n" +
            " </head>\n" +
            " <body>\n" +
            "  <h1>{0}</h1>\n" +
            "  {1}\n" +
            " </body>\n" +
            "</html>";


    /**
     * Шаблон таблицы
     */
    public static final String TABLE_TEMPLATE = "<table>\n" +
            "  {0}\n" +
            "</table>";

    /**
     * Шаблон строки таблицы
     */
    public static final String ROW_TEMPLATE = "<tr>\n" +
            "  {0}\n" +
            "</tr>";

    /**
     * Шаблон ячейки таблицы
     */
    public static final String CELL_TEMPLATE = "<td>\n" +
            "  {0}\n" +
            "</td>";

    /**
     * Шаблон заголовочной ячейки таблицы
     */
    public static final String HEADER_CELL_TEMPLATE = "<th>\n" +
            "  {0}\n" +
            "</th>";


    /**
     * Шаблон списка
     */
    public static final String UL_TEMPLATE = "<ul>\n" +
            "  {0}\n" +
            "</ul>";

    /**
     * Шаблон элемента списка
     */
    public static final String LI_TEMPLATE = "<li>\n" +
            "  {0}\n" +
            "</li>";

    /**
     * Шаблон ссылки
     */
    public static final String A_TEMPLATE = "<a href=\"{0}\">\n" +
            "  {1}\n" +
            "</a>";

    /**
     * Шаблон ссылки на предмет маркета
     */
    public static final String MARKET_ITEM_LINK_TEMPLATE = "{0}/item/{1}";

    /**
     * Шаблон ссылки на предмет стим
     */
    public static final String STEAM_ITEM_LINK_TEMPLATE = "https://steamcommunity.com/market/listings/{0}/{1}";

    @Override
    public void generateView(List<SteamItemInfo> steamItemInfoList, GameApp gameApp) {
        try(FileWriter writer = new FileWriter(outputFileName, false))
        {
            writer.write(obtainReportText(steamItemInfoList, gameApp));
            writer.flush();
        }
        catch(IOException ex){

            System.out.println(ex.getMessage());
        }
    }

    /**
     * Получение HTML текста отчета
     * @param steamItemInfoList Список предметов стим
     * @param gameApp Игра
     * @return HTML текст отчета
     */
    private String obtainReportText(List<SteamItemInfo> steamItemInfoList, GameApp gameApp) {
        return MessageFormat.format(DOCUMENT_TEMPLATE, "Рекомендации к покупке", obtainReportTable(steamItemInfoList, gameApp));
    }

    /**
     * Получение HTML таблицы
     *
     * @param steamItemInfoList Список предметов стим
     * @param gameApp Игра
     * @return HTML таблицы
     */
    private String obtainReportTable(List<SteamItemInfo> steamItemInfoList, GameApp gameApp) {
        return MessageFormat.format(TABLE_TEMPLATE, obtainRows(steamItemInfoList, gameApp));

    }

    /**
     * Получение HTML строк таблицы
     *
     * @param steamItemInfoList Список предметов стим
     * @param gameApp Игра
     * @return HTML строк таблицы
     */
    private String obtainRows(List<SteamItemInfo> steamItemInfoList, GameApp gameApp) {
        StringBuilder rows = new StringBuilder("");
        rows.append( MessageFormat.format(ROW_TEMPLATE, obtainHeaderRow()));
        for(SteamItemInfo steamItemInfo : steamItemInfoList) {
            rows.append(MessageFormat.format(ROW_TEMPLATE, obtainRow(steamItemInfo, gameApp)));
        }
        return rows.toString();
    }

    /**
     * Получение HTML строки-заголовка таблицы
     * @return HTML строки-заголовка таблицы
     */
    private String obtainHeaderRow() {
        StringBuilder row = new StringBuilder("");
        row.append(obtainHeaderCell("Список ссылок предметов с марета"));
        row.append(obtainHeaderCell("Ссылка стим"));
        row.append(obtainHeaderCell("Цена в стиме"));
        row.append(obtainHeaderCell("Выгода"));
        row.append(obtainHeaderCell("Рекодмендуемое количество к покупке"));
        return row.toString();
    }

    /**
     * Получение HTML строки таблицы
     *
     * @param steamItemInfo Список предметов стим
     * @param gameApp Игра
     * @return HTML строки таблицы
     */
    private String obtainRow(SteamItemInfo steamItemInfo, GameApp gameApp) {
        StringBuilder row = new StringBuilder("");
        //Список ссылок предметов с марета
        row.append(obtainCell(obtainMarketItemList(steamItemInfo.getMarketItems(), gameApp)));
        //Ссылка стим
        row.append(obtainCell(сreateSteamtLink(steamItemInfo, gameApp)));
        //Цена в стиме
        row.append(obtainCell(String.valueOf(steamItemInfo.getSteamItem().getPrice())));
        //Выгода
        row.append(obtainCell(String.valueOf(steamItemInfo.getProfit())));
        //Рекодмендуемое количество к покупке.
        row.append(obtainCell(String.valueOf(steamItemInfo.getRecommendedAmount())));
        return row.toString();
    }

    private String сreateSteamtLink(SteamItemInfo steamItemInfo, GameApp gameApp) {
        String link = MessageFormat.format(STEAM_ITEM_LINK_TEMPLATE, gameApp.getSteamAppId(), steamItemInfo.getSteamItem().getMarketHashName());
        return MessageFormat.format(A_TEMPLATE, link, steamItemInfo.getSteamItem().getMarketHashName());
    }

    /**
     * Получение HTML ячейки таблицы
     *
     * @param content контент ячейки
     * @return HTML ячейки таблицы
     */
    private String obtainCell(String content) {
        return MessageFormat.format(CELL_TEMPLATE, content);
    }

    /**
     * Получение HTML ячейки-заголовка таблицы
     *
     * @param content контент ячейки
     * @return HTML ячейки-заголовка таблицы
     */
    private String obtainHeaderCell(String content) {
        return MessageFormat.format(HEADER_CELL_TEMPLATE, content);
    }

    /**
     * Получение HTML списка предметов маркета
     *
     * @param marketItemInfoList список предметов маркета
     * @param gameApp игра
     * @return HTML списка предметов маркета
     */
    private String obtainMarketItemList(List<MarketItemInfo> marketItemInfoList, GameApp gameApp) {
        StringBuilder liList = new StringBuilder();
        for(MarketItemInfo marketItemInfo : marketItemInfoList) {
            liList.append(MessageFormat.format(LI_TEMPLATE, сreateMarketLink(marketItemInfo, gameApp)));
        }
        return MessageFormat.format(UL_TEMPLATE, liList);
    }

    /**
     * Получение HTML ссылки на предмет маркета
     * @param marketItemInfo предмет маркета
     * @param gameApp игра
     * @return HTML ссылка на предмет маркета
     */
    private String сreateMarketLink(MarketItemInfo marketItemInfo, GameApp gameApp) {
        if(marketItemInfo == null || marketItemInfo.getResult() == null){
            return "";
        }
        String link =  MessageFormat.format(MARKET_ITEM_LINK_TEMPLATE, gameApp.getMarketApiUrl(), marketItemInfo.getResult().obtainClassInstanceId().obtainClassInstanceIdString("-"));
        return MessageFormat.format(A_TEMPLATE, link, marketItemInfo.getResult().getInfo().getMarketName());
    }

}
