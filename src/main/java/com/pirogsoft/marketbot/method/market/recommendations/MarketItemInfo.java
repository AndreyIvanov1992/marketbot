package com.pirogsoft.marketbot.method.market.recommendations;

import com.pirogsoft.marketbot.database.entities.SteamItem;
import com.pirogsoft.marketbot.query.beans.market.response.massinfo.MassInfoHistoryItem;
import com.pirogsoft.marketbot.query.beans.market.response.massinfo.MassInfoResult;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

/**
 * Created by Andrey on 06.06.2018.
 */
public class MarketItemInfo {

    public static final int AMOUNT_OF_CONSIDERED_HISTORY_ITEMS = 10;

    public static final double AMOUNT_COEFFICIENT = 17;

    private MassInfoResult result;

    private SteamItem steamItem;

    private double recommendedAmount;

    private double profit;

    public MarketItemInfo(MassInfoResult result, SteamItem steamItem) {
        this.result = result;
        this.steamItem = steamItem;
        recommendedAmount = calculateRecommendedAmount();
        profit = calculateProfit();
    }

    public MassInfoResult getResult() {
        return result;
    }

    public SteamItem getSteamItem() {
        return steamItem;
    }

    private double calculateProfit() {
        //Считаем среднюю цену за 10 последних продаж
        double middlePrice = obtainMiddlePrice();
        if(steamItem == null || steamItem.getPrice() == null || steamItem.getPrice() == 0) {
            return 0;
        } else {
            return middlePrice / steamItem.getPrice();
        }
    }

    private double calculateRecommendedAmount() {
        if(result == null || result.getHistory() == null) {
            return 0;
        }
        int numberOfHistoryItem = result.getHistory().getNumber();
        LocalDateTime firstSaleDate = result.getHistory().getHistory().get(numberOfHistoryItem - 1).getDateTime();

        long resultDays = ChronoUnit.HOURS.between(LocalDateTime.now(), firstSaleDate);
        return ((double)numberOfHistoryItem) / resultDays * AMOUNT_COEFFICIENT;
    }

    private double obtainMiddlePrice() {
        int sum = 0;
        int counter = 0;
        if(result == null || result.getHistory() == null) {
            return 0;
        }
        for(MassInfoHistoryItem historyItem : result.getHistory().getHistory()) {
            sum += historyItem.getPrice();
            counter++;
            if(counter >= AMOUNT_OF_CONSIDERED_HISTORY_ITEMS) {
                break;
            }
        }
        double discount = steamItem.getGame().getMarketCommission();
        double discountCoefficient = (100 - discount) / 100;
        return ((double)sum) / AMOUNT_OF_CONSIDERED_HISTORY_ITEMS * discountCoefficient;
    }

    public double getRecommendedAmount() {
        return recommendedAmount;
    }

    public double getProfit() {
        return profit;
    }
}
