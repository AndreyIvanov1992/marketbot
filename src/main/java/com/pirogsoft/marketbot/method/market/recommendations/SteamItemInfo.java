package com.pirogsoft.marketbot.method.market.recommendations;

import com.pirogsoft.marketbot.database.entities.SteamItem;

import java.util.List;

/**
 * Created by Andrey on 07.06.2018.
 */
public class SteamItemInfo {
    private SteamItem steamItem;

    private List<MarketItemInfo> marketItems;

    private double recommendedAmount;

    private double profit;

    public SteamItemInfo(SteamItem steamItem, List<MarketItemInfo> marketItems) {
        this.steamItem = steamItem;
        this.marketItems =  marketItems;
        profit = calculateProfit();
        recommendedAmount = calculateRecommendedAmount();
    }

    public SteamItem getSteamItem() {
        return steamItem;
    }

    public List<MarketItemInfo> getMarketItems() {
        return marketItems;
    }

    private double calculateRecommendedAmount() {
        double sum = 0;
        for(MarketItemInfo marketItem : marketItems) {
            sum += marketItem.getRecommendedAmount();
        }
        return sum;
    }

    private double calculateProfit() {
        MarketItemInfo minProfitMarketItem = obtainMinProfitIndex();
        if(minProfitMarketItem == null) {
            return 0;
        }
        return minProfitMarketItem.getProfit();
    }

    private MarketItemInfo obtainMinProfitIndex() {
        Double minProfit = null;
        MarketItemInfo minProfitMarketItem = null;
        for (MarketItemInfo marketItem : marketItems) {
            double itemProfit = marketItem.getProfit();
            if (minProfit == null || itemProfit < minProfit) {
                minProfit = itemProfit;
                minProfitMarketItem = marketItem;
            }
        }
        return minProfitMarketItem;
    }

    public double getRecommendedAmount() {
        return recommendedAmount;
    }

    public double getProfit() {
        return profit;
    }
}
