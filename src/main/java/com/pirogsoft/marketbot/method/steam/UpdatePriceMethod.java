package com.pirogsoft.marketbot.method.steam;

import com.pirogsoft.marketbot.LogUtils;
import com.pirogsoft.marketbot.database.entities.Game;
import com.pirogsoft.marketbot.database.entities.SteamItem;
import com.pirogsoft.marketbot.database.repository.GameRepository;
import com.pirogsoft.marketbot.database.repository.SteamItemRepository;
import com.pirogsoft.marketbot.job.api.Method;
import com.pirogsoft.marketbot.query.beans.steam.request.SteamCurrency;
import com.pirogsoft.marketbot.query.beans.steam.response.PriceOverviewResponseBean;
import com.pirogsoft.marketbot.service.GameApp;
import com.pirogsoft.marketbot.service.SteamQueryService;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;

/**
 * Метод обновления цен предметов Steam
 * Created by AnIvanov on 10.05.2018.
 */
public class UpdatePriceMethod implements Method {

    public Logger logger = Logger.getLogger(UpdatePriceMethod.class);

    /**
     * Время между запросами
     */
    public static final int SECONDS_BETWEEN_QUERIES = 3000;

    /**
     * Максимальное количество попыток запроса для одной вещи
     */
    public static final int MAX_NUMBER_OF_ATTEMPTS = 3;

    /**
     * Сервис для запросов в steam
     */
    private SteamQueryService steamQueryService;

    /**
     * CRUD-сервис для предметов steam
     */
    private SteamItemRepository steamItemRepository;

    /**
     * CRUD сервис ддя работы с играми
     */
    private GameRepository gameRepository;

    /**
     * Игра
     */
    private GameApp game;

    /**
     * При включенно флаге, цены обновляются только для "рунированных" предметов
     */
    private Boolean inscribed;

    /**
     * Список вещей steam из БД
     */
    private List<SteamItem> steamItems;

    /**
     * Объект игры из БД
     */
    private Game gameDBItem;

    /**
     * Установка сервиса для запросов в steam
     *
     * @param steamQueryService Сервис для запросов в steam
     */
    @Autowired
    public void setSteamQueryService(SteamQueryService steamQueryService) {
        this.steamQueryService = steamQueryService;
    }

    /**
     * Установка CRUD-сервиса для предметов steam
     *
     * @param steamItemRepository CRUD-сервис для предметов steam
     */
    @Autowired
    public void setSteamItemRepository(SteamItemRepository steamItemRepository) {
        this.steamItemRepository = steamItemRepository;
    }

    /**
     * Установка CRUD сервис ддя работы с играми
     *
     * @param gameRepository CRUD сервис ддя работы с играми
     */
    @Autowired
    public void setGameRepository(GameRepository gameRepository) {
        this.gameRepository = gameRepository;
    }

    /**
     * Установка игры
     *
     * @param game игра
     */
    public void setGame(GameApp game) {
        this.game = game;
    }

    /**
     * Установка признака "рунирования"
     *
     * @param inscribed признак рунирования
     */
    public void setInscribed(Boolean inscribed) {
        this.inscribed = inscribed;
    }

    /**
     * Метод инициализации
     */
    @PostConstruct
    public void init() {
        if (game != null) {
            gameDBItem = gameRepository.findByName(game.toString());
        }

        if (inscribed == null && game == null) {
            steamItems = steamItemRepository.findAll();
        } else if (inscribed == null && game != null) {
            steamItems = steamItemRepository.findByGame(gameDBItem);
        } else if (inscribed != null && game == null) {
            steamItems = steamItemRepository.findByInscribed(inscribed);
        } else {
            steamItems = steamItemRepository.findByGameAndInscribed(gameDBItem, inscribed);
        }
    }

    @Override
    public void execute() {
        for (SteamItem steamItem : steamItems) {
            try {
                boolean success;

                PriceOverviewResponseBean responseBean = steamQueryService.priceOverview(game, steamItem.getMarketHashName(), SteamCurrency.RUB);
                success = responseBean != null && responseBean.isSuccess();
                Integer newPrice = null;
                if (success) {
                    newPrice = responseBean.obtainLowerPriceInteger();
                }
                if (success && newPrice != null) {

                    steamItem.setPrice(newPrice);
                    steamItem.setLastPriceUpdate(Timestamp.valueOf(LocalDateTime.now()));
                    logger.debug(LogUtils.obtainLogMessage("price updated. New price is " + newPrice + " for item " + steamItem.getMarketHashName()));
                    steamItemRepository.save(steamItem);
                } else {
                    logger.error(LogUtils.obtainLogMessage("update price error for item " + steamItem.getMarketHashName()));
                }
            } catch(Exception e) {
                logger.error(LogUtils.obtainLogMessage("update price exception for item " + steamItem.getMarketHashName()), e);
            }
        }
    }
}
