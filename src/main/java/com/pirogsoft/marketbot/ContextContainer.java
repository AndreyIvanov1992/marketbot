package com.pirogsoft.marketbot;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Created by AnIvanov on 14.04.2018.
 */
public class ContextContainer {

    private static ContextContainer ourInstance;

    ApplicationContext context;

    public static ContextContainer getInstance() {
        if(ourInstance == null) {
            ourInstance = new ContextContainer();
        }

        return ourInstance;
    }

    private ContextContainer() {
        context = new ClassPathXmlApplicationContext("ApplicationContext.xml");
    }

    public ApplicationContext getContext() {
        return context;
    }
}
