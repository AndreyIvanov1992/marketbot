package com.pirogsoft.marketbot.test;

import com.pirogsoft.marketbot.ContextContainer;
import com.pirogsoft.marketbot.exception.MarketBadKeyException;
import com.pirogsoft.marketbot.job.api.Method;
import com.pirogsoft.marketbot.method.market.correctorders.CorrectOrdersMethod;
import com.pirogsoft.marketbot.method.market.correctprice.CorrectPriceMethod;
import com.pirogsoft.marketbot.method.market.recommendations.MakeReportProcessor;
import com.pirogsoft.marketbot.method.market.recommendations.view.HtmlReportView;
import com.pirogsoft.marketbot.method.market.script.FillItemsFromMarket;
import com.pirogsoft.marketbot.method.market.script.FillItemsToBuy;
import com.pirogsoft.marketbot.method.market.script.FillItemsToSell;
import com.pirogsoft.marketbot.query.Query;
import com.pirogsoft.marketbot.query.beans.TestIpResponse;
import com.pirogsoft.marketbot.query.beans.market.request.masssetpricebyidrequest.IdPricePair;
import com.pirogsoft.marketbot.query.beans.market.request.massinfo.MassInfoRequestParametersBean;
import com.pirogsoft.marketbot.query.beans.market.request.masssetpricebyidrequest.MassSetPriceByIdRequestParametersBean;
import com.pirogsoft.marketbot.query.beans.market.request.processorder.ProcessOrderParametersBean;
import com.pirogsoft.marketbot.query.beans.market.response.currentdb.CurrentDbResponseBean;
import com.pirogsoft.marketbot.query.beans.market.response.getmyselloffers.GetMySellOffersResponseBean;
import com.pirogsoft.marketbot.query.beans.market.response.getorders.GetOrdersResponseBean;
import com.pirogsoft.marketbot.query.beans.market.response.itemdb.ItemdbDotaCsvRecordWrapper;
import com.pirogsoft.marketbot.query.beans.market.response.massinfo.MassInfoResponseBean;
import com.pirogsoft.marketbot.query.beans.market.response.masssetpricebyid.MassSetPriceByIdResponseBean;
import com.pirogsoft.marketbot.query.beans.market.response.processorder.ProcessOrderResponseBean;
import com.pirogsoft.marketbot.query.beans.steam.request.PriceOverviewRequestBean;
import com.pirogsoft.marketbot.query.beans.steam.request.SteamCurrency;
import com.pirogsoft.marketbot.query.beans.steam.response.PriceOverviewResponseBean;
import com.pirogsoft.marketbot.query.parser.csv.CsvRecordWrappersCollection;
import com.pirogsoft.marketbot.service.GameApp;
import com.pirogsoft.marketbot.service.MarketQueryService;
import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.*;

/**
 * Created by AnIvanov on 03.04.2018.
 */
public class Test {

    public static Logger logger = Logger.getLogger(Test.class);

    public static final String MARKET_KEY = "QHaT04e6sX29j7xrIt07cKZJzUv7607";

    public static final String CS_MARKET_API_URL = "https://market.csgo.com";

    public static final String DOTA_MARKET_API_URL = "https://market.dota2.net";

    private static LocalDateTime startTime;

    public static void main(String[] args) {
        ContextContainer.getInstance().getContext().getBean(MakeReportProcessor.class).execute();
        System.out.println("the end");
    }

    private static void testFillItemsFromMarket() {
        ApplicationContext context = ContextContainer.getInstance().getContext();
        FillItemsFromMarket fillItemsFromMarket = context.getBean(FillItemsFromMarket.class);
        fillItemsFromMarket.execute();
    }

    private static void testItemdb() {
        ApplicationContext context = ContextContainer.getInstance().getContext();
        MarketQueryService marketQueryService = context.getBean(MarketQueryService.class);
        CsvRecordWrappersCollection<ItemdbDotaCsvRecordWrapper> result = marketQueryService.getItemdbDota();

        int wrongCount = 0;

        List<Map<String,Object>> records = new ArrayList<>();
        for(ItemdbDotaCsvRecordWrapper record : result) {
            if(!record.getCClassid().isEmpty() && !record.getCInstanceid().isEmpty()) {
                Map row = new HashMap<>();
                records.add(row);
                row.put("classId", record.getCClassid());
                row.put("instanceId", record.getCInstanceid());
                row.put("marketHashName", record.getCMarketHashName());
                row.put("popularity", record.getCPopularity());
            }

            try{
                Integer.parseInt(record.getCClassid());
            } catch (NumberFormatException e) {
                wrongCount++;
            }
        }
        records.sort((o1, o2) -> {
            if((Integer)o1.get("popularity") < (Integer)o2.get("popularity")) {
                return 1;
            } else if ((Integer)o1.get("popularity") > (Integer)o2.get("popularity")) {
                return -1;
            } else {
                return 0;
            }
        });

        for(Map record : records) {
            logger.info("classId: " + record.get("classId"));
            logger.info("instanceId: " + record.get("instanceId"));
            logger.info("marketHashName: " + record.get("marketHashName"));
            logger.info("popularity: " + record.get("popularity"));
            logger.info("==========================================================================================");
        }
        logger.info("Wrong count is " + wrongCount);
        System.out.println("the end");
    }

    private static void testCurrent7300() {
        ApplicationContext context = ContextContainer.getInstance().getContext();
        MarketQueryService marketQueryService = (MarketQueryService) context.getBean("marketQueryService");
        CurrentDbResponseBean responseBean = marketQueryService.getCurrent730(GameApp.DOTA);
        System.out.println(responseBean.getDb());
        System.out.println(responseBean.getDateTime());
    }

    private static void testIp() {
        Query<Object, TestIpResponse> query = (Query) ContextContainer.getInstance().getContext().getBean("testIpQuery");
        TestIpResponse testIpResponse = null;
        try {
            testIpResponse = query.execute(null);
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println(testIpResponse.getIp());
    }

    private static void testUpdatePriceMethod() {
        ApplicationContext context = ContextContainer.getInstance().getContext();
        Method method = (Method) context.getBean("updatePriceMethod");
        method.execute();
    }

    private static void testSteamPriceOverview() {
        ApplicationContext context = ContextContainer.getInstance().getContext();
        Query<PriceOverviewRequestBean, PriceOverviewResponseBean> query = (Query) ContextContainer.getInstance().getContext().getBean("priceOverviewQuery");
        PriceOverviewRequestBean request = new PriceOverviewRequestBean();
        request.setMarketHashName("Inscribed Razzil's Midas Knuckles");
        request.setGameApp(GameApp.DOTA);
        request.setCurrency(SteamCurrency.RUB);
        PriceOverviewResponseBean resp = null;
        try {
            resp = query.execute(request);
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("isSuccess: " + resp.isSuccess());
        System.out.println("lowePrice: " + resp.getLowerPrice());
        System.out.println("volume: " + resp.getVolume());
        System.out.println("lowerPriceInt: " + resp.obtainLowerPriceInteger());
    }


    private static void testCorrectOrdersMethod() {
        ApplicationContext context = ContextContainer.getInstance().getContext();
        CorrectOrdersMethod correctPriceMethod = context.getBean(CorrectOrdersMethod.class);
        startTime = LocalDateTime.now();
        correctPriceMethod.execute();
    }

    private static void testCorrectPriceMethod() {
        ApplicationContext context = ContextContainer.getInstance().getContext();
        CorrectPriceMethod correctPriceMethod = context.getBean(CorrectPriceMethod.class);
        correctPriceMethod.execute();
    }

    private static void testFillItemsToBuy() {
        ApplicationContext context = ContextContainer.getInstance().getContext();
        FillItemsToBuy fillItemsToBuy = (FillItemsToBuy) context.getBean("fillItemsToBuy");
        fillItemsToBuy.execute();
    }

    private static void testFillItemsToSell() {
        ApplicationContext context = ContextContainer.getInstance().getContext();
        FillItemsToSell fillItemsToSell = (FillItemsToSell) context.getBean("fillItemsToSell");
        fillItemsToSell.execute();
    }

    private static void testGetMySellOffersMarket() {
        ApplicationContext context = ContextContainer.getInstance().getContext();
        MarketQueryService marketQueryService = (MarketQueryService) context.getBean("marketQueryService");
        try {
            GetMySellOffersResponseBean resp = marketQueryService.getMySellOffers(GameApp.CS);
            System.out.println(resp);
        } catch (MarketBadKeyException e) {
            e.printStackTrace();
        }
    }

    private static void testMassInfoMarket() {
        MassInfoRequestParametersBean parameters = new MassInfoRequestParametersBean();
        parameters.setGameApp(GameApp.DOTA);
        parameters.setMarketKey(MARKET_KEY);
        parameters.setBuy(MassInfoRequestParametersBean.Buy.TOP_50);
        parameters.setSell(MassInfoRequestParametersBean.Sell.TOP_50);
        parameters.setInfo(MassInfoRequestParametersBean.Info.STEAM);
        parameters.setHistory(MassInfoRequestParametersBean.History.TOP_100);
        List<String> classInstanceIds = new ArrayList<>();
        parameters.setList(classInstanceIds);
        classInstanceIds.add("560600162_0");
        Query<MassInfoRequestParametersBean, MassInfoResponseBean> query = (Query) ContextContainer.getInstance().getContext().getBean("massInfoMarketQuery");
        MassInfoResponseBean resp = null;
        try {
            resp = query.execute(parameters);
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println(resp.getResult().get(0).getSellOffers().getOffers().get(0).getPrice());
    }

    private static void testMassSetPriceByIdMarket() {
        MassSetPriceByIdRequestParametersBean parameters = new MassSetPriceByIdRequestParametersBean();
        parameters.setGameApp(GameApp.CS);
        parameters.setMarketKey(MARKET_KEY);
        ArrayList<IdPricePair> list = new ArrayList<>();
        parameters.setList(list);
        list.add(new IdPricePair(458052212, 6666));
        list.add(new IdPricePair(455038736, 7777));

        Query<MassSetPriceByIdRequestParametersBean, MassSetPriceByIdResponseBean> query = (Query) ContextContainer.getInstance().getContext().getBean("massSetPriceByIdQuery");
        MassSetPriceByIdResponseBean resp = null;
        try {
            resp = query.execute(parameters);
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println(resp.getItems().get(0).getStatus().getResult());

    }

    private static void testProcessOrderMakret() {
        ProcessOrderParametersBean parametersBean = new ProcessOrderParametersBean();
        parametersBean.setGameApp(GameApp.DOTA);
        parametersBean.setMarketKey(MARKET_KEY);
        parametersBean.setClassid("560600162");
        parametersBean.setInstanceid("0");
        parametersBean.setPrice(32);
        Query<ProcessOrderParametersBean, ProcessOrderResponseBean> query = (Query) ContextContainer.getInstance().getContext().getBean("processOrderQuery");
        ProcessOrderResponseBean resp = null;
        try {
            resp = query.execute(parametersBean);
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println(resp.isSuccess());
        System.out.println(resp.getError());
        System.out.println(resp.getWay());
        System.out.println(resp.getPrice());
        System.out.println(resp.getItem());
        System.out.println(resp.getBuyError());
    }

    private static void testGetOrdersMarket() {
        ApplicationContext context = ContextContainer.getInstance().getContext();
        MarketQueryService marketQueryService = (MarketQueryService) context.getBean("marketQueryService");
        try {
            GetOrdersResponseBean resp = marketQueryService.getOrders(GameApp.DOTA, 1);
            System.out.println("ResponseSize = " + resp.getOrders().size());
        } catch (MarketBadKeyException e) {
            e.printStackTrace();
        }
    }
}
