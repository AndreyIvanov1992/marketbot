package com.pirogsoft.marketbot.job.api;

import java.time.LocalDateTime;

/**
 * Джоб (обертка для метода, который должен периодически запускаться)
 * Created by AnIvanov on 03.04.2018.
 */
public interface Job extends Runnable, ObservebleSupport {

    /**
     * Получает признак поставлен ли джоб на паузу
     * @return значение признака
     */
    boolean isPaused();

    /**
     * устанавливает признак "поставлен ли джоб на паузу"
     * @param paused значение признака
     */
    void setPaused(boolean paused);

    /**
     * Получает период, с которым выполняется джоб. (время между концом работы метода и началом следующего запуска)
     * @return значение периода в миллисекундах
     */
    int getPeriod();

    /**
     * Устанавливает период, с которым выполняется джоб. (время между концом работы метода и началом следующего запуска)
     * @param period значение периода в миллисекундах
     */
    void setPeriod(int period);

    /**
     * Получает признак того, остановлен ли метод
     * @return значение признака
     */
    boolean isStopped();

    /**
     * Получает описание джоба
     * @return описание джоба
     */
    String getDescription();

    /**
     * Устанавливает описание джоба
     * @param description описание джоба
     */
    void setDescription(String description);

    /**
     * Получает полсдедний запуск метода
     * @return дата и время последнего запуска метода
     */
    LocalDateTime getLastDateTimeStart();

    /**
     * Получает последнее окончание работы метода
     * @return дата и время окончания работы метода
     */
    LocalDateTime getLastDateTimeFinished();

    /**
     * Получает, работает ли метод в данный момент
     * @return true - в случае если работает, false - в случае если не работает
     */
    boolean isMethodRunning();

    /**
     * Устанавливает метод, который должен регулярно выполняться
     * @param method метод, который должен регулярно выполняться
     */
    void setMethod(Method method);


    /**
     * Останавливает джоб
     */
    void stop();

    /**
     * возвращает состояние джоба
     * @return состояние джоба
     */
    JobState getJobState();

}
