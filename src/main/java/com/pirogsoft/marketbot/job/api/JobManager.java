package com.pirogsoft.marketbot.job.api;

import java.util.Map;

/**
 * Управление джобами
 * Created by AnIvanov on 03.04.2018.
 */
public interface JobManager {

    /**
     * Останавлевает все джобы
     */
    void stopAll();

    /**
     * Получает карту джобов
     * @return карта джобов
     */
    Map<String, Job> getJobs();

    /**
     * Устанавливает карту джобов
     * @param jobs карта джобов
     */
    void setJobs(Map<String, Job> jobs);

    /**
     * Точка входа. Запускает все джобы в менеджере.
     */
    void run();

    /**
     * Ставит на паузу джоб
     * @param jobName название джоба
     */
    void pause(String jobName);

    /**
     * Продолжает работу джоба
     * @param jobName название джоба
     */
    void resume(String jobName);

    /**
     * Остонавливает джоб
     * @param jobName название джоба
     */
    void stop(String jobName);

    /**
     * Запускает джоб
     * @param jobName название джоба
     */
    void start(String jobName);

}
