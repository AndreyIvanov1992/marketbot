package com.pirogsoft.marketbot.job.api;

import java.util.Observer;
import java.util.Set;

/**
 * Объект, поддерживающий наблюдение за ним.
 * Created by AnIvanov on 06.04.2018.
 */
public interface ObservebleSupport {

    /**
     * Устанавливает набдудателей за джобом
     * @param observers сет набдюдателей за объектом
     */
    void setObservers(Set<Observer> observers);
}
