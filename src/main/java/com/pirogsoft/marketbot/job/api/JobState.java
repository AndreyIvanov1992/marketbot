package com.pirogsoft.marketbot.job.api;

/**
 * Возможнные состояния джоба
 * Created by AnIvanov on 06.04.2018.
 */
public enum JobState {

    /**
     * Работает
     */
    RUNNING,

    /**
     * Ждет
     */
    WAITING,

    /**
     * Поставлен на паузу
     */
    PAUSED,

    /**
     * Остановлен
     */
    STOPPED,

    /**
     * Поставлен на паузу, дорабатывает.
     */
    PAUSED_RUNNING,

    /**
     * Остановлен, дорабатывает
     */
    STOPPED_RUNNING
}
