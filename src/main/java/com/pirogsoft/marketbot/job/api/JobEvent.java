package com.pirogsoft.marketbot.job.api;

/**
 * События джобов
 * Created by AnIvanov on 06.04.2018.
 */
public enum JobEvent {
    /**
     * Джоб остановлен
     */
    STOPPED
}
