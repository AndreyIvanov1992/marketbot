package com.pirogsoft.marketbot.job.impl;

import com.pirogsoft.marketbot.LogUtils;
import com.pirogsoft.marketbot.job.api.Job;
import com.pirogsoft.marketbot.job.api.JobEvent;
import com.pirogsoft.marketbot.job.api.JobState;
import com.pirogsoft.marketbot.job.api.Method;
import org.apache.log4j.Logger;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Observable;
import java.util.Observer;
import java.util.Set;

/**
 * Имплементация джоба
 */
public final class JobImpl implements Job {

    public static final Logger logger = Logger.getLogger(JobImpl.class);

    /**
     * Признак того, что джоб на паузе
     */
    private boolean paused;

    /**
     * Метод, который переодически должен выполняться
     */
    private Method method;

    /**
     * Период, с котормы должен выполняься джоб в миллисекундах
     */
    private int period;

    /**
     * Признак того, что метод остановлен
     */
    private boolean stopped;

    /**
     * Описание джоба
     */
    private String description;

    /**
     * Последний запуск метода
     */
    private LocalDateTime lastDateTimeStart;

    /**
     * Полсденее заверщение метода
     */
    private LocalDateTime lastDateTimeFinished;

    /**
     * Объект обозревателя. Уведомляет своих подписчиков, в случае, если произошло какое-то событие
     */
    private Observable observable = new Observable();

    public JobImpl() {
    }

    @Override
    public void setObservers(Set<Observer> observers) {
        for (Observer observer : observers) {
            observable.addObserver(observer);
        }
    }

    @Override
    public boolean isPaused() {
        return paused;
    }

    @Override
    public void setPaused(boolean paused) {
        this.paused = paused;
    }

    @Override
    public int getPeriod() {
        return period;
    }

    @Override
    public void setPeriod(int period) {
        this.period = period;
    }

    @Override
    public boolean isStopped() {
        return stopped;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public void setMethod(Method method) {
        this.method = method;
    }

    @Override
    public LocalDateTime getLastDateTimeStart() {
        return lastDateTimeStart;
    }

    @Override
    public LocalDateTime getLastDateTimeFinished() {
        return lastDateTimeFinished;
    }

    @Override
    public boolean isMethodRunning() {
        return lastDateTimeFinished.isBefore(lastDateTimeStart);
    }


    @Override
    public void run() {
        stopped = false;
        while (!stopped) {
            if (!paused) {
                lastDateTimeStart = LocalDateTime.now();
                logger.info(LogUtils.obtainLogMessage("Method starting at " + lastDateTimeStart));
                observable.notifyObservers();//TODO: В принципе, можно передавать ивент
                try {
                    method.execute();
                } catch(Exception e) {
                    logger.info(LogUtils.obtainLogMessage("An ecxeption occurred during execute method."), e);
                }
                lastDateTimeFinished = LocalDateTime.now();
                logger.info(LogUtils.obtainLogMessage("Method finished at " + lastDateTimeFinished + "and it was running " + ChronoUnit.SECONDS.between(lastDateTimeStart, lastDateTimeFinished) + " seconds"));
                observable.notifyObservers();//TODO: В принципе, можно передавать ивент
            }
            try {
                Thread.sleep(period);
            } catch (InterruptedException e) {
                logger.error(LogUtils.obtainLogMessage(""), e);
            }
        }
        observable.notifyObservers(JobEvent.STOPPED);
    }

    @Override
    public void stop() {
        stopped = true;
    }

    @Override
    public JobState getJobState() {
        if (isStopped()) {
            if (isMethodRunning()) {
                return JobState.STOPPED_RUNNING;
            } else {
                return JobState.STOPPED;
            }
        } else if (isPaused()) {
            if (isMethodRunning()) {
                return JobState.PAUSED_RUNNING;
            } else {
                return JobState.PAUSED;
            }
        } else {
            if (isMethodRunning()) {
                return JobState.RUNNING;
            } else {
                return JobState.WAITING;
            }
        }
    }
}
