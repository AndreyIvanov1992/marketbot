package com.pirogsoft.marketbot.job.impl;

import com.pirogsoft.marketbot.job.api.Job;
import com.pirogsoft.marketbot.job.api.JobEvent;
import com.pirogsoft.marketbot.job.api.JobManager;
import com.pirogsoft.marketbot.job.api.ObservebleSupport;

import java.util.*;

/**
 * Имплементация менеджера джобов
 */
public class JobManagerImpl implements Observer, JobManager, ObservebleSupport{

    /**
     * карта джобов
     */
    private Map<String, Job> jobs;

    /**
     * карта потоков
     */
    private Map<String, Thread> jobThreads = new HashMap<>();

    /**
     * Запущены ли джобы
     */
    private boolean run;

    /**
     * Объект обозревателя. Уведомляет своих подписчиков, в случае, если произошло какое-то событие
     */
    private Observable observable = new Observable();

    public JobManagerImpl() {
    }

    @Override
    public void setJobs(Map<String, Job> jobs) {
        this.jobs = jobs;
    }

    /**
     * Ставит джоб на паузу
     * @param jobName название джоба
     */
    @Override
    public void pause(String jobName) {
        jobs.get(jobName).setPaused(true);
    }

    /**
     * Запускает джоб с паузы
     * @param jobName название джоба
     */
    @Override
    public void resume(String jobName) {
        jobs.get(jobName).setPaused(false);
    }

    /**
     * Останавливает джоб
     * @param jobName название джоба
     */
    @Override
    public void stop(String jobName) {
        jobs.get(jobName).stop();
    }

    /**
     * Запускает джоб
     * @param jobName название джоба
     */
    @Override
    public void start(String jobName) {
        if(!jobThreads.get(createThreadName(jobName)).isAlive()) {
            Thread jobThread = new Thread(jobs.get(jobName), createThreadName(jobName));
            jobThread.start();
            jobThreads.put(jobName, jobThread);
        }
    }

    /**
     * У джоба произошло какое-то событие.
     * @param o Джоб
     * @param arg Агрументы
     */
    @Override
    public void update(Observable o, Object arg) {
        observable.notifyObservers();
        if(JobEvent.STOPPED == arg) {
            if (allJobsStopped()) {
                run = false;
            }
        }
    }

    /**
     * Проверяет все джобы ли остановлены
     * @return остановлены ли все лдобы
     */
    private boolean allJobsStopped() {
        for(Job job : jobs.values()) {
            if(!job.isStopped()) {
                return false;
            }
        }
        return true;
    }

    /**
     * Получает все джобы
     * @return джобы
     */
    @Override
    public Map<String, Job> getJobs() {
        return  Collections.unmodifiableMap(jobs);
    }

    /**
     * Запускает все джобы
     */
    @Override
    public void run() {
        if(run) {
            return;
        }
        for(String jobName : jobs.keySet()) {
            Job job = jobs.get(jobName);
            Thread jobThread = new Thread(job, createThreadName(jobName));
            jobThread.start();
            jobThreads.put(jobName, jobThread);
        }
        run = true;
    }

    @Override
    public void setObservers(Set<Observer> observers) {
        for(Observer observer : observers) {
            observable.addObserver(observer);
        }
    }

    @Override
    public void stopAll() {
        for(Job job : jobs.values()) {
            job.stop();
        }
    }

    /**
     * Сроит название потока по названию джоба
     * @param jobName название джоба
     * @return название джоба
     */
    private String createThreadName(String jobName) {
        return jobName;
    }
}
