package com.pirogsoft.marketbot.exception;

/**
 * Исключение, которое говорит о том, что ключ апи маркета не действителен.
 * Created by AnIvanov on 25.04.2018.
 */
public class MarketBadKeyException extends Exception {

    public MarketBadKeyException(String message) {
        super(message);
    }
}
