package com.pirogsoft.marketbot;

import com.pirogsoft.marketbot.job.api.JobManager;
import com.pirogsoft.marketbot.job.api.Method;
import com.pirogsoft.marketbot.job.impl.JobManagerImpl;
import org.springframework.context.ApplicationContext;

/**
 * Точка входа для консольного приложения.
 * Created by AnIvanov on 15.05.2018.
 */
public class Runner {

    public static class Mode {

        public static final String COMMON = "COMMON";

        public static final String FILL_ITEMS_TO_SELL_DOTA = "FILL_ITEMS_TO_SELL_DOTA";

        public static final String FILL_ITEMS_TO_SELL_CS = "FILL_ITEMS_TO_SELL_CS";
    }

    public static void main(String[] args) {
        ApplicationContext applicationContext = ContextContainer.getInstance().getContext();
        String mode = Mode.COMMON;
        if(args.length > 0) {
            mode = args[0];
        }
        if(mode.equals(Mode.COMMON)) {
            JobManager jobManager = applicationContext.getBean(JobManagerImpl.class);
            jobManager.run();
            System.out.print("COMMON");
        } else if(mode.equals(Mode.FILL_ITEMS_TO_SELL_CS)) {
            ((Method) applicationContext.getBean("fillItemsToSellDota")).execute();
        } else if(mode.equals(Mode.FILL_ITEMS_TO_SELL_DOTA)) {
            ((Method) applicationContext.getBean("fillItemsToSellCs")).execute();
        }
        System.out.print("done");
    }
}
