package com.pirogsoft.marketbot;

/**
 * Created by AnIvanov on 14.05.2018.
 */
public class LogUtils {
    public static String obtainLogMessage(String message) {
        return Thread.currentThread().getName() + ": " + message;
    }
}
