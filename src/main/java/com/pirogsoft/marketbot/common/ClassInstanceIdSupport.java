package com.pirogsoft.marketbot.common;

/**
 * Наличие ClassInstanseId
 * Created by AnIvanov on 05.05.2018.
 */
public interface ClassInstanceIdSupport {

    /**
     * получение ClassInstanceId
     *
     * @return ClassInstanceId
     */
    ClassInstanceId obtainClassInstanceId();
}
