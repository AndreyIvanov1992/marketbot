package com.pirogsoft.marketbot.common;

/**
 * ClassInstanceId предмета. Объект уникально идинцифицирует предмет на маркете или в стиме.
 * Created by AnIvanov on 04.05.2018.
 */
public class ClassInstanceId {

    /**
     * Разделитель между classId и instanceId по умолчнанию. Используется для строкового представления classId_instanceId
     */
    public static final String DEFAULT_SEPARATOR = "_";

    /**
     * ClassId предмета
     */
    String classId;

    /**
     * InstanceId предмета
     */
    String instanceId;

    /**
     * @param classId    ClassId предмета
     * @param instanceId InstanceId предмета
     */
    public ClassInstanceId(String classId, String instanceId) {
        this.classId = classId;
        this.instanceId = instanceId;
    }

    /**
     * Получает объект ClassInstanceId на основании строкового представления
     * ClassInstanceId используя разделитель DEFAULT_SEPARATOR
     *
     * @param ClassInstanceIdString строковое представление ClassInstanceId
     * @return объект ClassInstanceId
     */
    public static ClassInstanceId obtainClassInstanceId(String ClassInstanceIdString) {
        return obtainClassInstanceId(ClassInstanceIdString, DEFAULT_SEPARATOR);
    }

    /**
     * Получает объект ClassInstanceId на основании строкового представления
     * ClassInstanceId используя разделитель separator
     *
     * @param ClassInstanceIdString строковое представление ClassInstanceId
     * @param separator             разделитель
     * @return объект ClassInstanceId
     */
    public static ClassInstanceId obtainClassInstanceId(String ClassInstanceIdString, String separator) {
        String[] stringArray = ClassInstanceIdString.split(DEFAULT_SEPARATOR);
        return new ClassInstanceId(stringArray[0], stringArray[1]);
    }

    /**
     * Получение ClassId
     *
     * @return ClassId
     */
    public String getClassId() {
        return classId;
    }

    /**
     * Установка ClassId
     *
     * @param classId ClassId
     */
    public void setClassId(String classId) {
        this.classId = classId;
    }

    /**
     * Получение InstanceId
     *
     * @return InstanceId
     */
    public String getInstanceId() {
        return instanceId;
    }

    /**
     * Установка InstanceId
     *
     * @param instanceId InstanceId
     */
    public void setInstanceId(String instanceId) {
        this.instanceId = instanceId;
    }

    /**
     * Получение строкового представления ClassInstanceID, используя стандартный разделитель
     *
     * @return строковое представление ClassInstanceID
     */
    public String obtainClassInstanceIdString() {
        return obtainClassInstanceIdString(DEFAULT_SEPARATOR);
    }

    /**
     * Получение строкового представления ClassInstanceID, используя разделитеьл separator
     *
     * @param separator разделитель
     * @return строковое представление ClassInstanceID
     */
    public String obtainClassInstanceIdString(String separator) {
        return getClassId() + separator + getInstanceId();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ClassInstanceId that = (ClassInstanceId) o;

        if (classId != null ? !classId.equals(that.classId) : that.classId != null) return false;
        return instanceId != null ? instanceId.equals(that.instanceId) : that.instanceId == null;

    }

    @Override
    public int hashCode() {
        int result = classId != null ? classId.hashCode() : 0;
        result = 31 * result + (instanceId != null ? instanceId.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return obtainClassInstanceIdString();
    }
}
