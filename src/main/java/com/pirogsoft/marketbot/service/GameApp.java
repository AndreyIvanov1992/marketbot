package com.pirogsoft.marketbot.service;

/**
 * Приложение, с api которого работаем.
 * Created by AnIvanov on 03.05.2018.
 */
public enum GameApp {

    CS("https://market.csgo.com",730),
    DOTA("https://market.dota2.net",570),
    PUBG("https://pubg.tm",578080);

    /**
     * URL API маркета
     */
    private String marketApiUrl;

    /**
     * номер приложения в стиме
     */
    private int steamAppId;

    GameApp(String marketApiUrl, int steamAppId) {
        this.marketApiUrl = marketApiUrl;
        this.steamAppId = steamAppId;
    }

    /**
     * Получение API маркета
     * @return API маркета
     */
    public String getMarketApiUrl() {
        return marketApiUrl;
    }

    /**
     * Получение номера приложения в steam
     * @return
     */
    public int getSteamAppId() {
        return steamAppId;
    }
}
