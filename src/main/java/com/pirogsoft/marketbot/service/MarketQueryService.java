package com.pirogsoft.marketbot.service;

import com.pirogsoft.marketbot.ContextContainer;
import com.pirogsoft.marketbot.LogUtils;
import com.pirogsoft.marketbot.database.service.VariableService;
import com.pirogsoft.marketbot.exception.MarketBadKeyException;
import com.pirogsoft.marketbot.query.Query;
import com.pirogsoft.marketbot.query.beans.market.request.common.MarketParametersBean;
import com.pirogsoft.marketbot.query.beans.market.request.common.MarketSecurableRequestParametersBean;
import com.pirogsoft.marketbot.query.beans.market.request.getorders.GetOrdersRequestParametersBean;
import com.pirogsoft.marketbot.query.beans.market.request.itemdb.ItemdbRequestParametersBean;
import com.pirogsoft.marketbot.query.beans.market.request.massinfo.MassInfoRequestParametersBean;
import com.pirogsoft.marketbot.query.beans.market.request.masssetpricebyidrequest.IdPricePair;
import com.pirogsoft.marketbot.query.beans.market.request.masssetpricebyidrequest.MassSetPriceByIdRequestParametersBean;
import com.pirogsoft.marketbot.query.beans.market.request.processorder.ProcessOrderParametersBean;
import com.pirogsoft.marketbot.query.beans.market.response.common.MarketResponseBean;
import com.pirogsoft.marketbot.query.beans.market.response.currentdb.CurrentDbResponseBean;
import com.pirogsoft.marketbot.query.beans.market.response.getmyselloffers.GetMySellOffersResponseBean;
import com.pirogsoft.marketbot.query.beans.market.response.getorders.GetOrdersResponseBean;
import com.pirogsoft.marketbot.query.beans.market.response.getorders.MyOrder;
import com.pirogsoft.marketbot.query.beans.market.response.itemdb.ItemdbDotaCsvRecordWrapper;
import com.pirogsoft.marketbot.query.beans.market.response.massinfo.MassInfoResponseBean;
import com.pirogsoft.marketbot.query.beans.market.response.masssetpricebyid.MassSetPriceByIdResponseBean;
import com.pirogsoft.marketbot.query.beans.market.response.processorder.ProcessOrderResponseBean;
import com.pirogsoft.marketbot.query.parser.csv.CsvRecordWrappersCollection;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Сервис для работы с маркетом
 */
@Service
public class MarketQueryService {

    public static Logger logger = Logger.getLogger(MarketQueryService.class);

    /**
     * Названия бинов запросов
     */
    public static class QueryBeanNames {

        public static final String GET_MY_SELL_OFFERS = "getMySellOffersMarketQuery";

        public static final String MASS_INFO = "massInfoMarketQuery";

        public static final String MASS_SET_PRICE_BY_ID = "massSetPriceByIdQuery";

        public static final String PROCESS_ORDER = "processOrderQuery";

        public static final String GET_ORDERS = "getOrdersQuery";

        public static final String CURRENT_730_QUERY = "current730Query";

        public static final String ITEMDB_DOTA_MARKET_QUERY = "itemdbDotaMarketQuery";
    }

    /**
     * Сервис переменных БД
     */
    private VariableService variableService;

    /**
     * Установка сервиса переменных БД. Устанавливается спрингом.
     *
     * @param variableService Сервис переменных БД
     */
    @Autowired
    public void setVariableService(VariableService variableService) {
        this.variableService = variableService;
    }

    /**
     * Получение всех выставленных предметов
     *
     * @param game Приложение
     * @return Бин ответа
     * @throws MarketBadKeyException Ключ апи маркета не действителен
     */
    public GetMySellOffersResponseBean getMySellOffers(GameApp game) throws MarketBadKeyException {
        MarketSecurableRequestParametersBean parametersBean = new MarketSecurableRequestParametersBean();
        return execute(QueryBeanNames.GET_MY_SELL_OFFERS, game, parametersBean);
    }

    /**
     * Получение информации по предметам. Так как запрос может делится на несколько запросов
     * возвращает список карт ответа, по карте, для каждого запроса.
     *
     * @param game       Игра
     * @param sell       Количество выводимых запросов на продажу предмета
     * @param buy        Количество выводимых запросов на покупку предмета
     * @param history    Количество выводимых последних продаж
     * @param info       Детальность информации получаемой по проедмету
     * @param itemIdList Список из classid_instanceid
     * @return Список ответов
     * @throws MarketBadKeyException Ключ апи маркета не действителен
     */
    public List<MassInfoResponseBean> massInfo(GameApp game, MassInfoRequestParametersBean.Sell sell, MassInfoRequestParametersBean.Buy buy, MassInfoRequestParametersBean.History history, MassInfoRequestParametersBean.Info info, ArrayList<String> itemIdList) throws MarketBadKeyException {
        List<List<String>> separatedList = separateArrayList(itemIdList, MassInfoRequestParametersBean.MAX_LIST_ITEM_COUNT);
        List<MassInfoResponseBean> result = new ArrayList<>();
        for (List<String> partOfList : separatedList) {
            MassInfoRequestParametersBean parametersBean = new MassInfoRequestParametersBean();
            parametersBean.setSell(sell);
            parametersBean.setBuy(buy);
            parametersBean.setHistory(history);
            parametersBean.setInfo(info);
            parametersBean.setList(partOfList);
            MassInfoResponseBean response = execute(QueryBeanNames.MASS_INFO, game, parametersBean);
            result.add(response);
        }

        return result;
    }

    /**
     * Массовая установка цен. Так как запрос может делится на несколько запросов
     * возвращает список карт ответа, по карте, для каждого запроса
     *
     * @param game      Игра
     * @param newPrices список пар id предмета - новая цена
     * @return Список ответов
     * @throws MarketBadKeyException Ключ апи маркета не действителен
     */
    public List<MassSetPriceByIdResponseBean> massSetPriceById(GameApp game, ArrayList<IdPricePair> newPrices) throws MarketBadKeyException {
        List<List<IdPricePair>> newPricesParts = separateArrayList(newPrices, MassSetPriceByIdRequestParametersBean.MAX_LIST_ITEM_COUNT);
        List<MassSetPriceByIdResponseBean> result = new ArrayList<>();
        for (List<IdPricePair> newPricesPart : newPricesParts) {
            MassSetPriceByIdRequestParametersBean parametersBean = new MassSetPriceByIdRequestParametersBean();
            parametersBean.setList(newPricesPart);
            MassSetPriceByIdResponseBean response = execute(QueryBeanNames.MASS_SET_PRICE_BY_ID, game, parametersBean);
            result.add(response);
        }
        return result;
    }

    /**
     * Запрос на установку или обновления ордера
     *
     * @param classid    ClassID предмета в Steam
     * @param instanceid InstanceID предмета в Steam
     * @param price      цена в копейках
     * @return бин ответа
     */
    public ProcessOrderResponseBean processOrder(GameApp game, String classid, String instanceid, int price) throws MarketBadKeyException {
        ProcessOrderParametersBean parametersBean = new ProcessOrderParametersBean();
        parametersBean.setClassid(classid);
        parametersBean.setInstanceid(instanceid);
        parametersBean.setPrice(price);
        return execute(QueryBeanNames.PROCESS_ORDER, game, parametersBean);
    }

    /**
     * Запрос на первые 2000 моих ордеров на покупку.
     *
     * @param game Игра
     * @return Объект ответа
     * @throws MarketBadKeyException Ключ апи маркета не действителен
     */
    public GetOrdersResponseBean getOrders(GameApp game) throws MarketBadKeyException {
        return getOrders(game, null);
    }

    /**
     * Заспрос получение моих ордеров на покупку.
     *
     * @param game Игра
     * @param page Номер страницы, начиная с 1.
     *             Страница по 500 элементов.
     *             Если 0 или null, возвращает первые 2к записей.
     * @return Объект ответа
     * @throws MarketBadKeyException Ключ апи маркета не действителен
     */
    public GetOrdersResponseBean getOrders(GameApp game, Integer page) throws MarketBadKeyException {
        GetOrdersRequestParametersBean parametersBean = new GetOrdersRequestParametersBean();
        parametersBean.setPage(page);
        return execute(QueryBeanNames.GET_ORDERS, game, parametersBean);
    }

    /**
     * Получение моих ордеров на покупку постранично. Использовать в случае, если количество ордеров больше 2000.
     *
     * @param game Игра
     * @return Список ответов для каждой страницы
     * @throws MarketBadKeyException Ключ апи маркета не действителен
     */
    public List<GetOrdersResponseBean> getOrdersInParts(GameApp game) throws MarketBadKeyException {
        int page = 0;
        List<GetOrdersResponseBean> result = new ArrayList<>();
        boolean ordersExists;
        do {
            page++;
            GetOrdersResponseBean orders = getOrders(game, page);
            if (orders == null) {
                return null;
            }
            List<MyOrder> orderList = orders.getOrders();
            if (orderList != null) {
                result.add(orders);
            }
            ordersExists = orderList != null && orderList.size() == GetOrdersResponseBean.COUNT_PER_PAGE;
        } while (ordersExists);
        return result;
    }

    /**
     * Получение объекта с именем csv файла с предметами маркета
     *
     * @param game игра
     * @return объект с именем csv файла с предметами маркета
     */
    public CurrentDbResponseBean getCurrent730(GameApp game) {
        MarketParametersBean parametersBean = new MarketParametersBean();
        return execute(QueryBeanNames.CURRENT_730_QUERY, game, parametersBean);
    }

    /**
     * Получает БД маркета в формате CSV
     *
     * @param dbName название БД
     * @return БД маркета в формате CSV
     */
    public CsvRecordWrappersCollection<ItemdbDotaCsvRecordWrapper> getItemdbDota(String dbName) {
        ItemdbRequestParametersBean parametersBean = new ItemdbRequestParametersBean();
        parametersBean.setDbName(dbName);
        return execute(QueryBeanNames.ITEMDB_DOTA_MARKET_QUERY, GameApp.DOTA, parametersBean);
    }

    /**
     * Получает БД маркета в формате CSV
     *
     * @return БД маркета в формате CSV
     */
    public CsvRecordWrappersCollection<ItemdbDotaCsvRecordWrapper> getItemdbDota() {
        return getItemdbDota(getCurrent730(GameApp.DOTA).getDb());
    }

    /**
     * Выполнение запроса на маркете с ключем
     *
     * @param queryBeanName  Наименование бина запроса
     * @param game           Игра
     * @param parametersBean Бин параметров
     * @param <T>            Тип бина параметров
     * @param <S>            Тип объекта ответа
     * @return Бин ответа
     * @throws MarketBadKeyException Ключ апи маркета не действителен
     */
    private <T extends MarketSecurableRequestParametersBean, S extends MarketResponseBean> S execute(String queryBeanName, GameApp game, T parametersBean) throws MarketBadKeyException {

        parametersBean.setGameApp(game);
        Query<T, S> query = (Query) ContextContainer.getInstance().getContext().getBean(queryBeanName);
        parametersBean.setMarketKey(variableService.getApiMarketKey());
        try {
            //TODO: считаем тут запросы!
            S result = query.execute(parametersBean);
            if (result.isSuccess() == null || !result.isSuccess()) {
                if (result.getError() != null) {
                    if (result.isBadKey()) {
                        throw new MarketBadKeyException("Bed market key: " + parametersBean.getMarketKey());
                    }
                    logger.error(LogUtils.obtainLogMessage("An error occurred in the market during execute query " + query + ": " + result.getError()));
                }
            }
            return result;
        } catch (IOException e) {
            logger.error(LogUtils.obtainLogMessage("An error occurred during execute query: " + query), e);
            return null;
        }
    }

    /**
     * Выполнение запроса на маркете без ключа
     *
     * @param queryBeanName  Наименование бина запроса
     * @param game           Игра
     * @param parametersBean Бин параметров
     * @param <T>            Тип бина параметров
     * @param <S>            Тип объекта ответа
     * @return Бин ответа
     */
    private <T extends MarketParametersBean, S> S execute(String queryBeanName, GameApp game, T parametersBean) {
        parametersBean.setGameApp(game);
        Query<T, S> query = (Query) ContextContainer.getInstance().getContext().getBean(queryBeanName);
        try {
            return query.execute(parametersBean);
        } catch (IOException e) {
            logger.error(LogUtils.obtainLogMessage("An error occurred during execute query: " + query), e);
            return null;
        }
    }

    /**
     * Делит список items на много списков по length элементов в каждом
     *
     * @param items  список, который необходимо разделить
     * @param length по сколько элементы должны быть результирующие списки
     * @param <T>    тип элемента списка
     * @return Список списков по length элементов
     */
    private <T> List<List<T>> separateArrayList(ArrayList<T> items, int length) {
        if (items == null) {
            return null;
        }
        int i = 0;
        List<List<T>> result = new ArrayList<>();
        while (i < items.size()) {
            int toIndex = i + length;
            if (items.size() < toIndex) {
                toIndex = items.size();
            }
            result.add(items.subList(i, toIndex));
            i += length;
        }
        return result;
    }

    /**
     * Проверка удачно ли выполнился запрос на маркет по объекту ответа
     *
     * @param responseBean объект ответа
     * @return результат проверки
     */
    public boolean isSuccess(MarketResponseBean responseBean) {
        return responseBean != null && responseBean.isSuccess() != null && responseBean.isSuccess();
    }

    public void logErrorMessage(Logger logger, MarketResponseBean responseBean) {
        if (responseBean != null) {
            logger.error(LogUtils.obtainLogMessage(": Market error: " + responseBean.getError()));
        }
    }
}
