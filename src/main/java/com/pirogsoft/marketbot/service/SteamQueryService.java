package com.pirogsoft.marketbot.service;

import com.pirogsoft.marketbot.ContextContainer;
import com.pirogsoft.marketbot.LogUtils;
import com.pirogsoft.marketbot.query.Query;
import com.pirogsoft.marketbot.query.beans.steam.request.PriceOverviewRequestBean;
import com.pirogsoft.marketbot.query.beans.steam.request.SteamCurrency;
import com.pirogsoft.marketbot.query.beans.steam.response.PriceOverviewResponseBean;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import java.io.IOException;

/**
 * Сервис для стимовских запросов
 * Created by AnIvanov on 11.05.2018.
 */
@Service
public class SteamQueryService {

    public static Logger logger = Logger.getLogger(SteamQueryService.class);

    /**
     * Названия бинов запросов
     */
    public static class QueryBeanNames {

        public static final String PRICE_OVERVIEW_QUERY = "priceOverviewQuery";
    }

    /**
     * Получение цены в стиме
     *
     * @param game           игра
     * @param marketHashName строка, идинцифицирующий предмет в стиме
     * @param currency       валюта
     * @return объект ответа бина
     */
    public PriceOverviewResponseBean priceOverview(GameApp game, String marketHashName, SteamCurrency currency) {
        PriceOverviewRequestBean requestBean = new PriceOverviewRequestBean();
        requestBean.setCurrency(currency);
        requestBean.setMarketHashName(marketHashName);
        requestBean.setGameApp(game);
        PriceOverviewResponseBean responseBean = execute(QueryBeanNames.PRICE_OVERVIEW_QUERY, game, requestBean);
        if (responseBean.isSuccess() == null || responseBean.isSuccess() == false) {
            logger.error(LogUtils.obtainLogMessage("An error occurred during execute PriceOverview steam query."));
            return null;
        }
        return responseBean;
    }

    /**
     * Выполнение запроса в steam
     *
     * @param queryBeanName  навзвание бина запроса
     * @param game           игра
     * @param parametersBean бин аврвме
     * @param <T>            тип бина параметров
     * @param <S>            тип объекта ответа
     * @return объект ответа
     */
    private <T, S> S execute(String queryBeanName, GameApp game, T parametersBean) {
        Query<T, S> query = (Query) ContextContainer.getInstance().getContext().getBean(queryBeanName);
        try {
            return query.execute(parametersBean);
        } catch (IOException e) {
            logger.error(LogUtils.obtainLogMessage("An error occurred during execute query: " + query), e);
            return null;
        }
    }
}
